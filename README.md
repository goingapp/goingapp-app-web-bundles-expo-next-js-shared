# goingapp-app-web-bundles-expo-next-js-shared

Shared library for Going React Native front end repositories.

## Instalacja

1. Zaciągamy kod z repozytorium i uruchamiamy wiersz poleceń wewnątrz katalogu głównego.
1. Uruchamiamy polecenie:
```sh
yarn install
```
3. Instalujemy [yalc](https://github.com/wclr/yalc) - manager ułatwiający pracę nad biblioteką:
```sh
yarn global add yalc
```
4. Publikujemy bibliotekę w lokalnym yalc store, aby udostępnić ją dla lokalnych aplikacji:
```shell
yalc publish
```
5. Przenosimy się do repozytorium, w którym "konsumujemy" bibliotekę. Tam, na czas pracy zastępujemy odwołanie do biblioteki w package.json:
```shell
yalc add goingapp-shared --link
```

## Praca nad kodem
1. Aby ułatwić sobie pracę, możemy uruchomić "hot reload":
```shell
yarn hot:reload
```
2. (Opcjonalnie, jeśli nie zostało to jeszcze zrobione w danej aplikacji) Konfigurujemy webpacka, dodając do configu:
```js
watchFolders: [
    /* watch shared library changes for hot reload */
    path.resolve(
      __dirname,
      'node_modules/goingapp-app-web-bundles-expo-next-js-shared-refined'
    ),
  ],
```

## Release
### Docelowo:
1. Podbijamy wersję w package.json.
2. Wystawiamy PR do brancha master.
3. Przed mergem wchodzimy w ekran Commits i wybieramy najnowszy commit. Dodajemy mu tag z numerem wersji.
4. Mergujemy. Biblioteka powinna być dostępna do ściągnięcia pod adresem:
```
git+ssh://git@bitbucket.org/goingapp/goingapp-app-web-bundles-expo-next-js-shared#x.x.x (podmieniamy na nr wersji, która nas interesuje)
```

### Tymczasowo - do czasu ustawienia dostępów w Expo / zmiany statusu repozytorium z biblioteką na publiczne:
1. Dla utrzymania spójności kodu wykonujemy wszystkie 4 kroki spośrod powyższych.
2. Następnie wchodzimy w główny katalog repozytorium z biblioteką i odpalamy komendę:
```sh
npm pack
```
3. Powinien dzięki temu utworzyć się plik z biblioteką.
4. Plik ten wgrywamy w odpowiednie miejsce w aplikacji konsumującej i aktualizujemy ścieżkę w jej package.json (zmieni się numer wersji w nazwie pliku).
