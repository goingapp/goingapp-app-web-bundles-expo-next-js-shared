export const aspectRatio: { [key: string]: [number, number] } = {
  default: [81, 70],
  slidableCardVertical: [55, 95],
  gallerySmallImage: [389, 256],
}
