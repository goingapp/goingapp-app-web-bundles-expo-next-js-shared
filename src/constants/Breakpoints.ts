import { IBreakpoints } from '../models/Theme'

export enum Breakpoint {
  XS = 'xs',
  SM = 'sm',
  MD = 'md',
  LG = 'lg',
  XL = 'xl',
}

export enum BreakpointValue {
  XS = 0,
  SM = 600,
  MD = 960,
  LG = 1200,
  XL = 1920,
}

export const breakpoints: IBreakpoints = {
  xs: 0,
  sm: 600,
  md: 960,
  lg: 1200,
  xl: 1920,
}
