export enum GoingMoreMainCategories {
  Muzyka = 'Muzyka',
  Miasto = 'Miasto',
}

export const GoingMoreMainCategoriesSlugs = {
  [GoingMoreMainCategories.Miasto]: 'miasto',
  [GoingMoreMainCategories.Muzyka]: 'muzyka',
}

export const GoingMoreTagColors = {
  [GoingMoreMainCategories.Miasto]: '#FFE150',
  [GoingMoreMainCategories.Muzyka]: '#68BEB7',
}

export const GoingMoreMainCatsIds = {
  [GoingMoreMainCategories.Miasto]: 5139,
  [GoingMoreMainCategories.Muzyka]: 3238,
}
