export enum SPACING {
  HUMONGOUS = 24,
  HUGE = 12,
  LARGE = 8,
  MEDIUM = 6,
  REGULAR = 4,
  SMALL = 2,
  TINY = 1,
  ONE_PIXEL = 0.25,
}
