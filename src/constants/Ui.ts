export const Ui = {
  arrowControlsWidth: 96,
  minCardWidth: 272,
  calendarCols: 7,
  calendarRows: 6,
}
