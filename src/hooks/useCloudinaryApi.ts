import { useEffect, useState } from 'react'

import { Cloudinary } from '@cloudinary/url-gen'
import { ajax } from 'rxjs/ajax'
import { catchError, map } from 'rxjs/operators'

import { ICloudinaryList } from '../models/Cloudinary'
import { fillUrlParams } from '../utils/Helpers/fillUrlParams'

export const useCloudinaryApi = (
  cloudName: string,
  urlCloudinaryList: string,
  slug?: string,
  prefix?: string
): { cloudinaryImagesUrls: string[] } => {
  const [cloudinaryImagesUrls, setCloudinaryImagesUrls] = useState<string[]>([])
  const cloudinary = new Cloudinary({
    cloud: {
      cloudName,
    },
  })

  useEffect(() => {
    if (slug) {
      const url = prefix
        ? fillUrlParams(urlCloudinaryList, [':prefix', ':slug'], [prefix, slug])
        : fillUrlParams(urlCloudinaryList, [':slug'], [slug]).replace(
            '/:prefix/',
            '/'
          )

      const subscription = ajax
        .getJSON<ICloudinaryList>(url)
        .pipe(
          map((response) => response.resources),
          catchError(() => [])
        )
        .subscribe((data) => {
          const imagesUrls = data.map((image) =>
            cloudinary.image(image.public_id).toURL()
          )
          setCloudinaryImagesUrls(imagesUrls)
        })

      return () => subscription.unsubscribe()
    }
  }, [slug])

  return { cloudinaryImagesUrls }
}
