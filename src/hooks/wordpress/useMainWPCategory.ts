import { useEffect, useState } from 'react'

import { WP_REST_API_Category } from 'wp-types'

import {
  GoingMoreMainCategories,
  GoingMoreMainCatsIds,
} from '../../constants/GoingMore'

export const useMainWPCategory = (categories?: WP_REST_API_Category[]) => {
  const [mainCat, setMainCat] = useState<GoingMoreMainCategories>()

  useEffect(() => {
    if (
      categories?.find(
        (cat) =>
          cat.parent === GoingMoreMainCatsIds[GoingMoreMainCategories.Muzyka]
      )
    )
      setMainCat(GoingMoreMainCategories.Muzyka)
    if (
      categories?.find(
        (cat) =>
          cat.parent === GoingMoreMainCatsIds[GoingMoreMainCategories.Miasto]
      )
    )
      setMainCat(GoingMoreMainCategories.Miasto)
  })

  return { mainCat }
}
