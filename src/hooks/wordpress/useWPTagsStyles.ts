import { StyleSheet } from 'react-native'

import {
  GoingMoreMainCategories,
  GoingMoreTagColors,
} from '../../constants/GoingMore'

export const useWPTagsStyles = (
  mainCat: GoingMoreMainCategories,
  theme: ReactNativePaper.Theme
) =>
  StyleSheet.create({
    tagLabel: {
      color: theme.colors.black,
    },
    tag: {
      backgroundColor: GoingMoreTagColors[mainCat],
      borderRadius: 0,
    },
  })
