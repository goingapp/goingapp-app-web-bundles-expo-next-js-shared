import useSWR from 'swr'
import { WP_REST_API_User } from 'wp-types'

import { Fetcher } from '../../services/_Fetcher'

export const useWordpressAuthor = (author?: number, wordpressHost?: string) => {
  const { data: wpAuthorsData, isValidating: isWpAuthorsValidating } =
    useSWR<WP_REST_API_User>(
      !!author &&
        wordpressHost &&
        `${wordpressHost}/wp-json/wp/v2/users/${author}`,
      Fetcher.get
    )

  return {
    wpAuthorsData,
    isWpAuthorsValidating,
  }
}
