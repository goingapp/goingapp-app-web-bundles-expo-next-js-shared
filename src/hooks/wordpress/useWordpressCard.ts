import { useEffect, useState } from 'react'

import { useTheme } from 'react-native-paper'

import { GoingMoreMainCategories } from '../../constants/GoingMore'
import { getCategoriesFromWPPost } from '../../utils/Helpers/getCategoriesFromWPPost'
import { useMainWPCategory } from './useMainWPCategory'
import { useWordpressAuthor } from './useWordpressAuthor'
import { useWordpressCategories } from './useWordpressCategories'
import { useWordpressPost } from './useWordpressPost'
import { useWPTagsStyles } from './useWPTagsStyles'

export const useWordpressCard = (link?: string, wordpressHost?: string) => {
  const [postCatsIds, setPostCatsIds] = useState<number[]>([])
  const [postCats, setPostCats] = useState<
    Array<{ slug: string; name: string; id: number }>
  >([])
  const [authorId, setAuthorId] = useState<number>()
  const theme = useTheme()

  const isWpArticle = link?.includes('artykul/')
  const articleSlugSplit = isWpArticle ? link?.split('/') : undefined
  const articleSlug = !!articleSlugSplit?.length
    ? articleSlugSplit[articleSlugSplit.length - 2]
    : undefined

  const { wpPostData } = useWordpressPost(articleSlug, wordpressHost)
  const { wpCatsData } = useWordpressCategories(postCatsIds, wordpressHost)
  const { wpAuthorsData } = useWordpressAuthor(authorId, wordpressHost)
  const { mainCat } = useMainWPCategory(wpCatsData)

  const tagsStyles = useWPTagsStyles(
    mainCat || GoingMoreMainCategories.Miasto,
    theme
  )

  useEffect(() => {
    if (wpPostData?.length) {
      setPostCatsIds(getCategoriesFromWPPost(wpPostData[0]))
      setAuthorId(wpPostData[0].author)
    }
  }, [wpPostData])

  useEffect(() => {
    if (wpCatsData) {
      setPostCats(
        wpCatsData.map((cat) => ({
          name: cat.name,
          slug: cat.slug,
          id: cat.id,
        }))
      )
    }
  }, [wpCatsData])

  return { isWpArticle, postCats, tagsStyles, wpAuthorsData }
}
