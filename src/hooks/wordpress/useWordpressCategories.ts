import useSWR from 'swr'
import { WP_REST_API_Category } from 'wp-types'

import { Fetcher } from '../../services/_Fetcher'

export const useWordpressCategories = (
  postCats: number[],
  wordpressHost?: string
) => {
  const { data: wpCatsData, isValidating: isWpCatsValidating } = useSWR<
    WP_REST_API_Category[]
  >(
    !!postCats.length &&
      wordpressHost &&
      `${wordpressHost}/wp-json/wp/v2/categories?include=${postCats.join(',')}`,
    Fetcher.get
  )

  return {
    wpCatsData,
    isWpCatsValidating,
  }
}
