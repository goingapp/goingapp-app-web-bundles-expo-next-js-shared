import useSWR from 'swr'
import { WP_REST_API_Post } from 'wp-types'

import { Fetcher } from '../../services/_Fetcher'

export const useWordpressPost = (
  articleSlug?: string,
  wordpressHost?: string
) => {
  const { data: wpPostData, isValidating: isWpPostValidating } = useSWR<
    WP_REST_API_Post[]
  >(
    !!wordpressHost &&
      !!articleSlug &&
      `${wordpressHost}/wp-json/wp/v2/posts?slug=${articleSlug}`,
    Fetcher.get
  )

  return {
    wpPostData,
    isWpPostValidating,
  }
}
