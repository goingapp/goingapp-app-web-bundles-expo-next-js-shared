export const fallbackImage = [
  require('./mosaic_1.jpeg'),
  require('./mosaic_2.jpeg'),
  require('./mosaic_3.jpeg'),
  require('./mosaic_4.jpeg'),
  require('./mosaic_5.jpeg'),
  require('./mosaic_6.jpeg'),
  require('./mosaic_7.jpeg'),
  require('./mosaic_8.jpeg'),
  require('./mosaic_9.jpeg'),
  require('./mosaic_10.jpeg'),
  require('./mosaic_11.jpeg'),
]
