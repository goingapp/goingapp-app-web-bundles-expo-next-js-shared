// @TODO: refactor with proper models

export interface ITerm {
  description: string
  documentUrl: string
  id: number
  name: string
  required: number
}

export interface ITagV3 {
  [key: string]: string
}

export interface IEventCategory {
  id: number
  name: string
}

export interface ISlides {
  lar: string[]
  med: string[]
}

export interface IAktivistEventExtension {
  articleLink: null
  slides: ISlides
  thumb: string
}

export interface ICityV3 {
  id: number
  name: string
}

export interface IPlaceV3 {
  additionalInfo: string | null
  address: string
  city: ICityV3
  cityId: number
  facebookProfile: null
  id: number
  lat: number
  lon: number
  name: string
  showMap: number
  thumb: string
}

export interface IPartnerV3 {
  id: number
  name: string
  seatsIoKey: string
  terms: ITerm[]
}

export interface IRundateV3 {
  buttonLabel: string | null
  couponTypes: []
  dateDescription: string
  endDate: null
  enddateShort: null
  extendedRundateShort: string
  formSetting: null
  free_of_charge: boolean
  groupDate: string
  groupDateDesc: string
  hasManyPools: boolean
  hideClosedPools: number
  hourDescription: string
  id: number
  isAvailable: boolean
  notforsale: boolean
  partner: IPartnerV3
  partnerId: number
  passed: boolean
  place: IPlaceV3
  placeId: number
  price: string
  priceDescription: string
  redirectToUrl: string | null
  rundate: string
  rundateDescription: string
  rundateShort: string
  seatsIoChartId: null
  shipmentCarriers: []
  simpleGroupDate: string
  slug: string
  startDate: string
  start_date: string
  useLegacyForm: number
}

export interface IEventV3 {
  additionalDescription: string
  additionalDescriptionbb: string
  aktivistEventExtension: IAktivistEventExtension
  buyNextGeneric: null
  couponTypes: null
  description: string
  descriptionbb: string
  eventCategory: IEventCategory
  extraHTML: string
  fbEvent: string
  featPanska: boolean
  formattedDescription: string
  id: number
  media: []
  notforsale: number
  partnerId: number
  partnerName: string | null
  place: IPlaceV3
  recurring: null
  relatedRundates: null
  rundate: IRundateV3
  slides: ISlides
  slug: string
  tags: ITagV3
  teaser: string
  thumb: string
  title: string
}
