import { WP_REST_API_Post } from 'wp-types'

export interface IAuthor {
  avatarUrl: string
  link: string
  name: string
  description: string
}

export interface IArticle {
  articleSlug: string
  authors: IAuthor[]
  categories: number[]
  date: string
  html: string
  id: number
  imageUrl: string
  source: string
  tags: number[]
  title: string
}

export interface IWordpressFullAuthor {
  id: number
  name: string
  url: string
  description: string
  link: string
  slug: string
  avatar_urls: {
    '24': string
    '48': string
    '96': string
  }
  _links: {
    self: [{ href: string }]
    collection: [{ href: string }]
  }
  yoast_head?: string
}

export type IAuthorResponse = [IWordpressFullAuthor]

export type IArticleResponse = [IWordPressArticleEmbedded]

export interface IWordPressArticleEmbedded extends WP_REST_API_Post {
  _embedded: {
    author: [
      {
        id: number
        name: string
        url: string
        description: string
        link: string
        slug: string
        avatar_urls: {
          '24': string
          '48': string
          '96': string
        }
        _links: {
          self: [{ href: string }]
          collection: [{ href: string }]
        }
      }
    ]
    'wp:featuredmedia': IWordPressMedia[]
    'wp:term': IWordPressTerm[][]
  }
  yoast_head?: string
  published: string
}

interface IWordPressMediaFormat {
  file: string
  width: number
  height: number
  mime_type: string
  source_url: string
}

interface IWordPressMedia {
  id: number
  date: string
  slug: string
  type: string
  link: string
  title: { rendered: string }
  author: number
  caption: { rendered: string }
  alt_text: string
  media_type: string
  mime_type: string
  media_details: {
    width: number
    height: number
    file: string
    sizes: {
      medium: IWordPressMediaFormat
      large: IWordPressMediaFormat
      thumbnail: IWordPressMediaFormat
      medium_large: IWordPressMediaFormat
      '1536x1536': IWordPressMediaFormat
      'post-thumbnail': IWordPressMediaFormat
      'theissue-thumbnail': IWordPressMediaFormat
      'theissue-square': IWordPressMediaFormat
      'theissue-squaresmall': IWordPressMediaFormat
      'theissue-rectangle': IWordPressMediaFormat
      'theissue-masonry': IWordPressMediaFormat
      'theissue-full': IWordPressMediaFormat
      'theissue-thumbnail-small': IWordPressMediaFormat
      'theissue-thumbnail-x2': IWordPressMediaFormat
      'theissue-thumbnail-mini': IWordPressMediaFormat
      'theissue-square-small': IWordPressMediaFormat
      'theissue-square-x2': IWordPressMediaFormat
      'theissue-square-x3': IWordPressMediaFormat
      'theissue-square-mini': IWordPressMediaFormat
      'theissue-squaresmall-small': IWordPressMediaFormat
      'theissue-squaresmall-x2': IWordPressMediaFormat
      'theissue-squaresmall-mini': IWordPressMediaFormat
      'theissue-rectangle-small': IWordPressMediaFormat
      'theissue-rectangle-x2': IWordPressMediaFormat
      'theissue-rectangle-x3': IWordPressMediaFormat
      'theissue-rectangle-mini': IWordPressMediaFormat
      'theissue-masonry-small': IWordPressMediaFormat
      'theissue-masonry-x2': IWordPressMediaFormat
      'theissue-masonry-mini': IWordPressMediaFormat
      'theissue-full-small': IWordPressMediaFormat
      'theissue-full-mini': IWordPressMediaFormat
      'thb-sponsor-x2': IWordPressMediaFormat
      full: IWordPressMediaFormat
    }
  }
  source_url: string
  _links: {
    self: [{ href: string }]
    collection: [{ href: string }]
    about: [{ href: string }]
    author: [
      {
        embeddable: boolean
        href: string
      }
    ]
    replies: [
      {
        embeddable: boolean
        href: string
      }
    ]
  }
}

interface IWordPressTerm {
  id: number
  link: string
  name: string
  slug: string
  taxonomy: string
  _links: {
    self: [{ href: string }]
    collection: [{ href: string }]
    about: [
      {
        href: string
      }
    ]
    'wp:post_type': [
      {
        href: string
      }
    ]
    curies: [{ name: string; href: string; templated: boolean }]
  }
}

interface IWordPressLinkTerm {
  taxonomy: string
  embeddable: boolean
  href: string
}
