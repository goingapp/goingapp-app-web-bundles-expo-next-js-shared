import { ITag } from './Event'

export interface IArtist {
  id: number
  name: string
  slug: string
  bio?: string
  fbLink?: string
  spotifyLink?: string
  instagramLink?: string
  tags: ITag[]
}
