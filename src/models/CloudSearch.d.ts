import {
  IArtistFilterState,
  IDateFilter,
  ILocationFilterState,
  IPlaceFilterState,
} from './Filters'

interface ICloudSearchHitFields {
  address: string
  app_image: string
  artists_ids: string[]
  artists_names: string[]
  button_label: string
  calendar_event?: string
  change_monitor_name?: string
  city_id: string
  city_name: string
  city_slug?: string
  description_en: string
  description_pl: string
  empik_premium?: boolean
  end_date?: string
  event_category_id: string
  event_category_name: string
  event_slug: string
  geolocation: string
  has_pools: string
  horizontal_image: string
  location_ids: string[]
  locations_names: string[]
  partner_id: string
  partner_name: string
  partner_premium_level: string
  place_id: string
  place_name: string
  place_premium_level: string
  place_slug: string
  place_thumb?: string
  price: string
  price_description_pl?: string
  private_tags_ids: string[]
  private_tags_names: string[]
  public_tags_ids?: string[]
  public_tags_names?: string[]
  redirect_to_url?: string
  rundate_id: string
  rundate_not_for_sale: string
  rundate_rundate: string
  rundate_rundate_description?: string
  rundate_slug: string
  sales_channels_ids: string[]
  square_image: string
  title_en: string
  title_pl: string
}

export interface ICloudSearchHighlights {
  [key: string]: string | string[]
}

export interface ICloudSearchHit {
  id: string
  fields: ICloudSearchHitFields
  highlights: ICloudSearchHighlights
}

export interface ICloudSearchHits {
  found: number
  start: number
  hit: ICloudSearchHit[]
}

export interface ICloudSearchResponse {
  hits: ICloudSearchHits
}

export interface IQueryTerm {
  name: string
  value: (string | number)[]
}

export interface ISearchField {
  name: string
  boost: number
}

export interface IHighlight {
  name: string
  maxPhrases: number
}

export interface ICloudSearchOptions {
  phrase?: string
  andTerms?: IQueryTerm[]
  dates?: IDateFilter
  partnerName?: string
  place?: IPlaceFilterState
  location?: ILocationFilterState
  artist?: IArtistFilterState
  getHighlights?: boolean
  notTerms?: IQueryTerm[]
  returnValues?: string[]
  size?: number
  page?: number
  customSorting?: ICloudSearchSort[]
  cloudSearchUrl: string
  salesChannelId: number
}

export interface ICloudSearchSort {
  field: string
  priority: number
  sort: string
}
