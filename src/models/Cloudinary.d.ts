export interface ICloudinaryContext {
  custom: {
    placement: string
  }
}

export interface ICloudinaryImage {
  context: ICloudinaryContext
  created_at: string
  format: string
  height: number
  public_id: string
  type: string
  version: number
  width: number
}

export interface ICloudinaryList {
  resources: ICloudinaryImage[]
  updated_at: string
}
