import { ComponentType } from 'react'

import { BricksEnum } from '../constants/Compositions'
import { IGridItem } from '../ui/atoms/Grid/Grid'
import { ISectionAppProps } from '../ui/molecules/_compositions/Section/Section'
import { IDate } from './DateTime'
import { IDateTranslations } from './Locale'
import { IResolveUrl } from './Navigation'

export interface IComposition {
  sections: IPagesSections[]
  id: number
  slug?: string
  group: IPagesGroup
  metadata: IPagesMetadata | null
}

export interface IGroupCompositions {
  id: number
  slug: string
  label: string
}

export interface ICompositionGroup {
  id: number
  name: string
  slug: string
  pageCompositions: IGroupCompositions[]
  globalStylesUrl?: string
  buyPageStylesUrl?: string
  wordpressSourceUrl?: string
  additionalFooterText?: string
  isCalendar: boolean
  metadata: string
  partnerName?: string
}

export interface IParsedCompositionGroupMetadata {
  faviconUrl?: string
  title?: string
  description?: string
  ogImage?: string
  noIndex: 0 | 1
  ocKeywords: string[]
}

export interface IPagesSections {
  background: string
  backgroundImage: string | null
  components: ISectionsComponent[]
  contain: boolean
  id: number
  inRow: boolean
  mobileBackgroundImage: string | null
  slug?: string
  useGradient?: boolean
}

export interface IPagesMetadata {
  title: string | null
  description: string | null
  ogImage: string | null
  keywords: string[] | null
  noIndex: boolean
}

export interface IPagesGroup {
  id: number
  name: string | null
  slug: string | null
}

export interface IPagesTag {
  id: number
  isMain: boolean
  name: string
  slug: string
}

export interface ISectionsComponent {
  type: BricksEnum
  data: any[]
  extra?: any
  id: number
}

export interface ISectionComponentExtra {
  color?: string
  sectionDescription?: string
  sectionTitle?: string
}

interface IBrickConnection {
  component: ComponentType<any>
}

export interface IBricksConnection {
  [key: string]: IBrickConnection
}

export interface IBasicComponentData {
  title: string
  link?: string
}

export interface IAdditionalComponentData extends IBasicComponentData {
  date?: IDate
  dateDesc?: string
  description?: string
  thumbUrl: string
  tags?: IPagesTag[]
}

export interface ICompositionTranslations {
  seeMore: string
  openForm: string
}

export interface ICardBadge {
  name: string
  type?: string /* badge background color */
}

export interface ICardData extends IAdditionalComponentData {
  slug?: string
  badge?: ICardBadge
  place?: string
  place_slug?: string
  subtitle?: string
}

export interface ICardExtra extends ISectionComponentExtra {
  maxInRow: number
  moreButton: boolean
  moreButtonText?: string
  disableDate: boolean
  disableDescription: boolean
  disablePlace: boolean
  disableTitle: boolean
}

export interface IHtmlMarkdownProps extends IGridItem {
  uri?: string
  htmlString?: string
  markdownString?: string | null
  fontColor?: string
}

export type ISectionsComponentCommon = IGridItem &
  ISectionAppProps & {
    background?: string
    id: string
    resolveUrl: IResolveUrl
    dateTranslations: IDateTranslations
    compositionTranslations: ICompositionTranslations
    formIoAddress: string
    urlCloudinaryList: string
    cloudName: string
    urlApp: string
    isLegacySafari?: boolean
    markdownComponent: ComponentType<IHtmlMarkdownProps>
    wordpressHost?: string
  }
