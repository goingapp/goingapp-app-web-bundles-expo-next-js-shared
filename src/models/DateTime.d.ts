import { IDateTranslate } from './Locale'

export interface IDay {
  day: number
  selectable: boolean
  hash?: number
}

export interface IGetFriendlyDateProps {
  isoDate: string
  dateTranslate?: IDateTranslate
  isShort?: boolean
}

export interface IFormatRundateDatesProps {
  startDate?: IDate | string
  endDate?: IDate | string
  dateTranslate?: IDateTranslate
}

export type IMonth = IDay[][]

export interface IDate {
  day: string
  hour: string
  minutes: string
  month: string
  year: string
}
