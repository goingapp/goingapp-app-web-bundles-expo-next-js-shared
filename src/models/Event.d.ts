import { IArtist } from './Artist'
import { IPlace } from './Rundate'
import { ICategory } from './Tag'

export interface IEventImage {
  large: string
  medium: string
}

interface IPartner {
  id: number
  name: string
}

export interface ICity {
  name: string
  slug?: string
}

export interface IEvent {
  artists?: IArtist[]
  forFansOfArtists?: IArtist[]
  calendarEvent?: boolean
  category: ICategory
  description: string | null
  descriptionPL: string | null
  formattedDescription: string
  formattedPartnerDescription: string
  id: number
  images: IEventImage[]
  partner: IPartner
  partnerDescription: string
  place?: IPlace
  rundatesCount: number
  slug: string
  tags: ITag[]
  teaser: string
  title: string
  titlePL: string
}
