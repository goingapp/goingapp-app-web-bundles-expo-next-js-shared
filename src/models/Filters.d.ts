export interface IDateFilter {
  from: number | null
  to: number | null
}

export interface ILocationFilter {
  id: number
  name: string
  slug: string
}

export interface IPlaceFilter {
  name: string
}

export interface IArtistFilter {
  name: string
}

export type ILocationFilterState = ILocationFilter | null
export type IPlaceFilterState = IPlaceFilter | null
export type IArtistFilterState = IArtistFilter | null
