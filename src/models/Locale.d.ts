export interface IDateTranslate {
  locale: string
  today: string
  tomorrow: string
}

export interface IDateTranslations {
  calendarEvent: string
  locale: string
  today: string
  tomorrow: string
}

export interface IPriceTranslations {
  from: string
}

export interface IBuyButtonTranslations {
  disabledTooltip: string
  label: string
  labelSalesClosed: string
}

export interface ISearchTranslations {
  found: string
  events: string
  noResults: string
}

export interface IChevronButtonTranslations {
  close: string
  open: string
}

export interface IArticleTranslations {
  readArticle: string
  relatedTitle: string
}

export interface IDateSelectTranslations {
  clear: string
  date: string
  days: string[]
  filterName: string
  months: string[]
  selectDate: string
  addDate: string
  show: string
  thisMonth: string
  thisWeek: string
  today: string
  tomorrow: string
  weekend: string
}
