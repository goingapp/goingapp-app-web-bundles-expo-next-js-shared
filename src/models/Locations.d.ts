export interface ILocationCity {
  id: number
  slug: string
}

export interface IBasicLocation {
  id: number
  name: string
  slug: string
}

export interface ILocation extends IBasicLocation {
  cities: ILocationCity[]
  isDefault: boolean
}
