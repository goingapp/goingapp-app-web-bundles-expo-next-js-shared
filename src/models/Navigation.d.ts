export type IResolveUrl = (url: string) => Promise<void> | void
