export interface IPoolDescription {
  description: string
  ticketInfo: string | ''
  title: string
}

export interface IPool {
  active: boolean
  discountEnabled: boolean
  discountMaxTickets?: number
  hasDependency: boolean
  hasTransactionDataHydrator: boolean
  id: number
  isDetailedPurchaseRequired?: boolean
  onlyByShipment: boolean
  poolDescription: IPoolDescription | null
  price: number
  seatsIoCategories: [] // @TODO: use interface
  serviceFee: number
  sortOrder: string
  ticketsNumLimit: number
  vat: number
}

export interface IPoolsResponse {
  id: number
  isAvailable: boolean
  pools?: IPool[]
  runDate: string | null
  seatsIoPublicKey: string | null
  seatsio: ISeats[] | null
  ticketsNumLimit: number | null
}

export interface IPools extends IPoolsResponse {
  eventSlug: string
  rundateSlug?: string
}
