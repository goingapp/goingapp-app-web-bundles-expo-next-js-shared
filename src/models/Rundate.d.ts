import { QueueItId } from '../constants/Queue/QueueIt'
import { IDate } from './DateTime'
import { ICategory, ICity, IEvent, IPartner } from './Event'

export interface ICustomTerm {
  id: number
  link: string | null
  name: string
  required: boolean
  selectable: boolean
  text: string
}

export interface IPlace {
  address: string
  category: ICategory | null
  city: ICity
  description: string
  id: number
  lat: number
  lon: number
  name: string
  slug: string
  thumb?: string
}

export interface IRundate {
  buttonLabel?: string
  changeMonitorType?: IChangeMonitorType
  customTerms: ICustomTerm[]
  endDate?: IDate
  event: IEvent
  freeOfCharge: boolean
  hasManyPools: boolean
  id: number
  isAvailable: boolean
  isForPremiumUsers: boolean
  notForSale: boolean
  partner?: IPartner
  passed: boolean
  place?: IPlace
  premiumAuthUrl?: string
  price: number | null
  priceDescriptionPL: string
  redirectToUrl?: string
  rundateDescription: string
  shipmentAllowed: boolean
  slug: string
  startDate: IDate
  queue: QueueItId
  runDate?: IDate
}

export interface IChangeMonitorType {
  id: number
  name: string
  slug: string
}
