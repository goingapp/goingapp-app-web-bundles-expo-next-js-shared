export interface ITag {
  id: number | string
  name: string
  slug: string
}

export type ICategory = ITag
