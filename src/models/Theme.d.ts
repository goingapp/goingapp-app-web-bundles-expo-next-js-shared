import { TextStyle, ViewStyle } from 'react-native'

import { ButtonVariant, TypographyVariant } from '../constants/Variants'
import { ITag } from './Tag'

export interface IBreakpoints {
  xs: number
  sm: number
  md: number
  lg: number
  xl: number
}

export interface ITypography {
  [TypographyVariant.h1]: TextStyle
  [TypographyVariant.h2]: TextStyle
  [TypographyVariant.h2mobile]: TextStyle
  [TypographyVariant.h3]: TextStyle
  [TypographyVariant.h4]: TextStyle
  [TypographyVariant.h4mobile]: TextStyle
  [TypographyVariant.h5]: TextStyle
  [TypographyVariant.h6]: TextStyle
  [TypographyVariant.helperText]: TextStyle
  [TypographyVariant.subtitle1]: TextStyle
  [TypographyVariant.subtitle2]: TextStyle
  [TypographyVariant.overline]: TextStyle
  [TypographyVariant.body1]: TextStyle
  [TypographyVariant.body2]: TextStyle
  [TypographyVariant.caption]: TextStyle
  [TypographyVariant.buttonLarge]: TextStyle
  [TypographyVariant.buttonMedium]: TextStyle
  [TypographyVariant.buttonSmall]: TextStyle
  [TypographyVariant.tooltip]: TextStyle
  [TypographyVariant.underline]: (color: string) => TextStyle
}

export interface IButtons {
  [ButtonVariant.small]: ViewStyle
  [ButtonVariant.medium]: ViewStyle
  [ButtonVariant.large]: ViewStyle
  [ButtonVariant.roundedEdges]: ViewStyle
}

export interface SharedTheme {
  breakpoints: IBreakpoints
  buttons: IButtons
  spacing: ISpacingFunction
  typography: ITypography
}

type ISpacingFunction = (spacing?: number) => number

declare global {
  namespace ReactNativePaper {
    interface ThemeColors {
      black: string
      error: string
      info: string
      logo: string
      more: string
      primaryButtonText: string
      primaryDark: string
      primaryLight: string
      success: string
      mainTag: string
      textInverted: string
      textSecondary: string
      warning: string
      white: string
      categoryColor: (category?: ITag | string) => string
    }

    interface Theme extends SharedTheme {
      overrides: any
    }
  }
}
