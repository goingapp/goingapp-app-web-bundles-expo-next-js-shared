import {
  endOfDay,
  format,
  getMonth,
  isToday,
  isTomorrow,
  startOfDay,
} from 'date-fns'
import { enGB, pl } from 'date-fns/locale'

import { Texts } from '../../constants/Texts'
import { Ui } from '../../constants/Ui'
import { IEventV3 } from '../../models/ApiV3'
import { IDate, IFormatRundateDatesProps, IMonth } from '../../models/DateTime'
import { IGetFriendlyDateProps } from '../../models/DateTime'
import { IDateSelectTranslations } from '../../models/Locale'
import { capitalizeFirstLetter } from '../../utils/Helpers/capitalizeFirstLetter'

type MonthTimestamps = {
  from: number
  to: number
  name: string
}

export class DateTime {
  /**
   * Function converting date and time object to ISO date string
   *
   * @param {IDate} dateObject - date and time object
   *
   * @return {string} date-time ISO string
   */
  public static getIsoDateString = (dateObject: IDate): string => {
    const { year, month, day, hour, minutes } = dateObject

    return `${year}-${month}-${day}T${hour}:${minutes}:00`
  }

  /**
   * Converts date hash to ISO date string.
   *
   * @param {number} dateHash
   * @param {boolean} endDay - should include the day by it's end
   * @return {string}
   */
  public static getIsoFromHash = (
    dateHash: number,
    endDay?: boolean
  ): string => {
    const { year, month, day } = DateTime.dateHashToDateObject(dateHash)
    const hour = endDay ? '23' : '00'
    const minutes = endDay ? '59' : '00'

    return `${year}-${month + 1}-${day}T${hour}:${minutes}:00Z`
  }

  /**
   * Converts YYYY-MM-DD HH:MM date format to ISO
   *
   * @param {string} date - YYYY-MM-DD HH:MM format
   * @return {string} - ISO DATE STRING
   */
  public static convertToIso = (date: string) => date.replace(' ', 'T') + 'Z'

  public static checkIfDatesAreEqual(
    startDate: IDate | string,
    endDate: IDate | string
  ) {
    if (
      typeof startDate === 'string' &&
      typeof endDate === 'string' &&
      endDate.toString().split('T')[0] === startDate.toString().split('T')[0]
    ) {
      return true
    }

    return (
      typeof startDate !== 'string' &&
      typeof endDate !== 'string' &&
      startDate.day === endDate.day &&
      startDate.month === endDate.month &&
      startDate.year === endDate.year
    )
  }

  /**
   * Function translates ISO string date to human friendly format with localization
   *
   * @param {string} isoDate - ISO date string
   * @param {IDateTranslate} dateTranslate - date translation
   * @param isShort - should the date be in 'd.MM.yy' format
   *
   * @return {string} - human friendly localized date string
   */

  public static getFriendlyDate = ({
    isoDate,
    dateTranslate,
    isShort,
  }: IGetFriendlyDateProps): string => {
    const locale = dateTranslate?.locale === 'en' ? enGB : pl

    const _date = new Date(isoDate)
    const date = new Date(
      new Date(_date.valueOf() + _date.getTimezoneOffset() * 60 * 1000)
    )

    if (isToday(_date)) {
      return dateTranslate?.today || Texts.TODAY
    }

    if (isTomorrow(_date)) {
      return dateTranslate?.tomorrow || Texts.TOMORROW
    }

    return capitalizeFirstLetter(
      format(date, isShort ? 'd.MM.yy' : 'EEEE, d MMMM yyyy', {
        locale,
      })
    )
  }

  public static formatRundateDates = ({
    startDate,
    endDate,
    dateTranslate,
  }: IFormatRundateDatesProps) => {
    if (startDate) {
      const isoStart =
        typeof startDate === 'string'
          ? startDate
          : DateTime.getIsoDateString(startDate)
      const start = DateTime.getFriendlyDate({
        isoDate: isoStart,
        dateTranslate,
        isShort: true,
      })

      if (endDate && !this.checkIfDatesAreEqual(startDate, endDate)) {
        const isoEnd =
          typeof endDate === 'string'
            ? endDate
            : DateTime.getIsoDateString(endDate)

        return `${format(new Date(isoStart), 'd.MM.yy')} - ${format(
          new Date(isoEnd),
          'd.MM.yy'
        )}`
      }

      return start
    }

    return ''
  }

  public static getFriendlyTime = (dateObject: IDate): string => {
    return `${dateObject.hour}:${dateObject.minutes}`
  }

  /**
   * Converts date object to dash or dot format.
   *
   * @param {{day: number, month: number, year: number}} date
   * @param {boolean} withDash
   * @return {string}
   */
  public static formatDate = (
    date: {
      day: number
      month: number
      year: number
    },
    withDash?: boolean
  ): string => {
    const day = date.day.toString().padStart(2, '0')
    const month = (date.month + 1).toString().padStart(2, '0')

    return withDash
      ? `${date.year}-${month}-${day}`
      : `${day}.${month}.${date.year}`
  }

  /**
   * Function parses date hash number from date object.
   *
   * @param {number} year
   * @param {number} month
   * @param {number} day
   * @return {number}
   */
  public static makeDateHash = (
    year: number,
    month: number,
    day: number
  ): number => year * 10000 + month * 100 + day

  /**
   * Function splits date hash to date object.
   *
   * @param {number} dateHash
   * @return {{day: number, month: number, year: number}}
   */
  public static dateHashToDateObject = (
    dateHash: number
  ): { day: number; month: number; year: number } => {
    const str = dateHash.toString()

    return {
      day: parseInt(str.substr(6, 2), 10),
      month: parseInt(str.substr(4, 2), 10),
      year: parseInt(str.substr(0, 4), 10),
    }
  }

  /**
   * Returns today's date hash.
   *
   * @return {number}
   */
  public static today = (): number => {
    const date = new Date()
    const day = date.getDate()
    const month = date.getMonth()
    const year = date.getFullYear()

    return DateTime.makeDateHash(year, month, day)
  }

  /**
   * Returns tomorrow's date hash.
   *
   * @return {number}
   */
  public static tomorrow = (): number => {
    const date = new Date()
    let day = date.getDate()
    let month = date.getMonth()
    let year = date.getFullYear()

    const tomorrowDate = new Date(year, month, day + 1)

    day = tomorrowDate.getDate()
    month = tomorrowDate.getMonth()
    year = tomorrowDate.getFullYear()

    return DateTime.makeDateHash(year, month, day)
  }

  /**
   * Returns date hash array of nearest weekend date range.
   *
   * @return {number[]}
   */
  public static nearestWeekend = (): number[] => {
    const date = new Date()

    const whichDay = date.getDay()

    let day = date.getDate()
    let month = date.getMonth()
    let year = date.getFullYear()
    let add: number
    let daysToSunday = 2

    if (whichDay === 0) {
      add = 5
    } else if (whichDay === 6) {
      add = 0
      daysToSunday = 1
    } else {
      add = 5 - whichDay
    }

    const nextFriday = new Date(year, month, day + add)
    const nextSunday = new Date(year, month, day + add + daysToSunday)

    day = nextFriday.getDate()
    month = nextFriday.getMonth()
    year = nextFriday.getFullYear()

    const friday = DateTime.makeDateHash(year, month, day)

    day = nextSunday.getDate()
    month = nextSunday.getMonth()
    year = nextSunday.getFullYear()

    const sunday = DateTime.makeDateHash(year, month, day)

    return [friday, sunday]
  }

  /**
   * Returns date hash array of this month date range.
   *
   * @return {number[]}
   */
  public static thisMonth = (): number[] => {
    const date = new Date()

    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1)

    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0)

    let day = firstDay.getDate()
    let month = firstDay.getMonth()
    let year = firstDay.getFullYear()

    const first = DateTime.makeDateHash(year, month, day)

    day = lastDay.getDate()
    month = lastDay.getMonth()
    year = lastDay.getFullYear()

    const last = DateTime.makeDateHash(year, month, day)

    return [first, last]
  }

  /**
   * Returns date hash array of this week date range.
   *
   * @return {number[]}
   */
  public static thisWeek = () => {
    const date = new Date()
    const weekdayNumber = date.getDay()
    const currentDay = date.getDate()
    const currentMonth = date.getMonth()
    const currentYear = date.getFullYear()

    const firstWeekDay = new Date(
      currentYear,
      currentMonth,
      currentDay - (weekdayNumber - 1)
    )
    const lastWeekDay = new Date(
      currentYear,
      currentMonth,
      currentDay + (7 - weekdayNumber)
    )

    const firstDay = DateTime.calculateHashFromDate(firstWeekDay)
    const lastDay = DateTime.calculateHashFromDate(lastWeekDay)

    return [firstDay, lastDay]
  }

  /**
   * Returns date hash calculated from input Date.
   *
   * @param inputDate
   * @return {number}
   */
  public static calculateHashFromDate = (inputDate: Date) => {
    const day = inputDate.getDate()
    const month = inputDate.getMonth()
    const year = inputDate.getFullYear()

    return DateTime.makeDateHash(year, month, day)
  }

  /**
   * Returns calendar array of days for a specific month.
   *
   * @param {number} month
   * @param {number} year
   * @param {number} currentDateHash
   * @return {IMonth}
   */
  public static getCalendarArray = (
    month: number,
    year: number,
    currentDateHash: number
  ): IMonth => {
    const arr: IMonth = []

    let i
    let j

    for (i = 0; i < Ui.calendarRows; i++) {
      arr[i] = []
    }

    const info = DateTime.getMonthInfo(month, year)

    const start = info.startsAt
    const stop = info.startsAt + info.length

    const daysFromMonthBeforeArray = DateTime.getPastMonthDays(
      month,
      year,
      start
    )
    j = daysFromMonthBeforeArray.length

    for (i = 0; i < j; i++) {
      arr[0][i] = {
        day: daysFromMonthBeforeArray[i],
        selectable: false,
      }
    }

    const noDayHash = DateTime.makeDateHash(year, month, 0)

    for (i = start, j = 1; i < stop; ++i, ++j) {
      const indexRow: number = (i / Ui.calendarCols) >> 0
      const indexColumn: number = i % Ui.calendarCols

      const hash = noDayHash + j

      arr[indexRow][indexColumn] = {
        day: j,
        hash,
        selectable: hash >= currentDateHash,
      }
    }

    for (j = 1; i < Ui.calendarCols * Ui.calendarRows; ++i, ++j) {
      const indexRow: number = (i / Ui.calendarCols) >> 0
      const indexColumn: number = i % Ui.calendarCols

      arr[indexRow][indexColumn] = {
        day: j,
        selectable: false,
      }
    }

    return arr
  }

  /**
   * Returns length and start of a specific month.
   *
   * @param {number} month
   * @param {number} year
   * @return {{length: number, startsAt: number}}
   */
  private static getMonthInfo = (
    month: number,
    year: number
  ): {
    length: number
    startsAt: number
  } => {
    return {
      length: 32 - new Date(year, month, 32).getDate(),
      startsAt: (new Date(year, month, 1).getDay() + 6) % 7,
    }
  }

  /**
   * Returns past month's days array.
   *
   * @param {number} month (1-12)
   * @param {number} year
   * @param {number} startsAt
   * @return {number[]}
   */
  private static getPastMonthDays = (
    month: number,
    year: number,
    startsAt: number
  ): number[] => {
    const info = DateTime.getMonthInfo(month - 1, year)
    const arr = []

    let end = info.length

    for (let i = startsAt - 1; i >= 0; --i, --end) {
      arr.push(end)
    }

    return arr.reverse()
  }

  /**
   * Takes an array of IEventV3 (ApiV3) events and splits them into year, month or day "buckets": objects containing
   * an array of events and key property (year, month, day). Also handles undefined response.
   *
   * @param {IEventV3 | undefined} inputArray
   * @param {'year' | 'month' | 'day'} part
   *
   * @return Array<{key: string, events: IEventV3[]}>
   */

  public static splitApiV3EventsIntoDateBuckets = (
    inputArray: IEventV3[] | undefined,
    part: 'year' | 'month' | 'day'
  ) => {
    if (!inputArray) return []

    let datePartIndex = 0
    const outputArray: Array<{ key: string; events: IEventV3[] }> = []

    if (part === 'month') datePartIndex = 1
    if (part === 'day') datePartIndex = 2

    inputArray
      .map((event) => {
        const key =
          event.rundate.rundate &&
          event.rundate.rundate.split('-')[datePartIndex].split(' ')[0]

        if (!key) return

        const existingIndex = outputArray.findIndex((item) => item.key === key)

        if (existingIndex > -1) outputArray[existingIndex].events.push(event)
        else {
          outputArray.push({
            key,
            events: [event],
          })
        }
      })
      .filter(Boolean)

    return outputArray.sort(
      (current, next) => parseInt(current.key, 10) - parseInt(next.key, 10)
    )
  }

  public static getMonthNameFromDate = (
    date: Date,
    dateSelectTranslations: IDateSelectTranslations
  ) => dateSelectTranslations.months[getMonth(date)].toLowerCase()

  /**
   * Takes in month number (0-11) Returns date hash array of a certain month date range.
   *
   * @return {number[]}
   */
  public static certainMonth = (monthParam: number) => {
    const date = new Date()
    const currentMonth = date.getMonth()
    const isNextYearMonth = monthParam < currentMonth
    const yearFromMonthParam = isNextYearMonth
      ? date.getFullYear() + 1
      : date.getFullYear()
    const isLastMonth = monthParam === 12

    const firstDay = new Date(yearFromMonthParam, monthParam, 1)
    const lastDay = new Date(
      isLastMonth ? yearFromMonthParam + 1 : yearFromMonthParam,
      monthParam + 1,
      0
    )

    let day = firstDay.getDate()
    let month = firstDay.getMonth()
    let year = firstDay.getFullYear()

    const first = DateTime.makeDateHash(year, month, day)

    day = lastDay.getDate()
    month = lastDay.getMonth()
    year = lastDay.getFullYear()

    const last = DateTime.makeDateHash(year, month, day)

    return [first, last]
  }

  public static generateMonthsWithTimestamps = (
    range: { min: number; max: number },
    i18n: { [p: string]: any }
  ): Record<string, MonthTimestamps> => {
    if (range.min === 0 && range.max === 0) {
      return {}
    }
    const currentYear = new Date().getFullYear()
    const months: Record<string, MonthTimestamps> = {}

    const minDate = new Date(range.min)
    const maxDate = new Date(range.max)

    const minMonth = minDate.getMonth()
    const maxMonth = maxDate.getMonth()

    const minYear = minDate.getFullYear()
    const maxYear = maxDate.getFullYear()

    const currentMonth = new Date().getMonth()

    for (let monthIndex = 0; monthIndex < 12; monthIndex++) {
      const isCurrentYear = currentMonth <= monthIndex
      const monthStart = new Date(
        isCurrentYear ? currentYear : currentYear + 1,
        monthIndex,
        1
      )
      const monthEnd = new Date(
        isCurrentYear ? currentYear : currentYear + 1,
        monthIndex + 1,
        1
      )
      monthEnd.setMilliseconds(-1)

      if (
        (monthIndex >= minMonth && monthIndex <= maxMonth) ||
        (maxYear > minYear && monthIndex <= maxMonth + 12)
      ) {
        const startTimestamp =
          monthStart.getTime() < range.min ? range.min : monthStart.getTime()
        const endTimestamp =
          monthEnd.getTime() > range.max ? range.max : monthEnd.getTime()

        months[
          isCurrentYear ? monthIndex.toString() : (monthIndex + 12).toString()
        ] = {
          from: startTimestamp,
          to: endTimestamp,
          name: i18n.dateSelect.months[monthIndex],
        }
      }
    }

    return months
  }
}
