import { Logger } from '../../utils/Logger'

export class Fetcher {
  public static get = async (url: string) => {
    try {
      const data = await fetch(url)

      return data.json()
    } catch (error: any) {
      Logger.error('Fetcher', error.message)
    }
  }

  public static getText = async (url: string) => {
    try {
      const data = await fetch(url)

      return data.text()
    } catch (error: any) {
      Logger.error('Fetcher', error.message)
    }
  }

  public static getFile = async (url: string) => {
    try {
      const data = await fetch(url, {
        mode: 'no-cors',
      })

      return data.blob()
    } catch (error: any) {
      Logger.error('Fetcher', error.message)
    }
  }
}
