import { IGridItem } from '../../atoms/Grid/Grid'

export interface ICardDescriptionProps extends IGridItem {
  background?: string
  numOfLines: number
  text: string
}
