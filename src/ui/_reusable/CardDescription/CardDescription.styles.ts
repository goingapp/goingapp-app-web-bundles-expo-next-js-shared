import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { isColorDark } from '../../../utils/Helpers/isColorDark'

export const useStyles = (background?: string) => {
  const isBgColorDark = isColorDark(background)
  const theme = useTheme()

  return StyleSheet.create({
    root: {
      color: background
        ? isBgColorDark
          ? theme.colors.white
          : theme.colors.black
        : theme.colors.text,
    },
  })
}
