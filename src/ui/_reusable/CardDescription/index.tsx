import { Typography } from '../../atoms/Typography'
import { ICardDescriptionProps } from './CardDescription'
import { useStyles } from './CardDescription.styles'

export const CardDescription = ({
  background,
  numOfLines,
  text,
  gridItemProps,
}: ICardDescriptionProps) => {
  const styles = useStyles(background)

  return (
    <Typography
      numberOfLines={numOfLines}
      children={text}
      style={styles.root}
    />
  )
}
