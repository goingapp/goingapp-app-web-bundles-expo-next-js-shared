import { StyleSheet } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants/Typography'
import { IChevronButtonTranslations } from '../../../models/Locale'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IChevronButtonProps extends IGridItem {
  action: () => void
  style?: StyleSheet.NamedStyles
  isOpen: boolean
  withDecoration?: boolean
  variant?: TypographyVariant
  isMobile: boolean
  chevronButtonTranslations: IChevronButtonTranslations
}
