import { StyleSheet } from 'react-native'

export const useStyles = (isMobile: boolean) =>
  StyleSheet.create({
    root: {
      alignItems: 'center',
      flexDirection: 'row',
      flex: 1,
      justifyContent: isMobile ? 'center' : 'flex-start',
      margin: 0,
      padding: 0,
    },
  })
