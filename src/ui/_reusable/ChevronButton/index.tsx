import { TouchableOpacity } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants'
import { Icon } from '../../atoms/Icon'
import { Typography } from '../../atoms/Typography'
import { IChevronButtonProps } from './ChevronButton'
import { useStyles } from './ChevronButton.styles'

export const ChevronButton = ({
  action,
  isOpen,
  style,
  withDecoration = true,
  variant = TypographyVariant.buttonMedium,
  gridItemProps,
  isMobile,
  chevronButtonTranslations,
}: IChevronButtonProps) => {
  const styles = useStyles(isMobile)

  return (
    <TouchableOpacity onPress={action} style={[styles.root, style]}>
      <Typography
        withDecoration={withDecoration}
        children={
          isOpen
            ? chevronButtonTranslations.close
            : chevronButtonTranslations.open
        }
        variant={variant}
      />
      <Icon name={isOpen ? 'menu-up' : 'menu-down'} />
    </TouchableOpacity>
  )
}
