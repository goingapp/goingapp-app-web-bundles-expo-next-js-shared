import { aspectRatio } from '../../../constants/AspectRatio'
import { makeStyles } from '../../../utils/Helpers/makeStyles'
import { IStyles } from './GalleryPhotoPlaceholder'

export const useStyles = makeStyles<{ isDesktop?: boolean }, IStyles>(
  (theme, props) => {
    return {
      placeholder: {
        borderRadius: 16,
        backgroundColor: theme.colors.primary,
        aspectRatio: props?.isDesktop
          ? aspectRatio.gallerySmallImage[0] / aspectRatio.gallerySmallImage[1]
          : aspectRatio.default[0] / aspectRatio.default[1],
        justifyContent: 'center',
        alignItems: 'center',
      },
      remainingPhotos: {
        color: theme.colors.black,
      },
    }
  }
)
