import { StyleProp } from 'react-native'

import { IGridItem } from '../../atoms/Grid/Grid'

export interface IStyles {
  placeholder: StyleProp
  remainingPhotos: StyleProp
}

export interface IGalleryPhotoPlaceholderProps extends IGridItem {
  moreImages?: number
  imagesCount?: number
  isDesktop?: boolean
}
