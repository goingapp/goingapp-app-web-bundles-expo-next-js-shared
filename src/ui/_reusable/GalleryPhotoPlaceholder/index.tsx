import { View } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants'
import { Grid } from '../../atoms/Grid'
import { Typography } from '../../atoms/Typography'
import { IGalleryPhotoPlaceholderProps } from './GalleryPhotoPlaceholder'
import { useStyles } from './GalleryPhotoPlaceHolder.styles'

export const GalleryPhotoPlaceholder = ({
  moreImages,
  imagesCount,
  gridItemProps,
  isDesktop,
}: IGalleryPhotoPlaceholderProps) => {
  const styles = useStyles({ isDesktop })

  return (
    <Grid gridItemProps={gridItemProps} flex={1}>
      <View style={styles.placeholder}>
        {moreImages && !!imagesCount && (
          <Typography
            style={styles.remainingPhotos}
            variant={TypographyVariant.h5}
          >{`+ ${imagesCount - 3}`}</Typography>
        )}
      </View>
    </Grid>
  )
}
