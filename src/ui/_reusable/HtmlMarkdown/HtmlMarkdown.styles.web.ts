// import { useTheme } from 'react-native-paper'
//
// import { SPACING } from '../../../constants/Grid'
//
// export const useStyles = (
//   fontColor?: string
// ): {
//   link: React.CSSProperties
//   youTubeEmbed: React.CSSProperties
//   youTubeWrapper: React.CSSProperties
//   image: React.CSSProperties
//   root: React.CSSProperties
// } => {
//   const { colors, spacing } = useTheme()
//   const color = fontColor || colors.text
//
//   return {
//     link: { textDecoration: 'none', color: colors.primary },
//     youTubeEmbed: {
//       width: '100%',
//       position: 'absolute',
//       height: '100%',
//       left: 0,
//       top: 0,
//     },
//     youTubeWrapper: {
//       paddingBottom: '56.25%',
//       height: 0,
//       position: 'relative',
//     },
//     image: {
//       width: '100%',
//     },
//     root: {
//       color,
//       marginBottom: spacing(SPACING.REGULAR),
//     },
//   }
// }
