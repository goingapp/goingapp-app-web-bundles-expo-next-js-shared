// import ReactMarkdown from 'react-markdown'
// import rehypeRaw from 'rehype-raw'
//
// import { cssClasses } from '../../../constants/CSS'
// import { Grid } from '../../atoms/Grid'
// import { IHtmlMarkdownProps } from './HtmlMarkdown'
// import { useCustomComponents } from './useCustomComponents'
//
// export const HtmlMarkdown = ({
//   htmlString,
//   markdownString,
//   gridItemProps,
//   fontColor,
//   isMobile,
// }: IHtmlMarkdownProps) => {
//   const { mappedComponents } = useCustomComponents({
//     fontColor,
//     isMobile,
//   })
//
//   return (
//     <Grid dataSet={cssClasses.htmlMarkdown} gridItemProps={gridItemProps}>
//       <ReactMarkdown
//         rehypePlugins={[rehypeRaw]}
//         children={htmlString || markdownString || ''}
//         components={mappedComponents}
//       />
//     </Grid>
//   )
// } TODO: move to partner-profile
