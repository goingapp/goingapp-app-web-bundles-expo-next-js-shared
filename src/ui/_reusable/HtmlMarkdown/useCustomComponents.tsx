// import { ReactNode } from 'react'
//
// import { TypographyVariant } from '../../../constants/Variants'
// import { Typography } from '../../atoms/Typography'
// import { IMarkdownTag } from './HtmlMarkdown'
// import { useStyles } from './HtmlMarkdown.styles.web'
//
// export const useCustomComponents = ({
//   fontColor,
//   isMobile,
// }: {
//   fontColor?: string
//   isMobile: boolean
// }) => {
//   const webStyles = useStyles(fontColor)
//
//   const mappedComponents: { [key: string]: ({}: IMarkdownTag) => ReactNode } = {
//     a: ({ node, ...props }) => (
//       // @ts-ignore
//       <a {...props} style={webStyles.link} />
//     ),
//     h1: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.h1}
//       />
//     ),
//     h2: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.h2}
//       />
//     ),
//     h3: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={isMobile ? TypographyVariant.h4mobile : TypographyVariant.h3}
//       />
//     ),
//     h4: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.h4}
//       />
//     ),
//     h5: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.h5}
//       />
//     ),
//     h6: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.h6}
//       />
//     ),
//     p: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.body2}
//       />
//     ),
//     figcaption: ({ node, ...props }) => (
//       <Typography
//         {...props}
//         style={webStyles.root}
//         variant={TypographyVariant.caption}
//       />
//     ),
//     iframe: ({ node, ...props }) => (
//       <div style={webStyles.youTubeWrapper}>
//         <iframe {...props} style={webStyles.youTubeEmbed} />
//       </div>
//     ),
//     img: ({ node, ...props }) => <img {...props} style={webStyles.image} />,
//   }
//
//   return {
//     mappedComponents,
//   }
// }
