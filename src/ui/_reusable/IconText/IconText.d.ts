import { TypographyVariant } from '../../../constants/Variants/Typography'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IIconTextProps extends IGridItem {
  text: string
  iconName: string
  iconSize?: number
  variant?: TypographyVariant
  withDecoration?: boolean
  numberOfLines?: number
}
