import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'

export const useStyles = () => {
  const theme = useTheme()

  return StyleSheet.create({
    root: {
      alignItems: 'center',
      flexDirection: 'row',
      flexWrap: 'nowrap',
    },
    text: {
      marginLeft: theme.spacing(SPACING.REGULAR),
      flex: 1,
    },
  })
}
