import { View } from 'react-native'

import { useTheme } from 'react-native-paper'

import { TypographyVariant } from '../../../constants/Variants'
import { Icon } from '../../atoms/Icon'
import { Typography } from '../../atoms/Typography'
import { IIconTextProps } from './IconText'
import { useStyles } from './IconText.styles'

export const IconText = ({
  iconSize = 24,
  text,
  iconName,
  variant = TypographyVariant.body1,
  withDecoration,
  gridItemProps,
  numberOfLines,
}: IIconTextProps) => {
  const styles = useStyles()
  const theme = useTheme()

  return (
    <View style={styles.root}>
      <Icon name={iconName} size={iconSize} color={theme.colors.text} />
      <Typography
        style={styles.text}
        variant={variant}
        children={text}
        withDecoration={withDecoration}
        numberOfLines={numberOfLines}
      />
    </View>
  )
}
