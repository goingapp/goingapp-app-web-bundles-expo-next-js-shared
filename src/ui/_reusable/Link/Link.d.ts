import { ViewStyle } from 'react-native'

import { IResolveUrl } from '../../../models/Navigation'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface ILinkProps extends IGridItem {
  url: string
  nativeID?: string
  style?: ViewStyle
  resolveUrl: IResolveUrl
  flex?: number
}
