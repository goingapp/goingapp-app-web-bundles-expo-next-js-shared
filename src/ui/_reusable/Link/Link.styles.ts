import { StyleSheet } from 'react-native'

export const useStyles = () =>
  StyleSheet.create({
    root: {
      flex: 1,
    },
  })
