import { FC } from 'react'
import { TouchableOpacity } from 'react-native'

import { cssClasses } from '../../../constants/CSS'
import { Grid } from '../../atoms/Grid'
import { ILinkProps } from './Link'
import { useStyles } from './Link.styles'

export const Link: FC<ILinkProps> = ({
  children,
  gridItemProps,
  nativeID,
  url,
  resolveUrl,
  flex = 1,
  style,
}) => {
  const styles = useStyles()
  const handleLink = () => resolveUrl(url)

  return (
    <Grid
      gridItemProps={gridItemProps}
      dataSet={cssClasses.linkComponent}
      nativeID={nativeID}
      flex={flex}
      containerStyle={style}
    >
      <TouchableOpacity onPress={handleLink} style={styles.root}>
        {children}
      </TouchableOpacity>
    </Grid>
  )
}
