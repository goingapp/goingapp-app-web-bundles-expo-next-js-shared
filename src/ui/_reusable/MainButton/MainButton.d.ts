import { StyleSheet } from 'react-native'

import { ButtonVariant } from '../../../constants/Variants'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IMainButtonProps extends IGridItem {
  action: () => void
  disabled?: boolean
  fullWidth?: boolean
  icon?: string
  label: string
  outlined?: boolean
  secondary?: boolean
  style?: StyleSheet.NamedStyles
  textStyle?: StyleSheet.NamedStyles
  variant?: ButtonVariant
  textTransform?: 'none' | 'capitalize' | 'uppercase' | 'lowercase'
}
