import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'
import { ButtonVariant } from '../../../constants/Variants'

export const useStyles = (
  variant: ButtonVariant = ButtonVariant.large,
  outlined?: boolean,
  secondary?: boolean,
  fullWidth?: boolean,
  textTransform?: string
) => {
  const { buttons, colors, spacing } = useTheme()
  const backgroundColor = secondary ? colors.text : colors.primary
  const textColor = secondary ? colors.background : colors.primaryButtonText

  return StyleSheet.create({
    root: {
      alignItems: 'center',
      backgroundColor: outlined ? 'transparent' : backgroundColor,
      borderColor: outlined ? colors.text : backgroundColor,
      borderStyle: 'solid',
      borderWidth: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      paddingHorizontal: spacing(SPACING.REGULAR),
      width: fullWidth ? '100%' : undefined,
      ...buttons[variant],
    },
    text: {
      color: outlined ? colors.text : textColor,
      flex: 1,
      marginLeft: 'auto',
      marginRight: 'auto',
      position: 'relative',
      textAlign: 'center',
      textTransform,
    },
    disabled: {
      backgroundColor: colors.disabled,
      borderColor: 'transparent',
    },
    icon: {
      color: outlined ? colors.text : textColor,
    },
    textWithIcon: {
      left: -12, // half of icon width
      paddingHorizontal: spacing(SPACING.REGULAR),
    },
  })
}
