import { TouchableOpacity } from 'react-native'

import { ButtonVariant } from '../../../constants/Variants'
import { TypographyVariant } from '../../../constants/Variants'
import { Icon } from '../../atoms/Icon'
import { Typography } from '../../atoms/Typography'
import { IMainButtonProps } from './MainButton'
import { useStyles } from './MainButton.styles'

export const MainButton = ({
  action,
  disabled,
  fullWidth,
  icon,
  label,
  outlined,
  secondary,
  style,
  textStyle,
  variant,
  textTransform = 'uppercase',
}: IMainButtonProps) => {
  const styles = useStyles(
    variant,
    outlined,
    secondary,
    fullWidth,
    textTransform
  )

  const typographyMap = () => {
    switch (variant) {
      case ButtonVariant.small:
        return TypographyVariant.buttonSmall
      case ButtonVariant.medium:
        return TypographyVariant.buttonMedium
      case ButtonVariant.large:
        return TypographyVariant.buttonLarge
      default:
        return TypographyVariant.buttonLarge
    }
  }

  return (
    <TouchableOpacity
      style={[styles.root, disabled && styles.disabled, style]}
      onPress={action}
      disabled={disabled}
    >
      {icon && <Icon name={icon} style={[styles.icon, textStyle]} />}
      <Typography
        variant={typographyMap()}
        style={[styles.text, icon && styles.textWithIcon, textStyle]}
        children={label}
      />
    </TouchableOpacity>
  )
}
