import { IChevronButtonTranslations } from '../../../models/Locale'

export interface IPoolDescriptionProps {
  title?: string
  description?: string
  isMobile: boolean
  chevronButtonTranslations: IChevronButtonTranslations
}
