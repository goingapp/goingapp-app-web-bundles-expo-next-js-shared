import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'

export const useStyles = (isMobile: boolean) => {
  const { colors, spacing } = useTheme()

  return StyleSheet.create({
    root: {
      flex: 1,
      paddingHorizontal: spacing(SPACING.REGULAR),
      textAlign: isMobile ? 'center' : 'left',
    },
    title: {
      ...(isMobile && { marginBottom: spacing(SPACING.REGULAR) }),
      textAlign: isMobile ? 'center' : 'left',
    },
    description: {
      color: colors.textSecondary,
      ...(isMobile && { marginTop: spacing(SPACING.REGULAR) }),
    },
  })
}
