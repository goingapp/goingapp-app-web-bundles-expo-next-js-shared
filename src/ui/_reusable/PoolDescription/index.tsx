import { View } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants'
import { useToggle } from '../../../hooks/useToggle'
import { Typography } from '../../atoms/Typography'
import { ChevronButton } from '../ChevronButton'
import { IPoolDescriptionProps } from './PoolDescription'
import { useStyles } from './PoolDescription.styles'

export const PoolDescription = ({
  description,
  title,
  isMobile,
  chevronButtonTranslations,
}: IPoolDescriptionProps) => {
  const { isVisible, handleToggle } = useToggle()
  const styles = useStyles(isMobile)

  return (
    <View style={styles.root}>
      <Typography
        style={styles.title}
        variant={TypographyVariant.h5}
        children={title}
      />
      <ChevronButton
        action={handleToggle}
        isOpen={isVisible}
        withDecoration
        chevronButtonTranslations={chevronButtonTranslations}
        isMobile={isMobile}
      />
      {isVisible && (
        <Typography style={styles.description} children={description} />
      )}
    </View>
  )
}
