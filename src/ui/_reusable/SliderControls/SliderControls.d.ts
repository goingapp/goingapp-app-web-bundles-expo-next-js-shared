import { RefObject } from 'react'
import { FlatList } from 'react-native'

import { SPACING } from '../../../constants/Grid'
import { IGridItemElement } from '../../atoms/Grid/Grid'

export interface ISliderControlsProps {
  dataLength: number
  sliderRef: RefObject<FlatList>
  elementsInRow: number
  elementSpacing: SPACING
  isMobile: boolean
  isWeb: boolean
  children: IGridItemElement
  isLegacySafari?: boolean
}
