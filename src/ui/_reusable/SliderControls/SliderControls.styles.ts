import { StyleSheet } from 'react-native'

export const useStyles = () =>
  StyleSheet.create({
    arrowButtonContainer: {
      position: 'absolute',
      height: '100%',
      zIndex: 3,
    },
    leftArrowButton: {
      left: 0,
    },
    rightArrowButton: {
      right: 0,
    },
  })
