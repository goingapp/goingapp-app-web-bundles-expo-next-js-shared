import { RefObject, useState } from 'react'
import { FlatList } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'

export const useSliderControls = ({
  dataLength,
  ref,
  elementsInRow,
  elementSpacing,
  isMobile,
  isLegacySafari,
}: {
  dataLength: number
  ref: RefObject<FlatList>
  elementsInRow: number
  elementSpacing: SPACING
  isMobile: boolean
  isLegacySafari?: boolean
}) => {
  const [activeElem, setActiveElem] = useState(0)
  const theme = useTheme()

  const scrollToElement = (index: number) => {
    let safeIndex = index

    if (index > dataLength - 1) {
      safeIndex = 0
    }

    if (index < 0) {
      safeIndex = dataLength - 1
    }

    ref?.current?.scrollToIndex({
      index: safeIndex,
      viewOffset: safeIndex !== 0 ? -theme.spacing(elementSpacing) : undefined,
      animated: !isLegacySafari,
    })
    setActiveElem(safeIndex)
  }
  const scrollToNext = () =>
    (!isMobile && activeElem < dataLength - elementsInRow) ||
    (isMobile && activeElem < dataLength - 1)
      ? scrollToElement(activeElem + elementsInRow)
      : scrollToElement(0)
  const scrollToPrevious = () =>
    activeElem > 0
      ? scrollToElement(activeElem - elementsInRow)
      : scrollToElement(isMobile ? dataLength - 1 : dataLength - elementsInRow)

  return {
    scrollToNext,
    scrollToPrevious,
  }
}
