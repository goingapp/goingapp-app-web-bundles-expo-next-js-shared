import { FC } from 'react'

import { IconButton } from 'react-native-paper'

import { cssClasses } from '../../../constants/CSS'
import { Grid } from '../../atoms/Grid'
import { useSliderControls } from './hooks/useSliderControls'
import { ISliderControlsProps } from './SliderControls'
import { useStyles } from './SliderControls.styles'

export const SliderControls: FC<ISliderControlsProps> = ({
  dataLength,
  sliderRef,
  children,
  elementsInRow,
  elementSpacing,
  isMobile,
  isWeb,
  isLegacySafari,
}) => {
  const styles = useStyles()
  const { scrollToNext, scrollToPrevious } = useSliderControls({
    dataLength,
    ref: sliderRef,
    elementsInRow,
    elementSpacing,
    isMobile,
    isLegacySafari,
  })

  return (
    <Grid rowDirection={true} spacing={0}>
      <Grid
        containerStyle={[
          styles.leftArrowButton,
          isMobile && !isWeb ? styles.arrowButtonContainer : {},
        ]}
        justifyContent={'center'}
        dataSet={cssClasses.carouselArrowLeft}
      >
        <IconButton
          touchSoundDisabled={true}
          icon={'chevron-left'}
          onPress={scrollToPrevious}
        />
      </Grid>
      {children}
      <Grid
        containerStyle={[
          styles.rightArrowButton,
          isMobile && !isWeb ? styles.arrowButtonContainer : {},
        ]}
        justifyContent={'center'}
        dataSet={cssClasses.carouselArrowRight}
      >
        <IconButton
          touchSoundDisabled={true}
          icon={'chevron-right'}
          onPress={scrollToNext}
        />
      </Grid>
    </Grid>
  )
}
