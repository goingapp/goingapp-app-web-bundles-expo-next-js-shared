import { IGridItem } from '../../atoms/Grid/Grid'

export interface ISmallGalleryImageProps extends IGridItem {
  url?: string
  isDesktop?: boolean
}
