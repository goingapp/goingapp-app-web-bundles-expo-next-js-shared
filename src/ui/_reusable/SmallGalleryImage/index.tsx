import { aspectRatio } from '../../../constants/AspectRatio'
import { Grid } from '../../atoms/Grid'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { GalleryPhotoPlaceholder } from '../GalleryPhotoPlaceholder'
import { ISmallGalleryImageProps } from './SmallGalleryImage'

export const SmallGalleryImage = ({
  url,
  gridItemProps,
  isDesktop,
}: ISmallGalleryImageProps) => {
  if (!url) {
    return <GalleryPhotoPlaceholder gridItemProps={gridItemProps} />
  }

  return (
    <Grid flex={1} gridItemProps={gridItemProps}>
      <ResponsiveImage
        aspectRatio={
          isDesktop ? aspectRatio.gallerySmallImage : aspectRatio.default
        }
        imageUrl={url}
        flex={isDesktop ? 0 : 1}
        borderRadius={16}
      />
    </Grid>
  )
}
