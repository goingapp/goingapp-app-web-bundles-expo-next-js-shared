import { useCallback, useMemo, useState } from 'react'

import { useTheme } from 'react-native-paper'
import YoutubePlayer from 'react-native-youtube-iframe'

import { SPACING } from '../../../constants/Grid'
import { Grid } from '../../atoms/Grid'
import { IYouTubeProps } from './models/YouTube'

export const YouTube = ({
  teaser,
  padding = SPACING.LARGE,
  gridItemProps,
  style,
  containerWidth,
  isHideControls,
}: IYouTubeProps) => {
  const [playing, setPlaying] = useState(false)
  const { spacing } = useTheme()
  const paddingWidth = spacing(padding) * 2
  const height = useMemo(
    () => ((containerWidth - paddingWidth) / 16) * 9,
    [containerWidth, paddingWidth]
  )
  const initialPlayerParams = isHideControls
    ? { controls: false, modestbranding: true }
    : undefined

  const onStateChange = useCallback((state) => {
    if (state === 'ended') {
      setPlaying(false)
    }
  }, [])

  return (
    <Grid gridItemProps={gridItemProps} flex={1} containerStyle={style}>
      <YoutubePlayer
        height={height}
        width={containerWidth - paddingWidth}
        play={playing}
        videoId={teaser.split('/').pop() ?? ''}
        onChangeState={onStateChange}
        initialPlayerParams={initialPlayerParams}
      />
    </Grid>
  )
}
