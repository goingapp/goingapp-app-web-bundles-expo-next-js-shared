import { IYouTubeProps } from './models/YouTube'

export const YouTube = ({ teaser }: IYouTubeProps) => {
  const link = teaser.replace('youtube', 'youtube-nocookie')
  const hideInfo = '?rel=0&modestbranding=1&autohide=1&showinfo=0'
  const allow =
    'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'

  return (
    <div>
      <iframe
        allowFullScreen
        allow={allow}
        frameBorder={'0'}
        height={'315'}
        src={link + hideInfo}
        title={'YT'}
        width={'560'}
      />
    </div>
  )
}
