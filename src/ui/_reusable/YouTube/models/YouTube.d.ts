import { ViewStyle } from 'react-native'

import { SPACING } from '../../../../constants/Grid'
import { IGridItem } from '../../../atoms/Grid/Grid'

export interface IYouTubeProps extends IGridItem {
  padding?: SPACING | 0
  teaser: string
  style?: ViewStyle | ViewStyle[]
  containerWidth: number
  isHideControls?: boolean
}
