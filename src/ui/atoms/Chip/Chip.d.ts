import { StyleSheet } from 'react-native'

import { ITag } from '../../../models/Tag'
import { IGridItem } from '../Grid/Grid'

export interface IChipProps extends IGridItem {
  label: string
  outlined?: boolean
  style?: StyleSheet.NamedStyles
  textStyle?: StyleSheet.NamedStyles
  category?: ITag | string
  onPress?: () => void
}
