import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { ITag } from '../../../models/Tag'

export const useStyles = (category?: ITag | string) => {
  const { colors } = useTheme()

  return StyleSheet.create({
    chip: {
      borderRadius: 4,
    },
    chipText: {
      marginHorizontal: 0,
      lineHeight: 16,
      minHeight: 16,
    },
    fill: {
      backgroundColor: colors.categoryColor(category),
      color: colors.white,
    },
  })
}
