import { Chip as PaperChip } from 'react-native-paper'

import { IChipProps } from './Chip'
import { useStyles } from './Chip.styles'

export const Chip = ({
  label,
  outlined,
  style,
  textStyle,
  gridItemProps,
  category,
  onPress,
}: IChipProps) => {
  const styles = useStyles(category)

  return (
    <PaperChip
      textStyle={[styles.chipText, textStyle]}
      style={[styles.chip, !outlined && styles.fill, style]}
      mode={outlined ? 'outlined' : 'flat'}
      children={label}
      onPress={onPress}
    />
  )
}
