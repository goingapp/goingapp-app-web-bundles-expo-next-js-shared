import { StyleSheet } from 'react-native'

export interface IFilterChipProps {
  action: () => void
  active: boolean
  title: string
  clearAction: () => void
  style?: StyleSheet.NamedStyles
}
