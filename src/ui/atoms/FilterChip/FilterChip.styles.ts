import { StyleSheet } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants'

export const useStyles = (theme: ReactNativePaper.Theme) =>
  StyleSheet.create({
    root: {
      backgroundColor: theme.colors.surface,
      borderRadius: 4,
      width: 'fit-content',
      fontFamily: theme.typography[TypographyVariant.buttonSmall].fontFamily,
    },
  })
