import { Chip, useTheme } from 'react-native-paper'

import { IFilterChipProps } from './FilterChip'
import { useStyles } from './FilterChip.styles'

/* TODO: update it along with the whole Search refactor */

export const FilterChip = ({
  action,
  active,
  clearAction,
  style,
  title,
}: IFilterChipProps) => {
  const theme = useTheme()
  const styles = useStyles(theme)

  return (
    <Chip
      style={[style, styles.root]}
      children={title}
      onClose={active ? clearAction : undefined}
      onPress={action}
    />
  )
}
