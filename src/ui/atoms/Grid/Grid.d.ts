import { LayoutChangeEvent, ViewStyle } from 'react-native'

import { SPACING } from '../../../constants/Grid'
import {
  StyleFlexAlignItems,
  StyleFlexJustifyContent,
} from '../../../models/Styles'

export interface IGridItemProps {
  isFirst?: boolean
  rowDirection?: boolean
  spacing?: SPACING
  wrap?: boolean
}

export interface IGridItem {
  gridItemProps?: IGridItemProps
}

export interface IGridContainerProps {
  alignItems?: StyleFlexAlignItems
  borderRadius?: boolean
  flex?: number
  isWeb?: boolean
  justifyContent?: StyleFlexJustifyContent
  nativeID?: string
  padding?: SPACING
  rowDirection?: boolean
  spacing?: SPACING
  withBackground?: boolean
  wrap?: boolean
}

type IGridItemElement =
  | JSX.Element
  | undefined
  | string
  | number
  | boolean
  | null

export interface IGridProps extends IGridContainerProps, IGridItem {
  children: IGridItemElement | IGridItemElement[]
  containerStyle?: ViewStyle | (ViewStyle | false)[] | false
  dataSet?: { cssClass: string }
  onLayout?: (event: LayoutChangeEvent) => void
}
