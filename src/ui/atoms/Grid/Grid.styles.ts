import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { IGridContainerProps } from './Grid'

export const useStyles = ({
  alignItems,
  borderRadius,
  flex,
  justifyContent,
  padding,
  rowDirection,
  spacing = 0,
  withBackground,
  wrap,
  isWeb,
}: IGridContainerProps) => {
  const theme = useTheme()

  return StyleSheet.create({
    container: {
      borderRadius: borderRadius ? theme.spacing(4) : undefined,
      flexDirection: rowDirection ? 'row' : 'column',
      flexWrap: wrap ? 'wrap' : 'nowrap',
      overflow: 'hidden',
      position: 'relative',
      ...(flex && { flex }),
      ...(alignItems && { alignItems }),
      ...(withBackground && { backgroundColor: theme.colors.surface }),
      ...(justifyContent && { justifyContent }),
      ...(padding && { padding: theme.spacing(padding) }),
      ...(spacing && { gap: theme.spacing(spacing) + 'px' }),
      ...(rowDirection && {
        ...(isWeb && {
          width: wrap ? `calc(100% + ${theme.spacing(spacing)}px)` : '100%',
        }),
      }),
    },
  })
}
