import { Children, cloneElement } from 'react'
import { View } from 'react-native'

import { IGridProps } from './Grid'
import { useStyles } from './Grid.styles'

export const Grid = ({
  alignItems,
  borderRadius,
  children,
  containerStyle,
  flex,
  justifyContent,
  nativeID,
  padding,
  rowDirection,
  spacing,
  withBackground,
  wrap,
  dataSet,
  onLayout,
}: IGridProps) => {
  const styles = useStyles({
    alignItems,
    borderRadius,
    flex,
    justifyContent,
    padding,
    rowDirection,
    spacing,
    withBackground,
    wrap,
  })

  const items = Children.map(children, (child, index) => {
    if (
      typeof child === 'undefined' ||
      typeof child === 'string' ||
      typeof child === 'number' ||
      typeof child === 'boolean'
    ) {
      return child
    }

    return child
      ? cloneElement(child, {
          gridItemProps: { spacing, rowDirection, wrap, isFirst: index === 0 },
        })
      : child
  })?.filter(Boolean)

  return (
    <View
      style={[styles.container, containerStyle && containerStyle]}
      // @ts-ignore: this is a new prop and is not yet supported by types for react-native-web
      dataSet={dataSet}
      nativeID={nativeID}
      onLayout={onLayout}
    >
      {items}
    </View>
  )
}
