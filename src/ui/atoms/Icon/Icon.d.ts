import { StyleSheet } from 'react-native'

import { MaterialCommunityIcons } from '@expo/vector-icons'

import { IGridItem } from '../Grid/Grid'

export interface IIconProps extends IGridItem {
  color?: string
  name: MaterialCommunityIcons
  size?: number
  style?: StyleSheet.NamedStyles
}
