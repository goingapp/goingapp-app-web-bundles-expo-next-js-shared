import { MaterialCommunityIcons } from '@expo/vector-icons'
import { useTheme } from 'react-native-paper'

import { IIconProps } from './Icon'

const newFont = MaterialCommunityIcons.font['material-community'].replace(
  '/_next',
  '/more/_next'
)
MaterialCommunityIcons.font = {
  'material-community': newFont,
}

export const Icon = ({
  name,
  size = 24,
  color,
  style,
  gridItemProps,
}: IIconProps) => {
  const theme = useTheme()

  return (
    <MaterialCommunityIcons
      name={name}
      size={size}
      color={color || theme.colors.text}
      style={style}
    />
  )
}
