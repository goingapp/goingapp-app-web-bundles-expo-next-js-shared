import { IGridItem } from '../Grid/Grid'

export interface ILabeledIconButtonProps extends IGridItem {
  action: () => void
  label: string
  isDisabled?: boolean
  name: string
}
