import { StyleSheet } from 'react-native'

export const useStyles = () =>
  StyleSheet.create({
    container: {
      alignItems: 'center',
      flex: 1,
      justifyContent: 'center',
    },
    disabled: {
      opacity: 0.3,
    },
  })
