import { TouchableOpacity } from 'react-native'

import { useTheme } from 'react-native-paper'

import { cssClasses } from '../../../constants/CSS'
import { TypographyVariant } from '../../../constants/Variants'
import { Grid } from '../Grid'
import { Icon } from '../Icon'
import { Typography } from '../Typography'
import { ILabeledIconButtonProps } from './LabeledIconButton'
import { useStyles } from './LabeledIconButton.styles'

export const LabeledIconButton = ({
  action,
  label,
  name,
  isDisabled,
  gridItemProps,
}: ILabeledIconButtonProps) => {
  const { colors } = useTheme()
  const styles = useStyles()

  return (
    <Grid
      gridItemProps={gridItemProps}
      containerStyle={isDisabled ? styles.disabled : undefined}
      dataSet={!isDisabled ? cssClasses.hoverElement : undefined}
    >
      <TouchableOpacity onPress={action} style={styles.container}>
        <Icon name={name} size={32} color={colors.textSecondary} />
        <Typography children={label} variant={TypographyVariant.caption} />
      </TouchableOpacity>
    </Grid>
  )
}
