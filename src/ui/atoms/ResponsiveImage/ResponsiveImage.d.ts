import { StyleSheet } from 'react-native'

import { IGridItem } from '../Grid/Grid'

export interface IResponsiveImageStyleProps {
  aspectRatio?: [width: number, height: number]
  borderRadius?: number
  flex?: number
  isOriginalAspectRatio?: boolean
  objectFit?:
    | 'contain'
    | 'cover'
    | 'fill'
    | 'none'
    | 'scale-down'
    | 'inherit'
    | 'initial'
    | 'revert'
    | 'unset'
}

export interface IResponsiveImage
  extends IResponsiveImageStyleProps,
    IGridItem {
  alt?: string
  containerStyle?: StyleSheet.NamedStyles
  imageUrl?: string | null
}
