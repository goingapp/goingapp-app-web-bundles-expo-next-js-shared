import { StyleSheet } from 'react-native'

import { aspectRatio as _aspectRatio } from '../../../constants/AspectRatio'
import { calculateAspectRatioForLegacyBrowser } from '../../../utils/Helpers/calculateAspectRatioForLegacyBrowser'
import { IResponsiveImageStyleProps } from './ResponsiveImage'

export const useStyles = ({
  aspectRatio = _aspectRatio.default,
  borderRadius = 0,
  flex = 1,
}: IResponsiveImageStyleProps) =>
  StyleSheet.create({
    nativeAspectRatio: {
      aspectRatio: aspectRatio[0] / aspectRatio[1],
    },
    root: {
      borderRadius,
      flex,
      overflow: 'hidden',
      position: 'relative',
      alignItems: 'center',
      justifyContent: 'center',
    },
    image: {
      height: '100%',
      opacity: 0,
      width: '100%',
    },
    loaded: {
      opacity: 1,
    },
    legacyBrowser: {
      paddingTop: `${calculateAspectRatioForLegacyBrowser(aspectRatio)}%`,
    },
  })
