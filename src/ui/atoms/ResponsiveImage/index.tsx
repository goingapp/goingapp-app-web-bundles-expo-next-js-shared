import { FC, useEffect, useState } from 'react'
import { Image, View } from 'react-native'
import { ActivityIndicator } from 'react-native'

import { fallbackImage } from '../../../images'
import { IResponsiveImage } from './ResponsiveImage'
import { useStyles } from './ResponsiveImage.styles'

export const ResponsiveImage: FC<IResponsiveImage> = ({
  aspectRatio,
  borderRadius,
  children,
  imageUrl,
  flex,
  gridItemProps,
  alt = 'Image preview',
  containerStyle,
}) => {
  const fallback = Image.resolveAssetSource(
    fallbackImage[Math.floor(Math.random() * 10 + 1)]
  ).uri
  const [showImage, imageLoaded] = useState(false)
  const [src, changeSrc] = useState<string>(imageUrl || fallback)
  const styles = useStyles({
    aspectRatio,
    borderRadius,
    flex,
  })

  const onError = () => {
    changeSrc(fallback)
  }

  const imageReady = () => imageLoaded(true)

  useEffect(() => {
    changeSrc(imageUrl ?? fallback)
  }, [imageUrl])

  return (
    <View style={[styles.root, styles.nativeAspectRatio, containerStyle]}>
      <Image
        accessibilityRole={'image'}
        resizeMode={'cover'}
        style={[styles.image, showImage && styles.loaded]}
        source={{ uri: src }}
        accessibilityLabel={alt}
        onLoad={imageReady}
        onError={onError}
      />
      {children}
      {!showImage && <ActivityIndicator />}
    </View>
  )
}
