import React, { FC, useEffect, useState } from 'react'
import { Image as RNImage, View } from 'react-native'

import Image from 'next/image'

import { IResponsiveImage } from './ResponsiveImage'
import { useStyles } from './ResponsiveImage.styles'

export const ResponsiveImage: FC<IResponsiveImage> = ({
  aspectRatio,
  borderRadius,
  children,
  imageUrl,
  flex = 1,
  gridItemProps,
  alt = 'Image preview',
  containerStyle,
  isOriginalAspectRatio,
  objectFit,
}) => {
  const fallback = `https://goingapp.pl/static_legacy/fallbackImg/mosaic_${Math.floor(
    Math.random() * (10 - 1 + 1) + 1
  )}.jpg`
  const [isError, makeError] = useState(false)
  const [src, changeSrc] = useState<string>(imageUrl || fallback)
  const [intrinsicSize, setIntrinsicSize] = useState([0, 0])
  const styles = useStyles({
    aspectRatio: isOriginalAspectRatio
      ? [intrinsicSize[0], intrinsicSize[1]]
      : aspectRatio,
    borderRadius,
    flex,
  })

  const onError = () => {
    if (!isError) {
      makeError(true)
      changeSrc(fallback)
    }
  }

  useEffect(() => {
    changeSrc(imageUrl ?? fallback)
  }, [imageUrl])

  useEffect(() => {
    return RNImage.getSize(
      src,
      (width, height) => setIntrinsicSize([width, height]),
      (error) => console.error(error)
    )
  }, [src])

  return (
    <View style={[styles.root, styles.legacyBrowser, containerStyle]}>
      <Image
        src={src}
        role={'image'}
        layout={'fill'}
        objectFit={objectFit || 'cover'}
        onError={onError}
        alt={alt}
        style={styles.image}
      />
      {children}
    </View>
  )
}
