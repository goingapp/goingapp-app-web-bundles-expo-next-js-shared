import { FieldValues, UseControllerProps } from 'react-hook-form'
import { TextInputProps } from 'react-native-paper/lib/typescript/components/TextInput/TextInput'

import { IGridItem } from '../Grid/Grid'

type ITextInputOwnProps = {
  icon?: string
  iconAction?: () => void
}

type ExcludeThemeFrom<T> = Without<T, 'theme'>

export type ITextInputProps<T extends FieldValues> = ITextInputOwnProps &
  ExcludeThemeFrom<TextInputProps> &
  UseControllerProps<T> &
  IGridItem
