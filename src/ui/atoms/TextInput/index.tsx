import { FieldValues, useController } from 'react-hook-form'
import { TextInput as PaperInput } from 'react-native-paper'

import { ITextInputProps } from './TextInput'

export const TextInput = <T extends FieldValues>({
  name,
  control,
  defaultValue,
  rules,
  gridItemProps,
  icon,
  iconAction,
  style,
  ...rest
}: ITextInputProps<T>) => {
  const { field } = useController<T>({
    control,
    defaultValue,
    name,
    rules,
  })

  const right = icon && (
    <PaperInput.Icon
      name={icon}
      onPress={iconAction}
      touchSoundDisabled={true}
    />
  )

  return (
    <PaperInput
      style={style}
      right={right}
      {...rest}
      {...field}
      onChangeText={field.onChange}
    />
  )
}
