import { ReactNode } from 'react'

export interface ITwoColumnGridProps {
  box: ReactNode
  isMobile: boolean
  isWeb: boolean
}
