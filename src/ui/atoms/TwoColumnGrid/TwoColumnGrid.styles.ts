import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'

export const useStyles = (isWeb: boolean) => {
  const { spacing } = useTheme()

  return StyleSheet.create({
    box: {}, // TODO
    root: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'nowrap',
    },
    leftColumn: {
      flex: 2,
      paddingRight: spacing(SPACING.REGULAR / 2),
    },
    // @ts-ignore - web only styles
    rightColumn: {
      ...(isWeb && { display: 'block' }),
      flex: 1,
      paddingLeft: spacing(SPACING.REGULAR / 2),
    },
  })
}
