import { FC, Fragment } from 'react'
import { View } from 'react-native'

import { ITwoColumnGridProps } from './TwoColumnGrid'
import { useStyles } from './TwoColumnGrid.styles'

export const TwoColumnGrid: FC<ITwoColumnGridProps> = ({
  box,
  isWeb,
  isMobile,
  children,
}) => {
  const styles = useStyles(isWeb)

  if (isMobile) {
    return <Fragment children={children} />
  }

  return (
    <View style={styles.root}>
      <View style={styles.leftColumn}>{children}</View>
      <View style={styles.rightColumn}>
        <View style={styles.box}>{box}</View>
      </View>
    </View>
  )
}
