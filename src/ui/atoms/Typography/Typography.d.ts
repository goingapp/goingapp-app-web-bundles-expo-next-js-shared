import { StyleSheet } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants'
import { StyleTextAlign } from '../../../models/Styles'
import { IGridItem } from '../Grid/Grid'

export interface ITypographyVariantMap {
  ariaLevel?: number
  accessibilityRole: 'text' | 'header' | 'none'
}

interface ITypographyTestProps {
  testId?: string
}

export interface ITypographyStyleProps {
  isError?: boolean
  secondary?: boolean
  textAlign?: StyleTextAlign
  variant?: TypographyVariant
  decorationColor?: string
}

export interface ITypographyProps
  extends ITypographyTestProps,
    ITypographyStyleProps,
    IGridItem {
  numberOfLines?: number | false
  onPress?: () => void
  style?: StyleSheet.NamedStyles
  variantMapping?: Partial<Record<TypographyVariant, string>>
  withDecoration?: boolean
  dataSet?: { cssClass: string }
}
