import { StyleSheet } from 'react-native'

import { TypographyVariant } from '../../../constants/Variants'
import { ITypographyStyleProps } from './Typography'

export const useStyles = (
  { colors, typography }: ReactNativePaper.Theme,
  {
    isError,
    textAlign,
    secondary,
    variant = TypographyVariant.body2,
    decorationColor = colors.primary,
  }: ITypographyStyleProps
) =>
  StyleSheet.create({
    typography: {
      color: isError
        ? colors.error
        : secondary
        ? colors.textSecondary
        : colors.text,
      ...typography[variant],
      textAlign,
    },
    underline: typography[TypographyVariant.underline](decorationColor),
  })
