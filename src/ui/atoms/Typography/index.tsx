import { FC } from 'react'
import { Text } from 'react-native'

import { useTheme } from 'react-native-paper'

import { TypographyVariant } from '../../../constants/Variants'
import { ITypographyProps, ITypographyVariantMap } from './Typography'
import { useStyles } from './Typography.styles'

export const Typography: FC<ITypographyProps> = ({
  children,
  gridItemProps,
  isError,
  numberOfLines,
  onPress,
  secondary,
  style,
  testId,
  textAlign,
  variant,
  withDecoration,
  dataSet,
}) => {
  const theme = useTheme()
  const styles = useStyles(theme, { isError, secondary, textAlign, variant })

  if (!children) {
    return null
  }

  const mapVariant = (): ITypographyVariantMap => {
    switch (variant) {
      case TypographyVariant.h1:
        return { ariaLevel: 1, accessibilityRole: 'header' }
      case TypographyVariant.h2:
        return { ariaLevel: 2, accessibilityRole: 'header' }
      case TypographyVariant.h3:
        return { ariaLevel: 3, accessibilityRole: 'header' }
      case TypographyVariant.h4:
        return { ariaLevel: 4, accessibilityRole: 'header' }
      case TypographyVariant.h5:
        return { ariaLevel: 5, accessibilityRole: 'header' }
      case TypographyVariant.h6:
        return { ariaLevel: 6, accessibilityRole: 'header' }
      default:
        return { accessibilityRole: 'text' }
    }
  }

  const { accessibilityRole, ariaLevel } = mapVariant()

  return (
    <Text
      aria-level={ariaLevel}
      accessibilityRole={accessibilityRole}
      style={[
        styles.typography,
        withDecoration && styles.underline,
        style && style,
      ]}
      testID={testId}
      numberOfLines={numberOfLines ? numberOfLines : undefined}
      onPress={onPress}
      // @ts-ignore: this is a new prop and is not yet supported by types for react-native-web
      dataSet={dataSet}
    >
      {children}
    </Text>
  )
}
