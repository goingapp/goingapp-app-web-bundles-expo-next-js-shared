import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'

export const useStyles = (isMobile: boolean) => {
  const theme = useTheme()
  return StyleSheet.create({
    artistTitle: {
      backgroundColor: theme.colors.primary,
    },
    artistTitleText: {
      color: theme.colors.primaryButtonText,
    },
    reversed: {
      flexDirection: 'row-reverse',
    },
    root: {
      width: !isMobile
        ? `calc(50% - ${theme.spacing(SPACING.REGULAR)}px)`
        : undefined,
    },
  })
}
