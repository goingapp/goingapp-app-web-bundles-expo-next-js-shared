import { IArtist } from '../../../models/Artist'
import { IResolveUrl } from '../../../models/Navigation'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IEventArtistProps extends IGridItem {
  artist: IArtist
  isReversed?: boolean
  isNext?: boolean
  isMobile: boolean
  urlApp: string
  resolveUrl: IResolveUrl
}
