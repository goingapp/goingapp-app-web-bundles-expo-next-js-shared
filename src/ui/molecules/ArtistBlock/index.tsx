import { SPACING } from '../../../constants/Grid'
import { TypographyVariant } from '../../../constants/Variants'
import { useCloudinaryApi } from '../../../hooks/useCloudinaryApi'
import { Link } from '../../_reusable/Link'
import { Grid } from '../../atoms/Grid'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { Typography } from '../../atoms/Typography'
import { useStyles } from './ArtistBlock.styles'
import { IEventArtistProps } from './ArtistBlock.types'

export const ArtistBlock = ({
  artist,
  isReversed,
  gridItemProps,
  isMobile,
  isNext,
  urlApp,
  resolveUrl,
}: IEventArtistProps) => {
  const { slug } = artist
  const styles = useStyles(isMobile)
  const { cloudinaryImagesUrls } = useCloudinaryApi(slug, 'artist')
  const artistBlock = (
    <Grid
      spacing={0}
      rowDirection={true}
      borderRadius={true}
      containerStyle={isReversed ? styles.reversed : undefined}
    >
      <Grid flex={1}>
        <ResponsiveImage
          imageUrl={cloudinaryImagesUrls[0]}
          aspectRatio={[81, 70]}
        />
      </Grid>
      <Grid
        padding={SPACING.REGULAR}
        justifyContent={'flex-end'}
        containerStyle={styles.artistTitle}
        flex={1}
      >
        <Typography
          variant={isMobile ? TypographyVariant.body1 : TypographyVariant.h5}
          children={artist.name}
          style={styles.artistTitleText}
        />
      </Grid>
    </Grid>
  )

  return !isNext ? (
    <Link
      url={`${urlApp}/artysta/${artist.slug}`}
      style={styles.root}
      gridItemProps={gridItemProps}
      resolveUrl={resolveUrl}
    >
      {artistBlock}
    </Link>
  ) : (
    <Grid containerStyle={styles.root} gridItemProps={gridItemProps}>
      {artistBlock}
    </Grid>
  )
}
