import { IArtist } from '../../../models/Artist'
import { ResolveUrlType } from '../../../models/Navigation'

export interface IEventArtistsProps {
  artists: IArtist[]
  isNext?: boolean
  isMobile: boolean
  urlApp: string
  resolveUrl: ResolveUrlType
}
