import { SPACING } from '../../../constants/Grid'
import { IArtist } from '../../../models/Artist'
import { Grid } from '../../atoms/Grid'
import { ArtistBlock } from '../ArtistBlock'
import { IEventArtistsProps } from './ArtistBlocks'

export const ArtistBlocks = ({
  artists,
  isMobile,
  isNext,
  resolveUrl,
  urlApp,
}: IEventArtistsProps) => {
  const artistsCoupledIntoPairs: IArtist[][] = artists.reduce(
    (result: IArtist[][], _value, index, array) => {
      if (index % 2 === 0) {
        result.push(array.slice(index, index + 2))
      }

      return result
    },
    []
  )

  const eventArtists = isMobile
    ? artists.map((artist, index) => (
        <ArtistBlock
          artist={artist}
          isReversed={!(index % 2 === 0)}
          key={`artist-${artist.id}`}
          isMobile={isMobile}
          isNext={isNext}
          urlApp={urlApp}
          resolveUrl={resolveUrl}
        />
      ))
    : artistsCoupledIntoPairs.map((artistPair, index) => {
        return (
          <Grid rowDirection={true} spacing={SPACING.REGULAR}>
            {artistPair.map((artist) => (
              <ArtistBlock
                artist={artist}
                isReversed={!(index % 2 === 0)}
                key={`artist-${artist.id}`}
                isMobile={isMobile}
                isNext={isNext}
                urlApp={urlApp}
                resolveUrl={resolveUrl}
              />
            ))}
          </Grid>
        )
      })

  return <Grid spacing={SPACING.REGULAR}>{eventArtists}</Grid>
}
