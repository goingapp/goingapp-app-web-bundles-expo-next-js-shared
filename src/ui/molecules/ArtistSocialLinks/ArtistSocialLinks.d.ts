import { IArtist } from '../../../models/Artist'
import { IResolveUrl } from '../../../models/Navigation'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IArtistSocialLinksProps extends IGridItem {
  artist: IArtist
  resolveUrl: IResolveUrl
}
