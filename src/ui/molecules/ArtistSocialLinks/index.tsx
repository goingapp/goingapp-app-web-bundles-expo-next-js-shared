import { SPACING } from '../../../constants/Grid'
import { Grid } from '../../atoms/Grid'
import { LabeledIconButton } from '../../atoms/LabeledIconButton'
import { IArtistSocialLinksProps } from './ArtistSocialLinks'

export const ArtistSocialLinks = ({
  artist,
  gridItemProps,
  resolveUrl,
}: IArtistSocialLinksProps) => {
  const handleGoToSocial = (url?: string) => () =>
    url ? resolveUrl(url) : () => {}

  return (
    <Grid
      rowDirection={true}
      padding={SPACING.LARGE}
      spacing={SPACING.HUGE}
      justifyContent={'space-between'}
      withBackground={true}
      borderRadius={true}
      gridItemProps={gridItemProps}
    >
      <LabeledIconButton
        action={handleGoToSocial(artist.fbLink)}
        label={'Facebook'}
        name={'facebook'}
        isDisabled={!artist.fbLink}
      />
      <LabeledIconButton
        action={handleGoToSocial(artist.instagramLink)}
        label={'Instagram'}
        name={'instagram'}
        isDisabled={!artist.instagramLink}
      />
      <LabeledIconButton
        action={handleGoToSocial(artist.spotifyLink)}
        label={'Spotify'}
        name={'spotify'}
        isDisabled={!artist.spotifyLink}
      />
    </Grid>
  )
}
