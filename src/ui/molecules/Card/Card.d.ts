import { StyleSheet } from 'react-native'

import { ITag } from '../../../models/Tag'
import { IGridItem, IGridItemElement } from '../../atoms/Grid/Grid'

export interface ICardProps extends IGridItem {
  cardAction?: () => void
  cardsInRow?: number
  children: IGridItemElement | IGridItemElement[]
  cover?: string
  coverAspectRatio?: [width: number, height: number]
  isSliderCard?: boolean
  style?: StyleSheet.NamedStyles
  tagColor?: string
  tags?: string[] | ITag[]
  isMobile: boolean
  isWeb: boolean
  containerWidth: number
  alt?: string
}
