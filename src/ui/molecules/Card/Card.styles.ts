import { StyleSheet } from 'react-native'

import { SPACING } from '../../../constants/Grid'
import { Ui } from '../../../constants/Ui'
import { useCardWidth } from './hooks/useCardWidth'

export const useStyles = (
  isWeb: boolean,
  isMobile: boolean,
  containerWidth: number,
  cardsInRow?: number,
  isSliderCard?: boolean,
  spacing?: SPACING
) => {
  const { cardWidth } = useCardWidth({
    cardsInRow,
    containerWidth,
    spacing,
    isSliderCard,
  })

  return StyleSheet.create({
    root: {
      flexBasis: '0%',
      flexGrow: 1,
      flexShrink: 0,
      minWidth: cardsInRow ? cardWidth : Ui.minCardWidth,
      ...(cardsInRow && {
        maxWidth: cardWidth,
      }),
      ...(isWeb && {
        height: 'fit-content',
        minHeight: isMobile ? 0 : '100%',
      }),
    },
  })
}
