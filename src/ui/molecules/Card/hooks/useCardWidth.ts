import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'

export const useCardWidth = ({
  cardsInRow = 1,
  containerWidth,
  spacing = SPACING.REGULAR,
  isSliderCard,
}: {
  cardsInRow?: number
  containerWidth: number
  spacing?: SPACING
  isSliderCard?: boolean
}) => {
  const theme = useTheme()
  const baseCardWidth =
    cardsInRow && cardsInRow !== 1
      ? containerWidth / cardsInRow -
        ((cardsInRow - 1) * theme.spacing(spacing)) / cardsInRow
      : containerWidth

  const cardWidth = isSliderCard
    ? baseCardWidth - theme.spacing(8)
    : baseCardWidth

  return {
    cardWidth,
  }
}
