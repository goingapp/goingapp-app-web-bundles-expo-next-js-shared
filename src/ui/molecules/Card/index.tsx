import { FC } from 'react'

import { Card as PaperCard } from 'react-native-paper'

import { cssClasses } from '../../../constants/CSS'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { MainImageWithTagsContainer } from '../MainImageWithTagsContainer'
import { ICardProps } from './Card'
import { useStyles } from './Card.styles'

export const Card: FC<ICardProps> = ({
  coverAspectRatio,
  cardAction,
  cardsInRow,
  children,
  cover,
  gridItemProps,
  isSliderCard,
  style,
  tags,
  tagColor,
  isMobile,
  isWeb,
  containerWidth,
  alt,
}) => {
  const styles = useStyles(
    isWeb,
    isMobile,
    containerWidth,
    cardsInRow,
    isSliderCard,
    gridItemProps?.spacing
  )

  return (
    <PaperCard
      elevation={0}
      onPress={cardAction}
      style={[styles.root, style]}
      // @ts-ignore
      dataSet={cssClasses.card}
    >
      {tags ? (
        <MainImageWithTagsContainer
          aspectRatio={coverAspectRatio}
          image={cover}
          tags={tags}
          tagColor={tagColor}
          alt={alt}
          isWeb={isWeb}
        />
      ) : (
        <ResponsiveImage
          aspectRatio={coverAspectRatio}
          imageUrl={cover}
          flex={isWeb ? 0 : 1}
          alt={alt}
        />
      )}
      {children}
    </PaperCard>
  )
}
