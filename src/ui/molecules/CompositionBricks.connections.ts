import { BricksEnum } from '../../constants/Compositions'
import { IBricksConnection } from '../../models/Compositions'
import { ArtistSlider } from './_compositions/ArtistSlider'
import { BigAndVerticalList } from './_compositions/BigAndVerticalList'
import { BigBlock } from './_compositions/BigBlock'
import { Carousel } from './_compositions/Carousel'
import { CTAButton } from './_compositions/CTAButton'
import { CustomHtml } from './_compositions/CustomHtml'
import { DescriptionBrick } from './_compositions/DescriptionBrick'
import { Embed } from './_compositions/Embed'
import { Faq } from './_compositions/Faq'
import { FormIo } from './_compositions/FormIo'
import { GalleryBrick } from './_compositions/GalleryBrick'
import { MultilineCards } from './_compositions/MultilineCards'
import { SearchBrick } from './_compositions/SearchBrick'
import { SectionLead } from './_compositions/SectionLead'
import { SlidableHotspots } from './_compositions/SlidableHotspots'
import { Testimonials } from './_compositions/Testimonials'
import { TextBlock } from './_compositions/TextBlock'
import { Timeline } from './_compositions/Timeline'
import { TourList } from './_compositions/TourList'

export const brickComponents: IBricksConnection = {
  [BricksEnum.artist_slider]: {
    component: ArtistSlider,
  },
  [BricksEnum.big_and_vertical_list]: {
    component: BigAndVerticalList,
  },
  [BricksEnum.big_hotspot]: {
    component: BigBlock,
  },
  [BricksEnum.carousel]: {
    component: Carousel,
  },
  [BricksEnum.cta_button]: {
    component: CTAButton,
  },
  [BricksEnum.custom_code]: {
    component: CustomHtml,
  },
  [BricksEnum.description]: {
    component: DescriptionBrick,
  },
  [BricksEnum.embed]: {
    component: Embed,
  },
  [BricksEnum.faq]: {
    component: Faq,
  },
  [BricksEnum.formio]: {
    component: FormIo,
  },
  [BricksEnum.gallery]: {
    component: GalleryBrick,
  },
  [BricksEnum.multiline_hotspots]: {
    component: MultilineCards,
  },
  [BricksEnum.search_bar]: {
    component: SearchBrick,
  },
  [BricksEnum.section_lead]: {
    component: SectionLead,
  },
  [BricksEnum.slidable_hotspots]: {
    component: SlidableHotspots,
  },
  [BricksEnum.testimonials]: {
    component: Testimonials,
  },
  [BricksEnum.text_block]: {
    component: TextBlock,
  },
  [BricksEnum.text_block_with_image]: {
    component: TextBlock,
  },
  [BricksEnum.timeline]: {
    component: Timeline,
  },
  [BricksEnum.tour_list]: {
    component: TourList,
  },
}
