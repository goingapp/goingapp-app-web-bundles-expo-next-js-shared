import { StyleSheet } from 'react-native'

import { IAppContext } from '../../../models/AppContext'
import { IPriceTranslations } from '../../../models/Locale'
import { ResolveUrlType } from '../../../models/Navigation'
import { ITag } from '../../../models/Tag'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IEventCardProps extends IGridItem {
  address?: string
  artistsNames?: string[]
  buttonLabel: string
  cardsInRow?: number
  changeMonitorType?: string
  containerWidth: IAppContext['containerWidth']
  cover: string
  date?: string
  description: string
  id?: string
  index?: number
  notForSale?: boolean
  partnerName?: string
  place?: string
  placeSlug?: string
  price?: string
  style?: StyleSheet.NamedStyles
  tags?: string[] | ITag[]
  title: string
  isCalendarEvent?: boolean
  isMobile?: boolean
  isSliderCard?: boolean
  resolveUrl: ResolveUrlType
  priceTranslations: IPriceTranslations
  url: string
}

export interface IEventCardItemProps extends IEventCardProps {
  params: {
    eventSlug: string
    rundateSlug?: string
  }
  isCalendarEvent?: boolean
}

export type IEventCardItem = Omit<
  IEventCardItemProps,
  'url' | 'resolveUrl' | 'containerWidth' | 'priceTranslations'
>
