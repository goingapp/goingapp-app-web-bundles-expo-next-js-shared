import { IGridItem } from '../../atoms/Grid/Grid'

export interface IGalleryStyleProps {
  theme: ReactNativePaper.Theme
}

export interface IGalleryProps extends IGridItem {
  images: string[]
  isDesktop?: boolean
}
