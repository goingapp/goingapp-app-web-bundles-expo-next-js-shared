import React from 'react'
import { StyleSheet } from 'react-native'

import Constants from 'expo-constants'

import { SPACING } from '../../../constants/Grid'
import { IGalleryStyleProps } from './Gallery'

export const useStyles = ({ theme }: IGalleryStyleProps) =>
  StyleSheet.create({
    fab: {
      backgroundColor: theme.colors.primary,
      position: 'absolute',
      top: Constants.statusBarHeight,
      right: theme.spacing(SPACING.REGULAR),
    },
    photo: {
      width: '100%',
    },
  })

export const useWebStyles = (): {
  [key: string]: React.CSSProperties
} => ({
  webButton: {
    position: 'fixed',
    top: 32,
    right: 0,
  },
})
