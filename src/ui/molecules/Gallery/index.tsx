import { ScrollView, TouchableOpacity } from 'react-native'

import { FAB, Portal, useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'
import { useToggle } from '../../../hooks/useToggle'
import { GalleryPhotoPlaceholder } from '../../_reusable/GalleryPhotoPlaceholder'
import { SmallGalleryImage } from '../../_reusable/SmallGalleryImage'
import { Grid } from '../../atoms/Grid'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { IGalleryProps } from './Gallery'
import { useStyles } from './Gallery.styles'

export const Gallery = ({ images, gridItemProps }: IGalleryProps) => {
  const shouldRenderMorePhoto: boolean = images.length > 1
  const isMoreThenFour: boolean = images.length > 4
  const { handleHide, handleShow, isVisible } = useToggle()
  const theme = useTheme()
  const styles = useStyles({ theme })

  const OpenGallery = () => (
    <>
      <ScrollView>
        <Grid
          spacing={SPACING.REGULAR}
          withBackground={true}
          justifyContent={'center'}
        >
          {images.map((image, index) => (
            <ResponsiveImage
              imageUrl={image}
              key={`gallery-open-${index}`}
              containerStyle={styles.photo}
            />
          ))}
        </Grid>
      </ScrollView>
      <FAB
        icon={'close'}
        color={theme.colors.primaryButtonText}
        onPress={handleHide}
        small={true}
        style={styles.fab}
      />
    </>
  )

  return (
    <>
      <Portal>{isVisible && <OpenGallery />}</Portal>
      <TouchableOpacity onPress={handleShow}>
        <Grid gridItemProps={gridItemProps} spacing={SPACING.REGULAR}>
          <ResponsiveImage imageUrl={images[0]} />
          {shouldRenderMorePhoto && (
            <Grid flex={1} spacing={SPACING.REGULAR} rowDirection>
              <SmallGalleryImage url={images[1]} />
              <SmallGalleryImage url={images[2]} />
              {!!images[4] ? (
                <GalleryPhotoPlaceholder
                  moreImages={isMoreThenFour ? images.length : undefined}
                  imagesCount={isMoreThenFour ? images.length : undefined}
                />
              ) : (
                <SmallGalleryImage url={images[3]} />
              )}
            </Grid>
          )}
        </Grid>
      </TouchableOpacity>
    </>
  )
}
