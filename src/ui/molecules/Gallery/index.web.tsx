import { TouchableOpacity } from 'react-native'

import { FAB, Portal, useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'
import { useToggle } from '../../../hooks/useToggle'
import { GalleryPhotoPlaceholder } from '../../_reusable/GalleryPhotoPlaceholder'
import { SmallGalleryImage } from '../../_reusable/SmallGalleryImage'
import { Grid } from '../../atoms/Grid'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { IGalleryProps } from './Gallery'
import { useStyles, useWebStyles } from './Gallery.styles'

export const Gallery = ({
  images,
  isDesktop,
  gridItemProps,
}: IGalleryProps) => {
  const shouldRenderMorePhoto: boolean = images.length > 1
  const isMoreThenFour: boolean = images.length > 4
  const { handleHide, handleShow, isVisible } = useToggle()
  const theme = useTheme()
  const styles = useStyles({ theme })
  const webStyles = useWebStyles()

  const OpenGallery = () => (
    <>
      <Grid
        spacing={SPACING.REGULAR}
        withBackground={true}
        justifyContent={'center'}
      >
        {images.map((image, index) => (
          <ResponsiveImage
            imageUrl={image}
            key={`gallery-open-${index}`}
            containerStyle={styles.photo}
          />
        ))}
      </Grid>
    </>
  )

  if (isVisible)
    return (
      <Portal>
        <OpenGallery />
        <div style={webStyles.webButton} onClick={handleHide}>
          <FAB
            icon={'close'}
            color={theme.colors.primaryButtonText}
            onPress={handleHide}
            small={true}
            style={styles.fab}
          />
        </div>
      </Portal>
    )

  return (
    <>
      <TouchableOpacity onPress={handleShow}>
        <Grid
          gridItemProps={gridItemProps}
          spacing={SPACING.REGULAR}
          rowDirection={isDesktop}
        >
          {isDesktop ? (
            <Grid flex={2}>
              <ResponsiveImage imageUrl={images[0]} borderRadius={16} />
            </Grid>
          ) : (
            <ResponsiveImage imageUrl={images[0]} flex={0} borderRadius={16} />
          )}
          {shouldRenderMorePhoto && (
            <Grid flex={1} spacing={SPACING.REGULAR} rowDirection={!isDesktop}>
              <SmallGalleryImage url={images[1]} isDesktop={isDesktop} />
              <SmallGalleryImage url={images[2]} isDesktop={isDesktop} />
              {!!images[4] ? (
                <GalleryPhotoPlaceholder
                  moreImages={isMoreThenFour ? images.length : undefined}
                  imagesCount={isMoreThenFour ? images.length : undefined}
                  isDesktop={isDesktop}
                />
              ) : (
                <SmallGalleryImage url={images[3]} isDesktop={isDesktop} />
              )}
            </Grid>
          )}
        </Grid>
      </TouchableOpacity>
    </>
  )
}
