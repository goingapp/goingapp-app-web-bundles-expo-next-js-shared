import { IEventImage, ITag } from '../../../models/Tag'

export interface IMainImageWithTagsContainerProps {
  aspectRatio?: [width: number, height: number]
  tagColor?: string
  tags: ITag[] | string[]
  image?: IEventImage | string
  alt?: string
  isWeb: boolean
}
