import { StyleSheet } from 'react-native'

import { SPACING } from '../../../constants/Grid'
import { isColorDark } from '../../../utils/Helpers/isColorDark'

export const useStyles = (
  theme: ReactNativePaper.Theme,
  spacing: SPACING,
  tagColor?: string
) =>
  StyleSheet.create({
    customColor: {
      backgroundColor: tagColor,
      color: tagColor
        ? isColorDark(tagColor)
          ? theme.colors.white
          : theme.colors.black
        : undefined,
    },
    tags: {
      top: 0,
      position: 'absolute',
      left: 0,
    },
    tag: {
      borderRadius: 0,
      borderWidth: 0,
      transform: [{ translateX: theme.spacing(spacing) }],
    },
  })
