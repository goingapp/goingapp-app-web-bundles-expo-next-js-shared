import { useTheme } from 'react-native-paper'

import { ITag } from '../../../models/Tag'
import { Chip } from '../../atoms/Chip'
import { Grid } from '../../atoms/Grid'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { IMainImageWithTagsContainerProps } from './MainImageWithTagsContainer'
import { useStyles } from './MainImageWithTagsContainer.styles'

export const MainImageWithTagsContainer = ({
  aspectRatio,
  tagColor,
  tags,
  image,
  alt,
  isWeb,
}: IMainImageWithTagsContainerProps) => {
  const tagsSpacing = 0
  const theme = useTheme()
  const styles = useStyles(theme, tagsSpacing, tagColor)

  const tagElements = tags
    .slice(0, 4)
    .map((tag: ITag | string, index) => (
      <Chip
        outlined={index !== 0}
        label={typeof tag !== 'string' ? tag.name : tag}
        key={typeof tag !== 'string' ? tag.id.toString() : tag}
        style={[styles.tag, tagColor && styles.customColor]}
        category={tag}
      />
    ))

  return (
    <ResponsiveImage
      aspectRatio={aspectRatio}
      imageUrl={typeof image === 'string' ? image : image?.large}
      alt={alt}
      flex={isWeb ? 0 : 1}
    >
      <Grid
        containerStyle={styles.tags}
        spacing={tagsSpacing}
        justifyContent={'flex-end'}
        rowDirection
        children={tagElements}
      />
    </ResponsiveImage>
  )
}
