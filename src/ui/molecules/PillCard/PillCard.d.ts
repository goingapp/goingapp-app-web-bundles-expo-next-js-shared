import { StyleProp } from 'react-native'

import { IGridItemProps } from '../../atoms/Grid/Grid'

export interface IPillCardProps {
  img?: string
  cloudinaryImg?: string
  title?: string
  gridItemProps?: IGridItemProps
  styles: StyleProp
  cloudName: string
  cloudList: string
  prefix?: string
  slug?: string
}

export interface IPillCardStyleProps {
  background?: string
  width: number
  pillsInRow: number
}
