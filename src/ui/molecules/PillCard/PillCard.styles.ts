import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../constants/Grid'
import { isColorDark } from '../../../utils/Helpers/isColorDark'
import { IPillCardStyleProps } from './PillCard'

export const useStyles = ({
  background,
  width,
  pillsInRow,
}: IPillCardStyleProps) => {
  const theme = useTheme()
  return StyleSheet.create({
    pill: {
      width: (width - 2 * theme.spacing(SPACING.REGULAR)) / pillsInRow,
    },
    pillImage: {
      borderRadius: width / pillsInRow,
    },
    sliderItem: {
      backgroundColor: 'transparent',
      paddingHorizontal: theme.spacing(SPACING.REGULAR),
    },
    title: {
      textAlign: 'center',
      color: isColorDark(background) ? theme.colors.white : theme.colors.black,
      marginTop: theme.spacing(SPACING.REGULAR),
    },
  })
}
