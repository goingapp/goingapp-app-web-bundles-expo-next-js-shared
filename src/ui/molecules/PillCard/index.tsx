import { TypographyVariant } from '../../../constants/Variants'
import { useCloudinaryApi } from '../../../hooks/useCloudinaryApi'
import { Grid } from '../../atoms/Grid'
import { ResponsiveImage } from '../../atoms/ResponsiveImage'
import { Typography } from '../../atoms/Typography'
import { IPillCardProps } from './PillCard'

export const PillCard = ({
  img,
  title,
  gridItemProps,
  styles,
  cloudName,
  cloudList,
  prefix,
  slug,
}: IPillCardProps) => {
  const { cloudinaryImagesUrls } = useCloudinaryApi(
    cloudName,
    cloudList,
    slug,
    prefix
  )
  const coverImage = cloudinaryImagesUrls.length ? cloudinaryImagesUrls[0] : img

  return (
    <Grid
      gridItemProps={gridItemProps}
      containerStyle={[styles.pill, styles.sliderItem]}
    >
      <Grid spacing={0}>
        <ResponsiveImage
          imageUrl={coverImage}
          aspectRatio={[1, 1]}
          flex={1}
          containerStyle={styles.pillImage}
          alt={title}
        />
        <Grid>
          <Typography
            style={styles.title}
            variant={TypographyVariant.caption}
            children={title}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}
