import { IResolveUrl } from '../../../models/Navigation'
import { IGridItem } from '../../atoms/Grid/Grid'

export interface IPillSliderItem {
  item: IPillSliderData
}

export interface IPillSliderData {
  link: string
  slug?: string
  title: string
  img?: string
  cloudName: string
  cloudList: string
  prefix?: string
  slug?: string
}

export interface IPillSliderProps extends IGridItem {
  background?: string
  data: IPillSliderData[]
  isControls?: boolean
  isMobile: boolean
  resolveUrl: IResolveUrl
  withBackground?: boolean
  isWeb: boolean
  isLegacySafari?: boolean
}

export interface IUsePillSliderProps {
  background?: string
  resolveUrl: IResolveUrl
  width: number
  isMobile: boolean
}
