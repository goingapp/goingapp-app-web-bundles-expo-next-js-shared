import { createCompositionsCardUrl } from '../../../../utils/Helpers/createCompositionsCardUrl'
import { Link } from '../../../_reusable/Link'
import { PillCard } from '../../PillCard'
import { useStyles } from '../../PillCard/PillCard.styles'
import {
  IPillSliderData,
  IPillSliderItem,
  IUsePillSliderProps,
} from '../PillSlider'

export const usePillSlider = ({
  background,
  width,
  isMobile,
  resolveUrl,
}: IUsePillSliderProps) => {
  const styles = useStyles({
    background,
    width,
    pillsInRow: 3,
  })

  const renderItem = ({ item }: IPillSliderItem) => {
    const { img, link, title, slug, cloudName, cloudList, prefix } = item

    const preparedLink = createCompositionsCardUrl(link, slug)

    const card = (
      <PillCard
        img={img}
        title={title}
        styles={styles}
        cloudName={cloudName}
        cloudList={cloudList}
        prefix={prefix}
        slug={slug}
      />
    )

    return preparedLink ? (
      <Link url={preparedLink} resolveUrl={resolveUrl}>
        {card}
      </Link>
    ) : (
      card
    )
  }

  const keyExtractor = (item: IPillSliderData, index: number) =>
    `pill-slider-${isMobile ? 'mobile' : 'desktop'}-${item.slug}-${index}`

  return {
    keyExtractor,
    renderItem,
  }
}
