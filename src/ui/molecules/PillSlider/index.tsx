import { useRef, useState } from 'react'
import { FlatList, LayoutChangeEvent } from 'react-native'

import { cssClasses } from '../../../constants/CSS'
import { SPACING } from '../../../constants/Grid'
import { SliderControls } from '../../_reusable/SliderControls'
import { Grid } from '../../atoms/Grid'
import { usePillSlider } from './hooks/usePillSlider'
import { IPillSliderProps } from './PillSlider'

export const PillSlider = ({
  data,
  background,
  gridItemProps,
  isMobile,
  isControls,
  resolveUrl,
  isWeb,
  isLegacySafari,
}: IPillSliderProps) => {
  const showControls = !isMobile && isControls
  const [width, setWidth] = useState(0)
  const { renderItem, keyExtractor } = usePillSlider({
    background,
    width,
    isMobile,
    resolveUrl,
  })
  const ref = useRef<FlatList>(null)

  const handleOnLayout = (e: LayoutChangeEvent) =>
    setWidth(e.nativeEvent.layout.width - 2 * SPACING.REGULAR)

  return (
    <Grid
      gridItemProps={gridItemProps}
      flex={1}
      onLayout={handleOnLayout}
      spacing={SPACING.REGULAR}
      borderRadius={true}
      dataSet={
        showControls
          ? cssClasses.pillSlider
          : cssClasses.pillSliderWithoutArrows
      }
    >
      <Grid>
        {showControls ? (
          <SliderControls
            dataLength={data.length}
            sliderRef={ref}
            isMobile={isMobile}
            elementsInRow={3}
            elementSpacing={SPACING.REGULAR}
            isWeb={isWeb}
            isLegacySafari={isLegacySafari}
          >
            <FlatList
              data={data}
              horizontal={true}
              renderItem={renderItem}
              ref={ref}
              keyExtractor={keyExtractor}
              scrollEnabled={false}
            />
          </SliderControls>
        ) : (
          <FlatList
            data={data}
            horizontal={true}
            renderItem={renderItem}
            ref={ref}
            keyExtractor={keyExtractor}
          />
        )}
      </Grid>
    </Grid>
  )
}
