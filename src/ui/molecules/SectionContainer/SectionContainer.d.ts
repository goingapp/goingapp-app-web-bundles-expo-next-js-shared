import { ViewStyle } from 'react-native'

import { IGridItem } from '../../atoms/Grid/Grid'

export interface ISectionContainerProps extends IGridItem {
  container?: boolean
  disabled?: boolean
  title?: string
  style?: ViewStyle | ViewStyle[]
  isDesktop?: boolean
  isWeb?: boolean
}
