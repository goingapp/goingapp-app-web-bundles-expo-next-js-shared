import { StyleSheet } from 'react-native'

import { SPACING } from '../../../constants/Grid'

export const useStyles = (
  { colors, spacing }: ReactNativePaper.Theme,
  isWeb?: boolean
) => {
  return StyleSheet.create({
    title: {
      marginBottom: spacing(SPACING.REGULAR),
      paddingHorizontal: spacing(SPACING.REGULAR),
    },
    container: {
      backgroundColor: colors.surface,
      padding: spacing(SPACING.LARGE),
      borderRadius: isWeb ? spacing(SPACING.REGULAR) : undefined,
    },
  })
}
