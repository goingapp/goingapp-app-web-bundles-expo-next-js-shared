import { FC } from 'react'
import { View } from 'react-native'

import { useTheme } from 'react-native-paper'

import { cssClasses } from '../../../constants/CSS'
import { TypographyVariant } from '../../../constants/Variants'
import { Typography } from '../../atoms/Typography'
import { ISectionContainerProps } from './SectionContainer'
import { useStyles } from './SectionContainer.styles'

export const SectionContainer: FC<ISectionContainerProps> = ({
  children,
  container,
  disabled,
  title,
  gridItemProps,
  style,
  isDesktop,
  isWeb,
}) => {
  const theme = useTheme()
  const themeStyles = useStyles(theme, isWeb)

  return disabled ? null : (
    <>
      {title && (
        <Typography
          style={[themeStyles.title]}
          variant={
            isDesktop ? TypographyVariant.h4 : TypographyVariant.h4mobile
          }
          children={title}
        />
      )}
      {container ? (
        <View
          style={[themeStyles.container, style]}
          children={children}
          // @ts-ignore TODO
          dataSet={cssClasses.sectionContainer}
        />
      ) : (
        children
      )}
    </>
  )
}
