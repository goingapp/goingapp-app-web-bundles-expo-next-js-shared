import { ListRenderItemInfo } from 'react-native'

export interface ISliderItem {
  id?: string
}

export interface ISliderProps<T> {
  data: T[]
  children: ({ item }: ListRenderItemInfo<T>) => JSX.Element
}
