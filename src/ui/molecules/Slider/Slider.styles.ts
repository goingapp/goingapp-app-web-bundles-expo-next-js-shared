import { StyleSheet } from 'react-native'

export const useStyles = () =>
  StyleSheet.create({
    root: {
      flexGrow: 0,
      flexShrink: 0,
    },
  })
