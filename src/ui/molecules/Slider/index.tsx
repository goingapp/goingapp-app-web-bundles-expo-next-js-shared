import { useEffect, useRef } from 'react'
import { FlatList } from 'react-native'

import { ISliderItem, ISliderProps } from './Slider'
import { useStyles } from './Slider.styles'
import { useSlider } from './useSlider'

export const Slider = <T extends object & ISliderItem>({
  data,
  children,
}: ISliderProps<T>) => {
  const styles = useStyles()
  const { keyExtractor } = useSlider<T>()
  const ref = useRef<FlatList>(null)

  useEffect(() => {
    if (ref.current) {
      const params = {
        index: 0,
      }
      ref.current.scrollToIndex(params)
    }
  }, [data])

  return (
    <FlatList<T>
      style={styles.root}
      data={data}
      renderItem={children}
      horizontal
      keyExtractor={keyExtractor}
      ref={ref}
      viewabilityConfig={{ itemVisiblePercentThreshold: 75 }}
    />
  )
}
