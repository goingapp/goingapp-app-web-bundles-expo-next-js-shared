import { ISliderItem } from './Slider'

export const useSlider = <T extends object & ISliderItem>() => {
  const keyExtractor = (props: T, index: number): string => {
    if (props.id) {
      return props.id
    }

    return index.toString()
  }

  return {
    keyExtractor,
  }
}
