import { StyleSheet } from 'react-native'

import { ITag } from '../../../models/Event'
import { IResolveUrl } from '../../../models/Navigation'

export interface ITagsBarProps {
  tagTextStyle?: StyleSheet.NamedStyles
  tagStyle?: StyleSheet.NamedStyles
  style?: StyleSheet.NamedStyles
  maxLength?: number
  tags: string[] | ITag[]
  resolveUrl?: IResolveUrl
  isWpArticle?: boolean
}
