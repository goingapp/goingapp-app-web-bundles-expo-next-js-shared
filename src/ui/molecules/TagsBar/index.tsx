import { cssClasses } from '../../../constants/CSS'
import { SPACING } from '../../../constants/Grid'
import { Chip } from '../../atoms/Chip'
import { Grid } from '../../atoms/Grid'
import { ITagsBarProps } from './TagsBar'

export const TagsBar = ({
  maxLength,
  style,
  tags,
  tagStyle,
  tagTextStyle,
  resolveUrl,
  isWpArticle,
}: ITagsBarProps) => (
  <Grid
    spacing={SPACING.TINY}
    containerStyle={style}
    rowDirection
    wrap
    dataSet={cssClasses.tagsBar}
  >
    {tags.slice(0, maxLength).map((tag, index) => {
      return (
        <Chip
          outlined={index !== 0}
          label={typeof tag === 'string' ? tag : tag.name}
          key={typeof tag === 'string' ? tag : tag.id.toString()}
          category={tag}
          style={tagStyle}
          textStyle={tagTextStyle}
          onPress={
            resolveUrl && isWpArticle
              ? () => {
                  resolveUrl(`/kategoria/${tag.slug}`)
                }
              : undefined
          }
        />
      )
    })}
  </Grid>
)
