import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'
import { IPillSliderData } from '../../PillSlider/PillSlider'

export interface IArtistSliderProps extends ISectionsComponentCommon {
  extra: ISectionComponentExtra
  data: IPillSliderData[]
}
