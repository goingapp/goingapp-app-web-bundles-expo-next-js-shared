import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { PillSlider } from '../../PillSlider'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { IArtistSliderProps } from './ArtistSlider'

export const ArtistSlider = ({
  data,
  background,
  extra,
  gridItemProps,
  id: sliderId,
  isMobile,
  resolveUrl,
  isWeb,
}: IArtistSliderProps) => {
  const slider = (
    <PillSlider
      data={data}
      background={background}
      isMobile={isMobile}
      resolveUrl={resolveUrl}
      isWeb={isWeb}
    />
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        isMobile={isMobile}
        id={sliderId}
        children={slider}
        gridItemProps={gridItemProps}
      />
    )
  }

  return (
    <Grid gridItemProps={gridItemProps} spacing={SPACING.LARGE}>
      {slider}
    </Grid>
  )
}
