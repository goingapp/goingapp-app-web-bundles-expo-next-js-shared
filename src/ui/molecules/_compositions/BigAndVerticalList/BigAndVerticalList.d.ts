import { StyleProp } from 'react-native'

import {
  IAdditionalComponentData,
  ISectionComponentExtra,
} from '../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'
import { IDateTranslations } from '../../../../models/Locale'
import { IResolveUrl } from '../../../../models/Navigation'
import { IGridItem } from '../../../atoms/Grid/Grid'

export type IBigAndVerticalListData = IAdditionalComponentData

export interface IBigAndVerticalListItem extends IGridItem {
  item: IBigAndVerticalListData
  resolveUrl: IResolveUrl
  dateTranslations: IDateTranslations
}

export interface IStyles {
  bigBlock: StyleProp
  title: StyleProp
  container: StyleProp
}

export interface IBigAndVerticalListProps extends ISectionsComponentCommon {
  data: IBigAndVerticalListData[]
  extra: ISectionComponentExtra
}
