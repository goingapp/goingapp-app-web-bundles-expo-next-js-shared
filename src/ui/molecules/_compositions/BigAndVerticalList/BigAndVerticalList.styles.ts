import { StyleSheet } from 'react-native'

export const useStyles = () =>
  StyleSheet.create({
    smallImagesContainer: {
      width: '50%',
    },
  })
