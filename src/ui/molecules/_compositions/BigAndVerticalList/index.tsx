import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { BigListItem } from '../BigListItem'
import { VerticalListItem } from '../VerticalListItem'
import { IBigAndVerticalListProps } from './BigAndVerticalList'
import { useStyles } from './BigAndVerticalList.styles'

export const BigAndVerticalList = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  dateTranslations,
  resolveUrl,
  isMobile,
}: IBigAndVerticalListProps) => {
  const styles = useStyles()

  const list = (
    <Grid
      rowDirection={!isMobile}
      spacing={SPACING.REGULAR}
      dataSet={cssClasses.bigAndVerticalListSection}
    >
      <BigListItem
        item={data[0]}
        resolveUrl={resolveUrl}
        dateTranslations={dateTranslations}
      />
      <Grid
        spacing={SPACING.REGULAR}
        containerStyle={!isMobile && styles.smallImagesContainer}
      >
        <VerticalListItem
          item={data[1]}
          resolveUrl={resolveUrl}
          dateTranslations={dateTranslations}
        />
        <VerticalListItem
          item={data[2]}
          resolveUrl={resolveUrl}
          dateTranslations={dateTranslations}
        />
        <VerticalListItem
          item={data[3]}
          resolveUrl={resolveUrl}
          dateTranslations={dateTranslations}
        />
      </Grid>
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={list}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid gridItemProps={gridItemProps} spacing={SPACING.LARGE}>
      {list}
    </Grid>
  )
}
