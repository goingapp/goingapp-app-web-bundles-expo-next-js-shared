import { StyleProp, TextStyle } from 'react-native'

import { IDate } from '../../../../../models/Rundate'
import {
  IPagesTag,
  ISectionComponentExtra,
} from '../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface IBigBlockData {
  description?: string
  link: string | null
  slug?: string
  date?: IDate | string
  dateDesc?: string | null
  tags?: IPagesTag[]
  thumbUrl: string
  title: string
}

export interface IBigBlockProps extends ISectionsComponentCommon {
  data: IBigBlockData[]
  extra: ISectionComponentExtra
}

export interface IStyles {
  fontColor: StyleProp<TextStyle>
}
