import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { BigBlockItem } from '../BigBlockItem'
import { IBigBlockProps } from './BigBlock'

export const BigBlock = ({
  data,
  extra,
  background,
  gridItemProps,
  id,
  resolveUrl,
  isMobile,
  containerWidth,
  isWeb,
  compositionTranslations,
}: IBigBlockProps) => {
  const blockItems = (
    <Grid spacing={SPACING.LARGE} dataSet={cssClasses.bigBlockList}>
      {data.map((item, index) => (
        <BigBlockItem
          item={item}
          key={`bigBlockBrick-${id}-${index}`}
          isWeb={isWeb}
          resolveUrl={resolveUrl}
          containerWidth={containerWidth}
          compositionTranslations={compositionTranslations}
          isMobile={isMobile}
        />
      ))}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        isMobile={isMobile}
        id={id}
        color={extra.color}
        children={blockItems}
        gridItemProps={gridItemProps}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.LARGE}
      dataSet={cssClasses.bigBlock}
      nativeID={id}
      flex={1}
    >
      {blockItems}
    </Grid>
  )
}
