import { ICompositionTranslations } from 'goingapp-app-web-bundles-expo-next-js-shared'

import { IResolveUrl } from '../../../../models/Navigation'
import { IBigBlockData } from '../BigBlock/BigBlock'

export interface IBigBlockItem {
  item: IBigBlockData
  resolveUrl: IResolveUrl
  containerWidth: number
  isWeb: boolean
  isMobile: boolean
  compositionTranslations: ICompositionTranslations
}
