import { StyleSheet } from 'react-native'

export const useStyles = () =>
  StyleSheet.create({
    container: {
      width: '50%',
    },
  })
