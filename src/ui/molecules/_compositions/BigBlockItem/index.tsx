import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { Card } from '../../Card'
import { LinkButton } from '../_reusable/LinkButton'
import { IBigBlockItem } from './BigBlock'
import { useStyles } from './BigBlockItem.styles'

export const BigBlockItem = ({
  item,
  containerWidth,
  isWeb,
  resolveUrl,
  compositionTranslations,
  isMobile,
}: IBigBlockItem) => {
  const styles = useStyles()

  return !isMobile ? (
    <Grid rowDirection={true}>
      <Card
        cover={item.thumbUrl}
        containerWidth={containerWidth}
        isWeb={isWeb}
        isMobile={isMobile}
        alt={item.title}
      >
        <></>
      </Card>

      <Grid
        spacing={SPACING.MEDIUM}
        padding={SPACING.REGULAR}
        dataSet={cssClasses.bigBlockItem}
        containerStyle={styles.container}
        justifyContent={'space-evenly'}
      >
        <Typography
          variant={TypographyVariant.h2mobile}
          children={item.title}
        />
        <Typography
          variant={TypographyVariant.body2}
          children={item.description}
        />
        {item.link && (
          <LinkButton
            url={item.link}
            label={compositionTranslations.seeMore}
            resolveUrl={resolveUrl}
            isOutlined={true}
          />
        )}
      </Grid>
    </Grid>
  ) : (
    <Card
      cover={item.thumbUrl}
      containerWidth={containerWidth}
      isWeb={isWeb}
      isMobile={isMobile}
      alt={item.title}
    >
      <Grid
        spacing={SPACING.MEDIUM}
        padding={SPACING.REGULAR}
        dataSet={cssClasses.bigBlockItem}
      >
        <Typography
          variant={TypographyVariant.h2mobile}
          children={item.title}
        />
        <Typography
          variant={TypographyVariant.body2}
          children={item.description}
        />
        {item.link && (
          <LinkButton
            url={item.link}
            label={compositionTranslations.seeMore}
            resolveUrl={resolveUrl}
            isOutlined={true}
          />
        )}
      </Grid>
    </Card>
  )
}
