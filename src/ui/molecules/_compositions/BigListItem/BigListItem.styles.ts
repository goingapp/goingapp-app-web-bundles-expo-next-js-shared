import { aspectRatio } from '../../../../constants/AspectRatio'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { IStyles } from '../BigAndVerticalList/BigAndVerticalList'

export const useStyles = makeStyles<undefined, IStyles>((theme) => ({
  container: {
    height: '100%',
  },
  bigBlock: {
    aspectRatio: aspectRatio.default[0] / aspectRatio.default[1],
    width: '100%',
    height: '100%',
  },
  title: {
    color: theme.colors.primary,
  },
}))
