import { ImageBackground } from 'react-native'

import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { getFriendlyDate } from '../../../../utils/Helpers/getFriendlyDate'
import { Link } from '../../../_reusable/Link'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { IBigAndVerticalListItem } from '../BigAndVerticalList/BigAndVerticalList'
import { useStyles } from './BigListItem.styles'

export const BigListItem = ({
  item,
  gridItemProps: bigItemGridItemProps,
  resolveUrl,
  dateTranslations,
}: IBigAndVerticalListItem) => {
  const styles = useStyles()
  const { link, thumbUrl: uri, title, date, dateDesc } = item

  if (!link) {
    return null
  }

  return (
    <Link
      url={link}
      gridItemProps={bigItemGridItemProps}
      resolveUrl={resolveUrl}
    >
      <Grid containerStyle={styles.container}>
        <ImageBackground
          style={styles.bigBlock}
          source={{ uri }}
          accessibilityLabel={title}
        >
          <Grid justifyContent={'flex-end'} flex={1} padding={SPACING.REGULAR}>
            <Typography
              variant={TypographyVariant.h2mobile}
              children={title}
              numberOfLines={3}
              style={styles.title}
            />
            {(date || dateDesc) && (
              <Typography
                variant={TypographyVariant.subtitle2}
                children={getFriendlyDate(dateTranslations, date, dateDesc)}
                numberOfLines={1}
              />
            )}
          </Grid>
        </ImageBackground>
      </Grid>
    </Link>
  )
}
