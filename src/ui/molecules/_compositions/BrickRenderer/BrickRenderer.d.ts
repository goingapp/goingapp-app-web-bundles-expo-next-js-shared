import { ComponentType } from 'react'
import { StyleProp } from 'react-native'

import { IGridItem } from '../../../atoms/Grid/Grid'
import {
  ICompositionTranslations,
  ISectionsComponent,
} from '../../../models/Compositions'
import { IDateTranslations } from '../../../models/Locale'
import { ResolveUrlType } from '../../../models/Navigation'
import { IHtmlMarkdownProps } from '../../_reusable/HtmlMarkdown/HtmlMarkdown'

export interface IStyles {
  row: StyleProp
  section: StyleProp
}

export interface IComponentRendererProps extends IGridItem {
  background: string
  components: ISectionsComponent[]
  inRow: boolean
  id: number
  resolveUrl: ResolveUrlType
  dateTranslations: IDateTranslations
  formIoAddress: string
  urlCloudinaryList: string
  cloudName: string
  urlApp: string
  compositionTranslations: ICompositionTranslations
  markdownComponent: ComponentType<IHtmlMarkdownProps>
  isBgTurnedOff?: boolean
  isLegacySafari?: boolean
  wordpressHost?: string
}
