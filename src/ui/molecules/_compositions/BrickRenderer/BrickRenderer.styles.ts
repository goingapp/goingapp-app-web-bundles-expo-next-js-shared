import { SPACING } from '../../../../constants/Grid'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { IStyles } from './BrickRenderer'

export const useStyles = makeStyles<undefined, IStyles>((theme) => ({
  section: {
    paddingVertical: theme.spacing(SPACING.LARGE),
  },
  row: {
    flexDirection: 'row',
  },
}))
