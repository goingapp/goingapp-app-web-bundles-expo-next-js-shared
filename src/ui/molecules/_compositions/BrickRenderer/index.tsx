import { useTheme } from 'react-native-paper'

import { breakpoints } from '../../../../constants/Breakpoints'
import { BricksEnum } from '../../../../constants/Compositions'
import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { brickComponents } from '../../CompositionBricks.connections'
import { ISectionAppProps } from '../Section/Section'
import { IComponentRendererProps } from './BrickRenderer'
import { useStyles } from './BrickRenderer.styles'

export const BrickRenderer = ({
  components,
  background,
  inRow,
  resolveUrl,
  dateTranslations,
  formIoAddress,
  cloudName,
  urlCloudinaryList,
  urlApp,
  compositionTranslations,
  markdownComponent,
  isWeb,
  isDesktop,
  isMobile,
  isTablet,
  noDesktop,
  noMobile,
  noTablet,
  height,
  width,
  containerWidth,
  platform,
  isLegacySafari,
  wordpressHost,
}: IComponentRendererProps & ISectionAppProps) => {
  const theme = useTheme()
  const styles = useStyles()
  const mobilePadding = SPACING.REGULAR

  return (
    <Grid
      dataSet={cssClasses.sectionInner}
      containerStyle={styles.section}
      rowDirection={inRow}
      padding={isDesktop ? 0 : SPACING.REGULAR}
    >
      <Grid spacing={SPACING.HUGE}>
        {components.map((component) => {
          const { type, data, extra, id: componentId } = component
          const componentType = BricksEnum[type]
          if (!brickComponents.hasOwnProperty(componentType)) {
            return null
          }
          const Brick = brickComponents[componentType].component
          const id = type + '-' + componentId
          const brickProps = {
            data,
            background,
            extra,
            key: id,
            id,
            specialComponentId: componentId,
            isWeb,
            isDesktop,
            isMobile,
            isTablet,
            noDesktop,
            noMobile,
            noTablet,
            height,
            width,
            containerWidth: !(isWeb && isDesktop)
              ? containerWidth - 2 * theme.spacing(mobilePadding)
              : containerWidth - (containerWidth - breakpoints.lg),
            platform,
            resolveUrl,
            dateTranslations,
            formIoAddress,
            cloudName,
            urlCloudinaryList,
            urlApp,
            compositionTranslations,
            markdownComponent,
            isLegacySafari,
            wordpressHost,
          }

          return <Brick {...brickProps} />
        })}
      </Grid>
    </Grid>
  )
}
