import { StyleProp, ViewStyle } from 'react-native'

import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ISectionsCtaButton {
  title: string
  link: string
  align: 'left' | 'center' | 'right'
  backgroundColor: string
  isBorder: boolean
  borderWidth: string
  borderColor: string
}

export interface ICTAButtonProps extends ISectionsComponentCommon {
  data: ISectionsCtaButton[]
  extra: ISectionComponentExtra
}

export interface ICTAButtonStyleProps {
  backgroundColor: string
  isBorder: boolean
  borderWidth: string
  borderColor: string
}

export interface IStyles {
  button: StyleProp<ViewStyle>
}
