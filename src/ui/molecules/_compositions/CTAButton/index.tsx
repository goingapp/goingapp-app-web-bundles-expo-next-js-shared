import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { CTAItem } from '../CTAItem'
import { ICTAButtonProps } from './CTAButton'

export const CTAButton = ({
  data,
  id,
  background,
  extra,
  gridItemProps,
  resolveUrl,
  isMobile,
}: ICTAButtonProps) => {
  const ctaItems = (
    <Grid
      spacing={SPACING.LARGE}
      padding={SPACING.REGULAR}
      dataSet={cssClasses.ctaList}
    >
      {data.map((item, index) => (
        <CTAItem
          item={item}
          key={`ctaButtonBrick-${id}-${index}`}
          resolveUrl={resolveUrl}
          color={extra.color}
          background={background}
        />
      ))}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={ctaItems}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.LARGE}
      dataSet={cssClasses.ctaButton}
      nativeID={id}
    >
      {ctaItems}
    </Grid>
  )
}
