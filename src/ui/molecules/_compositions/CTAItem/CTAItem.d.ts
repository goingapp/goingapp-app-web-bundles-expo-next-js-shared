import { IResolveUrl } from '../../../../models/Navigation'
import { ISectionsCtaButton } from '../CTAButton/CTAButton'

export interface ICTAItemProps {
  item: ISectionsCtaButton
  background?: string
  resolveUrl: IResolveUrl
  color?: string
}
