import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { ICTAButtonStyleProps, IStyles } from '../CTAButton/CTAButton'

export const useStyles = makeStyles<ICTAButtonStyleProps, IStyles>(
  (theme, props) => ({
    button: {
      backgroundColor: props?.backgroundColor,
      borderColor: props?.isBorder ? props?.borderColor : undefined,
      borderStyle: props?.isBorder ? 'solid' : undefined,
      borderWidth: props?.isBorder
        ? parseInt(props?.borderWidth, 10)
        : undefined,
    },
  })
)
