import { useTheme } from 'react-native-paper'

import { cssClasses } from '../../../../constants/CSS'
import { StyleFlexAlignItems } from '../../../../models/Styles'
import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { Grid } from '../../../atoms/Grid'
import { LinkButton } from '../_reusable/LinkButton'
import { ICTAItemProps } from './CTAItem'
import { useStyles } from './CTAItem.styles'

export const CTAItem = ({
  item,
  background,
  resolveUrl,
  color,
}: ICTAItemProps) => {
  const isBgDark = isColorDark(item.backgroundColor || background)
  const theme = useTheme()
  const styles = useStyles({
    backgroundColor: item.backgroundColor,
    isBorder: item.isBorder,
    borderWidth: item.borderWidth,
    borderColor: item.borderColor,
  })

  const alignItems: { [key: string]: StyleFlexAlignItems } = {
    left: 'flex-start',
    center: 'center',
    right: 'flex-end',
  }

  return (
    <Grid alignItems={alignItems[item.align]} dataSet={cssClasses.ctaItem}>
      <LinkButton
        url={item.link}
        label={item.title}
        style={styles.button}
        textColor={color || isBgDark ? theme.colors.white : theme.colors.black}
        resolveUrl={resolveUrl}
      />
    </Grid>
  )
}
