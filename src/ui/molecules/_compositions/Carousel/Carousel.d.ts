import { StyleProp } from 'react-native'

import { ISectionComponentExtra } from '../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'
import { IResolveUrl } from '../../../../models/Navigation'
import { ITag } from '../../../../models/Tag'

export interface ICarouselData {
  alt?: string
  description?: string
  link?: string
  src: string
  thumbUrl?: string
  title?: string
  tags?: ITag[]
}

export interface ICarouselRenderItem {
  index: number
  item: ICarouselData
}

export interface ICarouselProps extends ISectionsComponentCommon {
  data: ICarouselData[]
  extra: ISectionComponentExtra
}

export interface IUseCarousel {
  id: string
  isMobile: boolean
  resolveUrl: IResolveUrl
  styles: IStyles
}

export interface ICarouselSlideProps extends ICarouselData {
  isMobile: boolean
  styles: IStyles
}

export interface IStyles {
  container: StyleProp
  dotsContainer: StyleProp
  paginationDot: StyleProp
  slideTitleText: StyleProp
  slideContainer: StyleProp
  contentBox: StyleProp
}
