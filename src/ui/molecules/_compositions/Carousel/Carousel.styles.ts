import { SPACING } from '../../../../constants/Grid'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { IStyles } from './Carousel'

export const useStyles = makeStyles<
  { isDesktop: boolean; containerWidth: number; isWeb: boolean },
  IStyles
>((theme, props) => ({
  container: {
    flex: 1,
  },
  contentBox: {
    position: 'absolute',
    left: theme.spacing(SPACING.REGULAR),
    bottom: theme.spacing(props?.isWeb ? SPACING.HUMONGOUS : SPACING.HUGE),
  },
  dotsContainer: {
    padding: theme.spacing(SPACING.LARGE),
  },
  paginationDot: {
    height: 12,
    marginLeft: 0,
    marginRight: theme.spacing(SPACING.SMALL),
    width: 12,
  },
  slideContainer: {
    width: props?.containerWidth,
  },
  slideTitleText: {
    color: theme.colors.white,
    textAlign: 'left',
  },
}))
