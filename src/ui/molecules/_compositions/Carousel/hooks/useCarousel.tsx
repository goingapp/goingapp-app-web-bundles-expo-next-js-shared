import { useRef } from 'react'

import { SwiperFlatList } from 'react-native-swiper-flatlist'

import { Link } from '../../../../_reusable/Link'
import { CarouselSlide } from '../../CarouselSlide'
import { ICarouselData, ICarouselRenderItem, IUseCarousel } from '../Carousel'

export const useCarousel = ({
  id,
  isMobile,
  resolveUrl,
  styles,
}: IUseCarousel) => {
  const ref = useRef<SwiperFlatList>(null)

  const renderItem = ({ item }: ICarouselRenderItem) => {
    const { alt, link, src, thumbUrl, title, description, tags } = item
    const imageUrl = isMobile ? thumbUrl || src : src

    if (link) {
      return (
        <Link url={link} resolveUrl={resolveUrl}>
          <CarouselSlide
            src={imageUrl}
            alt={alt}
            title={title}
            isMobile={isMobile}
            styles={styles}
            description={description}
            tags={tags}
          />
        </Link>
      )
    }

    return (
      <CarouselSlide
        src={imageUrl}
        alt={alt}
        isMobile={isMobile}
        styles={styles}
        description={description}
        tags={tags}
      />
    )
  }

  const keyExtractor = (_item: ICarouselData, index: number) =>
    `carousel-${id}-${index}`

  return {
    keyExtractor,
    renderItem,
    ref,
  }
}
