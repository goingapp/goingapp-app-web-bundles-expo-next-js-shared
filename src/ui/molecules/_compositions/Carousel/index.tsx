import { useTheme } from 'react-native-paper'
import { SwiperFlatList } from 'react-native-swiper-flatlist'

import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { ICarouselProps } from './Carousel'
import { useStyles } from './Carousel.styles'
import { useCarousel } from './hooks/useCarousel'

export const Carousel = ({
  data,
  id,
  background,
  extra,
  gridItemProps,
  isMobile,
  isDesktop,
  isWeb,
  containerWidth,
  resolveUrl,
}: ICarouselProps) => {
  const styles = useStyles({ containerWidth, isDesktop, isWeb })
  const { renderItem, keyExtractor, ref } = useCarousel({
    id,
    isMobile,
    resolveUrl,
    styles,
  })
  const theme = useTheme()
  const showControls = data.length > 1

  const carousel = (
    <Grid dataSet={cssClasses.carouselInner}>
      <SwiperFlatList
        autoplay={true}
        autoplayDelay={5}
        autoplayLoop={true}
        autoplayLoopKeepAnimation={true}
        index={0}
        showPagination={showControls}
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        ref={ref}
        paginationActiveColor={theme.colors.text}
        paginationDefaultColor={theme.colors.textSecondary}
        paginationStyle={styles.dotsContainer}
        paginationStyleItem={styles.paginationDot}
      />
    </Grid>
  )
  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        isMobile={isMobile}
        id={id}
        children={carousel}
        gridItemProps={gridItemProps}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.LARGE}
      dataSet={cssClasses.carouselOuter}
    >
      {carousel}
    </Grid>
  )
}
