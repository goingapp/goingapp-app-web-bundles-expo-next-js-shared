import { Carousel as ResponsiveCarousel } from 'react-responsive-carousel'

import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { ICarouselProps } from './Carousel'
import { useStyles } from './Carousel.styles'
import { useCarousel } from './hooks/useCarousel'

export const Carousel = ({
  data,
  id,
  background,
  extra,
  gridItemProps,
  isMobile,
  isDesktop,
  isWeb,
  containerWidth,
  resolveUrl,
}: ICarouselProps) => {
  const styles = useStyles({ containerWidth, isDesktop, isWeb })

  const { renderItem: Slide } = useCarousel({
    id,
    isMobile,
    resolveUrl,
    styles,
  })
  const slides = data.map((item, index) => <Slide index={index} item={item} />)
  const showControls = data.length > 1
  const carousel = (
    <ResponsiveCarousel
      autoPlay={true}
      children={slides}
      dynamicHeight={true}
      emulateTouch={true}
      infiniteLoop={true}
      showArrows={showControls}
      showIndicators={showControls}
      showStatus={false}
      showThumbs={false}
      swipeable={true}
      preventMovementUntilSwipeScrollTolerance={true}
      swipeScrollTolerance={50}
      useKeyboardArrows={false}
    />
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        isMobile={isMobile}
        id={id}
        children={carousel}
        gridItemProps={gridItemProps}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.LARGE}
      dataSet={cssClasses.carouselOuter}
    >
      {carousel}
    </Grid>
  )
}
