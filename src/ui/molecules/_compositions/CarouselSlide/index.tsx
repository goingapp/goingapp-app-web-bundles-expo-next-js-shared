import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { Grid } from '../../../atoms/Grid'
import { ResponsiveImage } from '../../../atoms/ResponsiveImage'
import { Typography } from '../../../atoms/Typography'
import { TagsBar } from '../../TagsBar'
import { ICarouselSlideProps } from '../Carousel/Carousel'

export const CarouselSlide = ({
  src,
  alt,
  title,
  isMobile,
  styles,
  description,
  tags,
}: Omit<ICarouselSlideProps, 'thumbUrl'>) => (
  <Grid containerStyle={styles.slideContainer}>
    <ResponsiveImage
      alt={alt}
      imageUrl={src}
      isOriginalAspectRatio={!isMobile}
    />
    <Grid spacing={SPACING.SMALL} containerStyle={styles.contentBox}>
      {tags && <TagsBar tags={tags} />}
      {title && (
        <Typography
          children={title}
          variant={!isMobile ? TypographyVariant.h2 : TypographyVariant.h4}
          style={styles.slideTitleText}
          dataSet={cssClasses.carouselTitle}
        />
      )}
      {description && (
        <Typography
          children={description}
          style={styles.slideTitleText}
          dataSet={cssClasses.carouselDescription}
          variant={TypographyVariant.body1}
        />
      )}
    </Grid>
  </Grid>
)
