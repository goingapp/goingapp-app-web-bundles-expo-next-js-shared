import { StyleProp } from 'react-native'

import { ISectionComponentExtra } from '../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ICustomHtmlData {
  cssSrc?: string
  htmlSrc: string
}

export interface ICustomHtmlProps extends ISectionsComponentCommon {
  data: ICustomHtmlData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}

export interface ICustomHtmlStyleProps {
  width: number
  height: number
}

export interface IStyles {
  container: StyleProp
  webview: StyleProp
  loader: StyleProp
}
