import { SPACING } from '../../../../constants/Grid'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { ICustomHtmlStyleProps, IStyles } from './CustomHtml'

export const useStyles = makeStyles<ICustomHtmlStyleProps, IStyles>(
  (theme, props) => ({
    container: {
      flex: 1,
      position: 'relative',
    },
    webview: {
      flex: 1,
      backgroundColor: 'transparent',
      width: props?.width
        ? props?.width - 2 * theme.spacing(SPACING.REGULAR)
        : undefined,
    },
    loader: {
      position: 'absolute',
      top: 0,
      left: 0,
      width: props?.width
        ? props?.width - 2 * theme.spacing(SPACING.REGULAR)
        : undefined,
      height: props?.height,
      backgroundColor: theme.colors.surface,
    },
  })
)
