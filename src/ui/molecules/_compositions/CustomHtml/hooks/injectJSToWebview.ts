export const injectedJavaScript = `
  try {
     var body = document.querySelector("body");
     body.style.backgroundColor = "transparent";

    document.addEventListener("click", function handler(evt) {
      var href = evt.target.tagName === 'A' && !!evt.target.href;
      var parentHref = evt.target.parentNode.tagName === 'A' && !!evt.target.parentNode.href;
      var grandParentHref = evt.target.parentNode.parentNode.tagName === 'A' && !!evt.target.parentNode.parentNode.href;

      if (href) {
        evt.preventDefault();
        evt.stopPropagation();
        evt.stopImmediatePropagation();

        window.ReactNativeWebView.postMessage(JSON.stringify({redirect: evt.target.href}));
      } else if (parentHref) {
        evt.preventDefault();
        evt.stopPropagation();
        evt.stopImmediatePropagation();

        window.ReactNativeWebView.postMessage(JSON.stringify({redirect: evt.target.parentNode.href}));
      } else if (grandParentHref) {
        evt.preventDefault();
        evt.stopPropagation();
        evt.stopImmediatePropagation();

        window.ReactNativeWebView.postMessage(JSON.stringify({redirect: evt.target.parentNode.parentNode.href}));
      }
    }, true);
  } catch (e) {}

  true;
`
