import { useEffect, useState } from 'react'

import useSWR from 'swr'

import { loadDOMStyle } from '../../../../../utils/Helpers/loadDOMStyle'
import { Logger } from '../../../../../utils/Logger'
import { ICustomHtmlData } from '../CustomHtml'

export const useCustomHtml = ({ data }: { data: ICustomHtmlData[] }) => {
  const getHtml = async (url: string) => {
    try {
      const data = await fetch(url)

      return data.text()
    } catch (error: any) {
      Logger.error('getHtml', error.message)
    }
  }

  const { htmlSrc, cssSrc } = data[0]
  const { data: htmlCode } = useSWR(htmlSrc, getHtml)
  const [html, setHtml] = useState('')

  useEffect(() => {
    if (cssSrc) {
      loadDOMStyle(cssSrc)
    }
  }, [cssSrc])

  useEffect(() => {
    if (htmlCode) {
      setHtml(htmlCode)
    }
  }, [htmlCode])

  return {
    html,
  }
}
