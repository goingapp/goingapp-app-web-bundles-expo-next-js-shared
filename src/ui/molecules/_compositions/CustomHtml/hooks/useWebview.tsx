import { useEffect, useRef, useState } from 'react'

import WebView, { WebViewMessageEvent } from 'react-native-webview'
import {
  WebViewErrorEvent,
  WebViewNavigation,
} from 'react-native-webview/lib/WebViewTypes'

import { IResolveUrl } from '../../../../../models/Navigation'
import { injectedJavaScript } from './injectJSToWebview'

export const useWebview = ({
  htmlSrc,
  resolveUrl,
}: {
  htmlSrc: string
  resolveUrl: IResolveUrl
}) => {
  const webviewRef = useRef<WebView>(null)
  const [isReady, setIsReady] = useState(false)
  const [uri, setUri] = useState<string | null>(null)

  const onMessage = async (event: WebViewMessageEvent) => {
    let data: { [key: string]: any } | string = event.nativeEvent.data as string

    try {
      data = JSON.parse(data)
    } catch (e) {}

    if (data && typeof data !== 'string' && data.redirect) {
      await resolveUrl(data.redirect)
    }
  }

  const onError = (error: WebViewErrorEvent) => {
    console.error('[Webview Error]', error)
  }

  const onNavigationStateChange = (event: WebViewNavigation) => {
    console.debug('[Webview onNavigationStateChange]', event)

    if (!event.loading && !event.canGoBack) {
      setIsReady(true)
    }
  }

  useEffect(() => {
    if (isReady) {
      webviewRef.current?.injectJavaScript(injectedJavaScript)
    }
  }, [isReady])

  useEffect(() => {
    setUri(htmlSrc)
  }, [htmlSrc])

  return {
    isReady,
    webviewRef,
    onError,
    onMessage,
    onNavigationStateChange,
    uri,
  }
}
