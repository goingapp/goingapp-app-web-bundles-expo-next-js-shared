import AutoHeightWebView from 'react-native-autoheight-webview'
import { ActivityIndicator, useTheme } from 'react-native-paper'

import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { ICustomHtmlProps } from './CustomHtml'
import { useStyles } from './CustomHtml.styles'
import { useWebview } from './hooks/useWebview'

export const CustomHtml = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  width,
  height,
  isMobile,
  resolveUrl,
}: ICustomHtmlProps) => {
  const { htmlSrc } = data[0]
  const { colors } = useTheme()
  const {
    webviewRef,
    onNavigationStateChange,
    onMessage,
    onError,
    isReady,
    uri,
  } = useWebview({ htmlSrc, resolveUrl })
  const styles = useStyles({
    height,
    width,
  })

  const customHtmlRender = (
    <>
      {!!uri && (
        <AutoHeightWebView
          ref={webviewRef}
          javaScriptEnabled={true}
          onNavigationStateChange={onNavigationStateChange}
          onError={onError}
          onMessage={onMessage}
          bounces={false}
          style={styles.webview}
          source={{ uri: htmlSrc }}
          allowsInlineMediaPlayback={true}
        />
      )}

      {!isReady && (
        <ActivityIndicator
          style={styles.loader}
          color={colors.text}
          size={'small'}
          children
        />
      )}
    </>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={customHtmlRender}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid gridItemProps={gridItemProps} containerStyle={styles.container}>
      {customHtmlRender}
    </Grid>
  )
}
