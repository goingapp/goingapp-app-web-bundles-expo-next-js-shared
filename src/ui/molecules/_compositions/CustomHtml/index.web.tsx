import { useTheme } from 'react-native-paper'

import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { ICustomHtmlProps } from './CustomHtml'
import { useCustomHtml } from './hooks/useCustomHtml'

export const CustomHtml = ({
  data,
  extra,
  id,
  background,
  gridItemProps,
  isMobile,
  containerWidth,
}: ICustomHtmlProps) => {
  const theme = useTheme()
  const width = containerWidth - 2 * theme.spacing(SPACING.REGULAR)
  const { html } = useCustomHtml({ data })

  const customHtmlRender = (
    <div
      id={id}
      style={{ width, minHeight: '100vh' }}
      dangerouslySetInnerHTML={{ __html: html }}
      className={cssClasses.customHtmlInner.cssClass}
    />
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={customHtmlRender}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      containerStyle={{ width }}
      dataSet={cssClasses.customHtml}
    >
      {customHtmlRender}
    </Grid>
  )
}
