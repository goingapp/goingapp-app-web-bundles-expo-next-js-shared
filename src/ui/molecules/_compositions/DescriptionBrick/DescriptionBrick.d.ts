import {
  ISectionComponentExtra,
  ISectionsComponentCommon,
} from '../../../../models/Compositions'
import { IGridItem } from '../../../atoms/Grid/Grid'

export interface IDescriptionBrickData {
  description: string
  formattedDescription: string
}

export interface IDescriptionBrickItem extends IGridItem {
  item: IDescriptionBrickData
}

export interface IDescriptionBrickProps extends ISectionsComponentCommon {
  data: IDescriptionBrickData[]
  extra: ISectionComponentExtra
}
