import { StyleSheet } from 'react-native'

import { SPACING } from '../../../../constants/Grid'

export const useStyles = ({
  theme,
}: {
  theme: ReactNativePaper.Theme
  windowWidth: number
}) =>
  StyleSheet.create({
    description: {
      paddingHorizontal: theme.spacing(SPACING.REGULAR),
    },
    maxTextWidth: {
      alignSelf: 'center',
      width: theme.breakpoints.sm,
    },
  })
