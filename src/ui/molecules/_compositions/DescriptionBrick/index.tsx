import { useTheme } from 'react-native-paper'

import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import {
  IDescriptionBrickItem,
  IDescriptionBrickProps,
} from './DescriptionBrick'
import { useStyles } from './DescriptionBrick.styles'

export const DescriptionBrick = ({
  data,
  extra: { color, sectionTitle, sectionDescription },
  background,
  id,
  gridItemProps,
  noMobile,
  isMobile,
  width,
  markdownComponent: HtmlMarkdown,
}: IDescriptionBrickProps) => {
  const theme = useTheme()
  const isBgDark = isColorDark(background)
  const autoColor = isBgDark ? theme.colors.white : theme.colors.black
  const styles = useStyles({ theme, windowWidth: width })

  const DescriptionBrickItem = ({
    item,
    gridItemProps: DBIGridItemProps,
  }: IDescriptionBrickItem) => {
    const { formattedDescription } = item

    return (
      <HtmlMarkdown
        markdownString={formattedDescription}
        fontColor={color ?? autoColor}
        gridItemProps={DBIGridItemProps}
      />
    )
  }

  const descriptionBlock = (
    <Grid
      spacing={SPACING.LARGE}
      containerStyle={[styles.description, noMobile && styles.maxTextWidth]}
      dataSet={cssClasses.description}
      nativeID={id}
    >
      {data.map((item, index) => (
        <DescriptionBrickItem
          item={item}
          key={`descriptionBrick-${id}-${index}`}
        />
      ))}
    </Grid>
  )

  if (sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={sectionTitle}
        description={sectionDescription}
        background={background}
        color={color}
        id={id}
        children={descriptionBlock}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid gridItemProps={gridItemProps} spacing={SPACING.LARGE}>
      {descriptionBlock}
    </Grid>
  )
}
