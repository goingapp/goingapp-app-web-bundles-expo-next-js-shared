import { StyleProp } from 'react-native'

import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface IEmbedData {
  embedSrc: string
  title: string
}

export interface IEmbedProps extends ISectionsComponentCommon {
  data: IEmbedData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}

export interface IEmbedStyleProps {
  width: number
}

export interface IStyles {
  webview: StyleProp
}
