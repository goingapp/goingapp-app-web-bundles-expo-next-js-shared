import { StyleSheet } from 'react-native'

import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { IEmbedStyleProps, IStyles } from './Embed'

export const useStyles = makeStyles<IEmbedStyleProps, IStyles>((theme, props) =>
  StyleSheet.create({
    webview: {
      height: 300,
      flex: 1,
      width: props?.width,
    },
  })
)
