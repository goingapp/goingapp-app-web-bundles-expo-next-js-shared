import { SPACING } from '../../../../constants/Grid'
import { YouTube } from '../../../_reusable/YouTube'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { IEmbedProps } from './Embed'

export const Embed = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  containerWidth,
  isMobile,
}: IEmbedProps) => {
  const src = `${data[0].embedSrc}`

  const embed = (
    <Grid spacing={SPACING.LARGE} flex={1}>
      <YouTube
        teaser={src}
        containerWidth={containerWidth}
        isHideControls={true}
        padding={SPACING.REGULAR}
      />
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={embed}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return <Grid gridItemProps={gridItemProps}>{embed}</Grid>
}
