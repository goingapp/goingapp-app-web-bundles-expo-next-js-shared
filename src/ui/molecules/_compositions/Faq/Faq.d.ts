import { StyleProp } from 'react-native'

import {
  ISectionComponentExtra,
  ISectionsComponentCommon,
} from '../../../../models/Compositions'

export interface IFaqData {
  description: string
  title: string
}

export interface IFaqProps extends ISectionsComponentCommon {
  data: IFaqData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}

export interface IFaqStyleProps {
  color?: string
  background?: string
}

export interface IStyles {
  divider: StyleProp
  fontColor: StyleProp
  maxTextWidth: StyleProp
}
