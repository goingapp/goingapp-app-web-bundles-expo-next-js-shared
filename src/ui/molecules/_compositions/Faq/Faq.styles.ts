import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { IFaqStyleProps, IStyles } from './Faq'

export const useStyles = makeStyles<IFaqStyleProps, IStyles>(
  (theme, props) => ({
    divider: {
      backgroundColor: theme.colors.textSecondary,
    },
    fontColor: {
      color: props?.color
        ? props.color
        : isColorDark(props?.background)
        ? theme.colors.white
        : theme.colors.black,
    },
    maxTextWidth: {
      alignSelf: 'center',
      width: theme.breakpoints.sm,
    },
  })
)
