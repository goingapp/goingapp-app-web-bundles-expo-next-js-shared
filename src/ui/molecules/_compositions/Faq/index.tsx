import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { FaqElement } from '../FaqElement'
import { IFaqProps } from './Faq'
import { useStyles } from './Faq.styles'

export const Faq = ({
  data,
  extra: { color, sectionDescription, sectionTitle },
  background,
  id,
  gridItemProps,
  isMobile,
}: IFaqProps) => {
  const styles = useStyles({ color, background })

  const faq = (
    <Grid padding={SPACING.REGULAR} spacing={SPACING.LARGE}>
      {data.map((item, index) => (
        <FaqElement
          item={item}
          key={`faq-${id}-${index}`}
          isMobile={isMobile}
          styles={styles}
        />
      ))}
    </Grid>
  )

  if (sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={sectionTitle}
        description={sectionDescription}
        background={background}
        color={color}
        id={id}
        children={faq}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return <Grid spacing={SPACING.LARGE}>{faq}</Grid>
}
