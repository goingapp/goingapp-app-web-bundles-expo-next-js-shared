import { StyleProp } from 'react-native'

import { IGridItem } from '../../../atoms/Grid/Grid'
import { IFaqData } from '../../bricks/Components/Faq/Faq'

export interface IFaqElement extends IGridItem {
  item: IFaqData
  isMobile: boolean
  styles: StyleProp
}
