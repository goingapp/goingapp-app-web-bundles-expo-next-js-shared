import { Divider } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { IFaqElement } from './FaqElement'

export const FaqElement = ({
  item,
  gridItemProps: elementGridItemProps,
  isMobile,
  styles,
}: IFaqElement) => {
  const { description, title } = item

  return (
    <Grid
      spacing={SPACING.LARGE}
      gridItemProps={elementGridItemProps}
      containerStyle={!isMobile && styles.maxTextWidth}
    >
      <Grid spacing={SPACING.REGULAR}>
        <Typography
          variant={TypographyVariant.h5}
          children={title}
          style={styles.fontColor}
        />
        <Typography
          variant={TypographyVariant.subtitle1}
          children={description}
          style={styles.fontColor}
        />
      </Grid>
      <Grid>
        <Divider style={styles.divider} />
      </Grid>
    </Grid>
  )
}
