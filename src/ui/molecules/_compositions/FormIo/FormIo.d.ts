import { StyleProp } from 'react-native'

import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface IFormioData {
  formSlug: string
  id?: string
}

export interface IFormIoProps extends ISectionsComponentCommon {
  data: IFormioData[]
  extra: ISectionComponentExtra
}

export interface IFormIoStyleProps {
  width: number
}

export interface IStyles {
  container: StyleProp
}
