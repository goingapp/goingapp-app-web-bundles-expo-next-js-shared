import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { IFormIoStyleProps, IStyles } from './FormIo'

export const useStyles = makeStyles<IFormIoStyleProps, IStyles>(
  (theme, props) => ({
    container: {
      width: props?.width,
    },
  })
)
