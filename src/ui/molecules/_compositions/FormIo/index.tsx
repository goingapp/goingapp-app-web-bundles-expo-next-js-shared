import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { LinkButton } from '../_reusable/LinkButton'
import { IFormIoProps } from './FormIo'

export const FormIo = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  isMobile,
  resolveUrl,
  urlApp,
  compositionTranslations,
}: IFormIoProps) => {
  const { formSlug } = data[0]
  const url = `${urlApp}/form/${formSlug}`

  const formIo = (
    <LinkButton
      url={url}
      label={compositionTranslations.openForm}
      resolveUrl={resolveUrl}
      textColor={extra.color}
      gridItemProps={gridItemProps}
    />
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={formIo}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return formIo
}
