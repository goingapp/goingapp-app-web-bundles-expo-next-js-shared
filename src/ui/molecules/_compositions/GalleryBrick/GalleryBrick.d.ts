import {
  ISectionComponentExtra,
  ISectionsComponentCommon,
} from '../../../../models/Compositions'

export interface IGalleryBrickImage {
  alt: string
  ocSrc: string
  src: string
}

export interface IGalleryBrickData {
  cloudinaryTag?: string
  images?: IGalleryBrickImage[]
}

export interface IGalleryBrickProps extends ISectionsComponentCommon {
  data: IGalleryBrickData[]
  extra: ISectionComponentExtra
}
