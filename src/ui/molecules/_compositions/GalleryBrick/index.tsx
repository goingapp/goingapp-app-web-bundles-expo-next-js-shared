import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { GalleryBrickItem } from '../GalleryBrickItem'
import { IGalleryBrickProps } from './GalleryBrick'

export const GalleryBrick = ({
  data,
  background,
  extra,
  gridItemProps,
  id,
  cloudName,
  urlCloudinaryList,
  isMobile,
  isDesktop,
}: IGalleryBrickProps) => {
  const gallery = (
    <Grid spacing={SPACING.LARGE}>
      {data.map((item, index) => (
        <GalleryBrickItem
          key={`gallery-${id}-${index}`}
          item={item}
          cloudName={cloudName}
          urlCloudinaryList={urlCloudinaryList}
          isDesktop={isDesktop}
        />
      ))}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={gallery}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid spacing={SPACING.LARGE} gridItemProps={gridItemProps}>
      {gallery}
    </Grid>
  )
}
