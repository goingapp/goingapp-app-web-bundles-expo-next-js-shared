import { IGridItem } from '../../../atoms/Grid/Grid'
import { IGalleryBrickData } from '../GalleryBrick/GalleryBrick'

export interface IGalleryBrickItem extends IGridItem {
  item: IGalleryBrickData
  cloudName: string
  urlCloudinaryList: string
  isDesktop?: boolean
}
