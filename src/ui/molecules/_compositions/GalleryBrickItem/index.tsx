import { useCloudinaryApi } from '../../../../hooks/useCloudinaryApi'
import { Gallery } from '../../Gallery'
import { IGalleryBrickItem } from './GalleryBrickItem'

export const GalleryBrickItem = ({
  item,
  gridItemProps: galleryItemGridProps,
  cloudName,
  urlCloudinaryList,
  isDesktop,
}: IGalleryBrickItem) => {
  const { cloudinaryTag, images } = item
  const cldImages = useCloudinaryApi(
    cloudName,
    urlCloudinaryList,
    cloudinaryTag
  )

  const imagesUrls = cloudinaryTag
    ? cldImages.cloudinaryImagesUrls
    : images?.map((image) => image.src)

  if (!imagesUrls) {
    return null
  }

  return (
    <Gallery
      images={imagesUrls}
      gridItemProps={galleryItemGridProps}
      isDesktop={isDesktop}
    />
  )
}
