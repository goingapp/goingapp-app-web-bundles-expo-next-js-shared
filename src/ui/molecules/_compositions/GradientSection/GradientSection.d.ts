import { IStyles } from '../Section/Section'

export interface IGradientSectionProps {
  background: string
  nativeID: string
  isDesktop: boolean
  styles: IStyles
  renderer: JSX.Element
}
