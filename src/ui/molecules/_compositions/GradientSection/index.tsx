import { LinearGradient } from 'expo-linear-gradient'

import { IGradientSectionProps } from './GradientSection'

export const GradientSection = ({
  background,
  nativeID,
  isDesktop,
  styles,
  renderer,
}: IGradientSectionProps) => (
  <LinearGradient
    colors={[background, 'transparent', background]}
    start={{ x: 0, y: 0 }}
    end={{ x: 0, y: 1 }}
    style={[isDesktop && styles.desktopMaxWidth]}
    nativeID={nativeID}
  >
    {renderer}
  </LinearGradient>
)
