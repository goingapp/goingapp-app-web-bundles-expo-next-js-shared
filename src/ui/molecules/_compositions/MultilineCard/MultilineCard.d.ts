import { StyleProp } from 'react-native'

import { IDateTranslations } from '../../../../models/Locale'
import { IResolveUrl } from '../../../../models/Navigation'
import { IMultilineData } from '../MultilineCards/MultilineCards'

export interface IMultilineCardStylesProps {
  background?: string
  color?: string
}

export interface IStyles {
  container: StyleProp
  globalFontColor: StyleProp
  multilineCardOuter: StyleProp
}

export interface IMultilineCardProps {
  containerWidth: number
  item: IMultilineData
  index: number
  isContent: boolean
  isMoreButton: boolean
  isHorizontal: boolean
  isMobile: boolean
  isTablet: boolean
  isWeb: boolean
  maxInRow: number
  moreButtonText?: string
  wordpressHost?: string
  dateTranslations: IDateTranslations
  resolveUrl: IResolveUrl
  styles: StyleProp
}
