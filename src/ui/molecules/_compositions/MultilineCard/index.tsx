import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import {
  ButtonVariant,
  TypographyVariant,
} from '../../../../constants/Variants'
import { useWordpressCard } from '../../../../hooks/wordpress/useWordpressCard'
import { IDate } from '../../../../models/DateTime'
import { DateTime } from '../../../../services/Date/DateTime'
import { createCompositionsCardUrl } from '../../../../utils/Helpers/createCompositionsCardUrl'
import { Link } from '../../../_reusable/Link'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { Card } from '../../Card'
import { TagsBar } from '../../TagsBar'
import { LinkButton } from '../_reusable/LinkButton'
import { IMultilineCardProps } from './MultilineCard'

export const MultilineCard = ({
  item: {
    badge,
    date,
    dateDesc,
    place,
    subtitle,
    thumbUrl,
    title,
    description,
    link,
    slug,
    ctaButton,
    secondaryCtaButton,
  },
  index,
  isContent,
  isMobile,
  isTablet,
  isHorizontal,
  maxInRow,
  styles,
  isMoreButton,
  moreButtonText,
  dateTranslations,
  isWeb,
  containerWidth,
  resolveUrl,
  wordpressHost,
}: IMultilineCardProps) => {
  const { isWpArticle, postCats, tagsStyles, wpAuthorsData } = useWordpressCard(
    link,
    wordpressHost
  )
  const preparedLink = createCompositionsCardUrl(link, slug)
  const dateVal = (date?: IDate, dateDesc?: string) =>
    dateDesc
      ? dateDesc
      : date
      ? DateTime.getFriendlyDate({
          isoDate: DateTime.getIsoDateString(date),
          dateTranslate: dateTranslations,
        })
      : undefined

  const MultilineItem = () => {
    return (
      <Card
        isSliderCard={isHorizontal}
        cover={thumbUrl}
        style={styles.container}
        tags={badge && badge.name.length > 0 ? [badge.name] : undefined}
        tagColor={badge?.type}
        cardsInRow={isMobile ? 1 : maxInRow}
        gridItemProps={{
          rowDirection: isHorizontal || !isMobile,
          spacing: SPACING.REGULAR,
          isFirst: index === 0 || (!isMobile && index % maxInRow === 0), // TODO: check if that's really neccessary
        }}
        isWeb={isWeb}
        isMobile={isMobile}
        containerWidth={containerWidth}
        alt={title}
      >
        {isContent && (
          <Grid
            spacing={SPACING.REGULAR}
            padding={SPACING.REGULAR}
            containerStyle={styles.multilineCardOuter}
            flex={1}
            dataSet={cssClasses.multilineCardOuter}
          >
            <Grid
              spacing={SPACING.SMALL}
              dataSet={cssClasses.multilineCardInnerUpper}
            >
              {isWpArticle ? (
                <TagsBar
                  tags={postCats}
                  tagStyle={tagsStyles.tag}
                  tagTextStyle={tagsStyles.tagLabel}
                  resolveUrl={resolveUrl}
                  isWpArticle={isWpArticle}
                />
              ) : undefined}
              <Typography
                variant={
                  isMobile || isTablet
                    ? TypographyVariant.h4mobile
                    : TypographyVariant.h4
                }
                numberOfLines={3}
                children={title}
                style={styles.globalFontColor}
                dataSet={cssClasses.multilineTitle}
              />
              <Typography
                variant={TypographyVariant.subtitle1}
                numberOfLines={isHorizontal || !isMobile ? 3 : undefined}
                children={subtitle}
                style={styles.globalFontColor}
                dataSet={cssClasses.multilineSubtitle}
              />
              <Typography
                variant={TypographyVariant.subtitle1}
                children={
                  !!wpAuthorsData?.name
                    ? dateVal(date, dateDesc) + '\r\n' + `${wpAuthorsData.name}`
                    : dateVal(date, dateDesc)
                }
                style={styles.globalFontColor}
                dataSet={cssClasses.multilineDate}
              />
              <Typography
                variant={TypographyVariant.subtitle2}
                children={place}
                style={styles.globalFontColor}
                dataSet={cssClasses.multilinePlace}
              />
              <Typography
                variant={TypographyVariant.body2}
                children={description}
                style={styles.globalFontColor}
                dataSet={cssClasses.multilineDescription}
              />
            </Grid>
            <Grid
              spacing={SPACING.SMALL}
              dataSet={cssClasses.multilineCardInnerLower}
              justifyContent={'flex-end'}
              flex={1}
            >
              {ctaButton?.link && ctaButton?.label && (
                <LinkButton
                  url={ctaButton.link}
                  label={ctaButton.label}
                  textColor={ctaButton.color}
                  bgColor={ctaButton.bgColor}
                  resolveUrl={resolveUrl}
                />
              )}
              {secondaryCtaButton?.link && secondaryCtaButton?.label && (
                <LinkButton
                  url={secondaryCtaButton.link}
                  label={secondaryCtaButton.label}
                  textColor={secondaryCtaButton.color}
                  bgColor={secondaryCtaButton.bgColor}
                  resolveUrl={resolveUrl}
                />
              )}
              {isMoreButton && moreButtonText && preparedLink && (
                <LinkButton
                  url={preparedLink}
                  label={moreButtonText}
                  variant={ButtonVariant.medium}
                  resolveUrl={resolveUrl}
                />
              )}
            </Grid>
          </Grid>
        )}
      </Card>
    )
  }

  if (!isMoreButton && preparedLink) {
    return (
      <Link
        url={preparedLink}
        resolveUrl={resolveUrl}
        style={isWeb ? { maxWidth: 'fit-content' } : undefined}
      >
        <Grid dataSet={cssClasses.hoverElement} flex={1}>
          <MultilineItem />
        </Grid>
      </Link>
    )
  }

  return <MultilineItem />
}
