import { StyleProp, ViewStyle } from 'react-native'

import {
  ICardData,
  ICardExtra,
  ISectionsComponentCommon,
} from '../../../../models/Compositions'
import { IDateTranslations } from '../../../../models/Locale'
import { IResolveUrl } from '../../../../models/Navigation'
import { IGridItem } from '../../../atoms/Grid/Grid'

export interface IMultilineCtaButton {
  label?: string
  link?: string
  disabled?: boolean
  bgColor?: string
  color?: string
}

export interface IMultilineData extends ICardData {
  subtitle?: string
  ctaButton?: IMultilineCtaButton
  secondaryCtaButton?: IMultilineCtaButton
}

export interface IStyles {
  item: StyleProp<ViewStyle>
}

export interface IMultilineCardsProps extends ISectionsComponentCommon {
  data: IMultilineData[]
  extra: IMultilineItemExtra
}

export interface IMultilineItemExtra extends ICardExtra {
  maxInRow: number
  isHorizontal: boolean
  fixedWidthCards: boolean
  background?: string
  moreButtonText?: string
}

export interface IUseMultilineCardsProps extends IMultilineItemExtra {
  id: string
  isMobile: boolean
  isTablet: boolean
  isWeb: boolean
  dateTranslations: IDateTranslations
  containerWidth: number
  resolveUrl: IResolveUrl
  wordpressHost?: string
}

export interface IRenderMultilineItem extends IGridItem {
  item: IMultilineData
  index: number
}
