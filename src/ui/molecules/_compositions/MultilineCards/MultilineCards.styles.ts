import { StyleSheet } from 'react-native'

import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import {
  IMultilineCardStylesProps,
  IStyles,
} from '../MultilineCard/MultilineCard'

export const useStyles = makeStyles<IMultilineCardStylesProps, IStyles>(
  (theme, props) => {
    return StyleSheet.create({
      container: {
        flex: 1,
        height: 'auto',
      },
      globalFontColor: {
        color: props?.color ? props.color : theme.colors.text,
      },
      multilineCardOuter: {
        flexGrow: 1,
        flexShrink: 1,
        flexBasis: '100%', // TODO: this might work only for desktop
      },
    })
  }
)
