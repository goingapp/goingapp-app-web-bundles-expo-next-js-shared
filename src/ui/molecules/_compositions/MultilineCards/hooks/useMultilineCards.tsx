import { MultilineCard } from '../../MultilineCard'
import {
  IMultilineData,
  IRenderMultilineItem,
  IUseMultilineCardsProps,
} from '../MultilineCards'
import { useStyles } from '../MultilineCards.styles'

export const useMultilineCards = ({
  isHorizontal,
  maxInRow,
  background,
  moreButton: isMoreButton,
  moreButtonText,
  color,
  disableDate: isDisableDate,
  disableDescription: isDisableDescription,
  disablePlace: isDisablePlace,
  disableTitle: isDisableTitle,
  // fixedWidthCards, // TODO on desktop
  containerWidth,
  id,
  isMobile,
  isTablet,
  isWeb,
  dateTranslations,
  resolveUrl,
  wordpressHost,
}: IUseMultilineCardsProps) => {
  const styles = useStyles({
    background,
    color,
  })
  const isContent = !(
    isDisableDate &&
    isDisablePlace &&
    isDisableTitle &&
    isDisableDescription
  )

  const renderItem = ({ item, index }: IRenderMultilineItem) => (
    <MultilineCard
      item={item}
      index={index}
      isMobile={isMobile}
      isTablet={isTablet}
      dateTranslations={dateTranslations}
      isHorizontal={isHorizontal}
      maxInRow={maxInRow}
      isMoreButton={isMoreButton}
      moreButtonText={moreButtonText}
      isContent={isContent}
      isWeb={isWeb}
      containerWidth={containerWidth}
      resolveUrl={resolveUrl}
      styles={styles}
      wordpressHost={wordpressHost}
    />
  )

  const keyExtractor = (_item: IMultilineData, index: number) =>
    `multiline-${id}-${index}`

  return {
    keyExtractor,
    renderItem,
  }
}
