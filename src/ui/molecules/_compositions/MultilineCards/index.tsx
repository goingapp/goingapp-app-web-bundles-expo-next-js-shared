import { FlatList } from 'react-native'

import { useTheme } from 'react-native-paper'

import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { useMultilineCards } from './hooks/useMultilineCards'
import { IMultilineCardsProps } from './MultilineCards'
import { useStyles } from './MultilineCards.styles'

export const MultilineCards = ({
  data,
  extra,
  background,
  gridItemProps,
  id,
  dateTranslations,
  isWeb,
  isMobile,
  isTablet,
  containerWidth,
  resolveUrl,
  wordpressHost,
}: IMultilineCardsProps) => {
  const styles = useStyles({})
  const theme = useTheme()
  const {
    color,
    disableDate,
    disableDescription,
    disablePlace,
    disableTitle,
    isHorizontal,
    maxInRow,
    fixedWidthCards,
    moreButton,
    moreButtonText,
  } = extra
  const { keyExtractor, renderItem } = useMultilineCards({
    id,
    fixedWidthCards,
    maxInRow: !isHorizontal ? maxInRow : 1,
    background,
    moreButtonText,
    color,
    disableDate,
    disableDescription,
    disablePlace,
    disableTitle,
    isHorizontal: isHorizontal && isMobile,
    moreButton,
    isMobile,
    isTablet,
    dateTranslations,
    isWeb,
    containerWidth,
    resolveUrl,
    wordpressHost,
  })

  const multiline = (
    <Grid
      rowDirection={!isMobile}
      dataSet={cssClasses.multilineHotspots}
      spacing={SPACING.REGULAR}
      flex={1}
    >
      {!isMobile ? (
        <FlatList
          key={`multiline-${id}-${maxInRow}`}
          numColumns={maxInRow}
          data={data}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          horizontal={false}
          viewabilityConfig={{ itemVisiblePercentThreshold: 75 }}
          style={styles.container}
          initialNumToRender={maxInRow}
          columnWrapperStyle={
            maxInRow > 1 && {
              marginBottom:
                data.length > maxInRow ? theme.spacing(SPACING.REGULAR) : 0,
            }
          }
        />
      ) : isHorizontal ? (
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          horizontal={true}
          viewabilityConfig={{ itemVisiblePercentThreshold: 75 }}
          style={styles.container}
          initialNumToRender={maxInRow}
        />
      ) : (
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          style={styles.container}
          initialNumToRender={4}
          viewabilityConfig={{ itemVisiblePercentThreshold: 75 }}
        />
      )}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={color}
        id={id}
        children={multiline}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.LARGE}
      nativeID={id}
      flex={1}
    >
      {multiline}
    </Grid>
  )
}
