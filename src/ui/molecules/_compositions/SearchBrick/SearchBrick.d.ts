import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ISearchBrickFilter {
  filter: string
  value: string
}

export interface ISearchBrickData {
  filters: ISearchBrickFilter[]
}

export interface ISearchBrickProps extends ISectionsComponentCommon {
  data: ISearchBrickData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}
