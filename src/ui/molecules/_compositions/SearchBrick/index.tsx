import { useTheme } from 'react-native-paper'

import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { SearchBrickItem } from '../SearchBrickItem'
import { ISearchBrickProps } from './SearchBrick'

export const SearchBrick = ({
  data,
  background,
  extra,
  gridItemProps,
  id,
  resolveUrl,
  isMobile,
}: ISearchBrickProps) => {
  const theme = useTheme()

  const searchbar = (
    <Grid>
      {!data[0] ? (
        <SearchBrickItem resolveUrl={resolveUrl} theme={theme} />
      ) : (
        <SearchBrickItem item={data[0]} resolveUrl={resolveUrl} theme={theme} />
      )}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={searchbar}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return <Grid gridItemProps={gridItemProps}>{searchbar}</Grid>
}
