import { TouchableOpacity } from 'react-native'

import { Searchbar } from 'react-native-paper'

import { IResolveUrl } from '../../../../models/Navigation'
import { ISearchBrickData } from '../SearchBrick/SearchBrick'

export const SearchBrickItem = ({
  item,
  resolveUrl,
  theme,
}: {
  item?: ISearchBrickData
  resolveUrl: IResolveUrl
  theme: ReactNativePaper.Theme
}) => {
  const filtersString =
    item &&
    item.filters.map((filter, index) => {
      index < item.filters.length - 1
        ? `${filter.filter}=${filter.value}&`
        : `${filter.filter}=${filter.value}`
    })
  const searchUrl = `/wydarzenia?${filtersString ?? filtersString}`
  const handleRedirect = () => resolveUrl(searchUrl)
  return (
    <TouchableOpacity onPress={handleRedirect}>
      {/* @ts-ignore TODO: find out why Searchbar forces optional parameters */}
      <Searchbar
        pointerEvents={'none'}
        value={''}
        style={theme.overrides.searchbar().searchbar}
        inputStyle={theme.overrides.searchbar().input}
        iconColor={theme.colors.primaryButtonText}
      />
    </TouchableOpacity>
  )
}
