import { ComponentType } from 'react'
import { StyleProp, ViewStyle } from 'react-native'

import {
  ICompositionTranslations,
  IPagesSections,
} from '../../../../models/Compositions'
import { IDateTranslations } from '../../../../models/Locale'
import { IResolveUrl } from '../../../../models/Navigation'
import { IHtmlMarkdownProps } from '../../../_reusable/HtmlMarkdown/HtmlMarkdown'

export interface IStyles {
  backgroundImage: StyleProp<ViewStyle>
  backgroundSolid: StyleProp<ViewStyle>
  desktopMaxWidth: StyleProp<ViewStyle>
  mobilePadding: StyleProp<ViewStyle>
}

export interface ISectionStylesProps {
  background: string
  containerWidth: number
}

export interface ISectionAppProps {
  isWeb: boolean
  isDesktop: boolean
  isMobile: boolean
  isTablet: boolean
  noDesktop: boolean
  noMobile: boolean
  noTablet: boolean
  height: number
  width: number
  containerWidth: number
  platform: null | string
}

export interface ISectionProps extends IPagesSections {
  resolveUrl: IResolveUrl
  dateTranslations: IDateTranslations
  compositionTranslations: ICompositionTranslations
  formIoAddress: string
  urlCloudinaryList: string
  cloudName: string
  urlApp: string
  markdownComponent: ComponentType<IHtmlMarkdownProps>
  isBgTurnedOff?: boolean
  isLegacySafari?: boolean
  wordpressHost?: string
}
