import { SPACING } from '../../../../constants/Grid'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { ISectionStylesProps, IStyles } from './Section'

export const useStyles = makeStyles<ISectionStylesProps, IStyles>(
  (theme, props) => ({
    backgroundSolid: {
      backgroundColor: props?.background,
    },
    backgroundImage: {
      width: '100%',
    },
    desktopMaxWidth: {
      paddingHorizontal: props?.containerWidth
        ? (props?.containerWidth - theme.breakpoints.lg) / 2
        : undefined,
    },
    mobilePadding: {
      paddingHorizontal: theme.spacing(SPACING.REGULAR),
    },
  })
)
