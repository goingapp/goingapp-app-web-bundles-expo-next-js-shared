import { View } from 'react-native'

import { cssClasses } from '../../../../constants/CSS'
import { BrickRenderer } from '../BrickRenderer'
import { GradientSection } from '../GradientSection'
import { WithBackgroundImage } from '../WithBackgroundImage'
import { ISectionAppProps, ISectionProps } from './Section'
import { useStyles } from './Section.styles'

export const Section = ({
  background,
  backgroundImage,
  components,
  contain,
  id,
  inRow,
  mobileBackgroundImage,
  slug,
  useGradient,
  resolveUrl,
  dateTranslations,
  formIoAddress,
  cloudName,
  urlCloudinaryList,
  urlApp,
  compositionTranslations,
  markdownComponent,
  isWeb,
  isDesktop,
  isMobile,
  isTablet,
  noDesktop,
  noMobile,
  noTablet,
  height,
  width,
  containerWidth,
  platform,
  isBgTurnedOff,
  isLegacySafari,
  wordpressHost,
}: ISectionProps & ISectionAppProps) => {
  const nativeID = `section-${slug ? slug + '-' : ''}${id}`
  const styles = useStyles({ background, containerWidth })
  const backgroundImg = isMobile
    ? mobileBackgroundImage ?? backgroundImage
    : backgroundImage

  const renderer = (
    <BrickRenderer
      components={components}
      background={background}
      inRow={inRow}
      id={id}
      resolveUrl={resolveUrl}
      dateTranslations={dateTranslations}
      formIoAddress={formIoAddress}
      cloudName={cloudName}
      urlCloudinaryList={urlCloudinaryList}
      urlApp={urlApp}
      compositionTranslations={compositionTranslations}
      markdownComponent={markdownComponent}
      isWeb={isWeb}
      isTablet={isTablet}
      isMobile={isMobile}
      isDesktop={isDesktop}
      noDesktop={noDesktop}
      noMobile={noMobile}
      noTablet={noTablet}
      height={height}
      width={width}
      platform={platform}
      containerWidth={containerWidth}
      isLegacySafari={isLegacySafari}
      wordpressHost={wordpressHost}
    />
  )

  if (backgroundImg && !isBgTurnedOff) {
    return (
      <WithBackgroundImage
        background={background}
        backgroundImg={backgroundImg}
        isDesktop={isDesktop}
        nativeID={nativeID}
        contain={contain}
        useGradient={useGradient}
        renderer={renderer}
        styles={styles}
      />
    )
  }

  if (useGradient && !backgroundImg && !isBgTurnedOff) {
    return (
      <GradientSection
        nativeID={nativeID}
        isDesktop={isDesktop}
        background={background}
        styles={styles}
        renderer={renderer}
      />
    )
  }

  return (
    <View
      style={[styles.backgroundSolid, isDesktop && styles.desktopMaxWidth]}
      // @ts-ignore: this is a new prop and is not yet supported by types for react-native-web
      dataSet={cssClasses.sectionOuter}
      nativeID={nativeID}
    >
      {renderer}
    </View>
  )
}
