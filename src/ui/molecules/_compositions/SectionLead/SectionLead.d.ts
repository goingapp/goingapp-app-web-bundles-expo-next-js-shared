import { StyleProp } from 'react-native'

import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ISectionLeadData {
  description?: string
  title: string
}

export interface ISectionLeadProps extends ISectionsComponentCommon {
  data: ISectionLeadData[]
  extra: {
    color?: string
  }
}

export interface ISectionLeadStyleProps {
  background?: string
  color?: string
}

export interface IStyles {
  lead: StyleProp
}

export type ISectionLead = ISectionLeadProps
