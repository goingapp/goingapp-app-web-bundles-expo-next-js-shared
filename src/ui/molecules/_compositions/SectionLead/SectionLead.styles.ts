import { SPACING } from '../../../../constants/Grid'
import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { ISectionLeadStyleProps, IStyles } from './SectionLead'

export const useStyles = makeStyles<ISectionLeadStyleProps, IStyles>(
  (theme, props) => ({
    lead: {
      color: props?.color
        ? props.color
        : isColorDark(props?.background)
        ? theme.colors.white
        : theme.colors.black,
      textAlign: 'center',
      paddingHorizontal: theme.spacing(SPACING.REGULAR),
    },
  })
)
