import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { SectionLeadItem } from '../SectionLeadItem'
import { ISectionLead } from './SectionLead'
import { useStyles } from './SectionLead.styles'

export const SectionLead = ({
  data,
  extra: { color },
  background,
  id,
  gridItemProps,
  isMobile,
}: ISectionLead) => {
  const styles = useStyles({ background, color })

  return (
    <Grid
      spacing={SPACING.LARGE}
      gridItemProps={gridItemProps}
      dataSet={cssClasses.sectionLead}
      nativeID={id}
    >
      {data.map((item, index) => (
        <SectionLeadItem
          item={item}
          key={`sectionLead-${id}-${index}`}
          isMobile={isMobile}
          styles={styles}
        />
      ))}
    </Grid>
  )
}
