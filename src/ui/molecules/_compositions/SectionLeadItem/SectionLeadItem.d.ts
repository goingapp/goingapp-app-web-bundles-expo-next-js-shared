import { StyleProp } from 'react-native'

import { IGridItem } from '../../../atoms/Grid/Grid'
import { ISectionLeadData } from '../SectionLead/SectionLead'

export interface ISectionLeadItemProps extends IGridItem {
  styles: StyleProp
  item: ISectionLeadData
  isMobile: boolean
  isSubheader?: boolean
}
