import { cssClasses } from '../../../../constants/CSS'
import { TypographyVariant } from '../../../../constants/Variants'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { ISectionLeadItemProps } from './SectionLeadItem'

export const SectionLeadItem = ({
  item,
  styles,
  isMobile,
  isSubheader,
  gridItemProps,
}: ISectionLeadItemProps) => {
  const { title, description } = item

  return (
    <Grid dataSet={cssClasses.sectionLead} gridItemProps={gridItemProps}>
      <Typography
        style={styles.lead}
        variant={
          isSubheader
            ? isMobile
              ? TypographyVariant.h4mobile
              : TypographyVariant.h4
            : isMobile
            ? TypographyVariant.h2mobile
            : TypographyVariant.h2
        }
        children={title}
      />
      {description && (
        <Typography
          style={styles.lead}
          variant={TypographyVariant.body2}
          children={description}
        />
      )}
    </Grid>
  )
}
