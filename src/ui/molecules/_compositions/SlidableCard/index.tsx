import { aspectRatio } from '../../../../constants/AspectRatio'
import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { Ui } from '../../../../constants/Ui'
import { TypographyVariant } from '../../../../constants/Variants'
import { IDate } from '../../../../models/DateTime'
import { DateTime } from '../../../../services/Date/DateTime'
import { createCompositionsCardUrl } from '../../../../utils/Helpers/createCompositionsCardUrl'
import { CardDescription } from '../../../_reusable/CardDescription'
import { Link } from '../../../_reusable/Link'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { Card } from '../../Card'
import { LinkButton } from '../_reusable/LinkButton'
import { ISlidableCard } from '../SlidableHotspots/SlidableHotspots'

export const SlidableCard = ({
  slug,
  badge,
  place,
  subtitle,
  thumbUrl,
  title,
  date,
  dateDesc,
  description,
  link,
  place_slug: placeSlug,
  index,
  background,
  verticalImage,
  isControls,
  isDisableTitle,
  isDisableDescription,
  isAnyCardWithDescription,
  isAnyCardWithAdditionalInfo,
  isMoreButton,
  moreButtonText,
  color,
  isMobile,
  isWeb,
  containerWidth: _containerWidth,
  dateTranslations,
  maxInRow,
  resolveUrl,
  styles,
  theme,
  isDarkBg,
  dataLength,
}: ISlidableCard) => {
  const containerWidth = isControls
    ? _containerWidth - Ui.arrowControlsWidth
    : _containerWidth
  const preparedLink = createCompositionsCardUrl(link, slug)
  const isContent = !(isDisableDescription && isDisableTitle)
  const dateVal = (date?: IDate, dateDesc?: string) =>
    dateDesc
      ? dateDesc
      : date
      ? DateTime.getFriendlyDate({
          isoDate: DateTime.getIsoDateString(date),
          dateTranslate: dateTranslations,
        })
      : undefined
  const fallbackColor = isDarkBg ? theme.colors.white : theme.colors.black

  return (
    <Card
      isSliderCard={isMobile && !!dataLength && dataLength > 1}
      coverAspectRatio={
        verticalImage ? aspectRatio.slidableCardVertical : aspectRatio.default
      }
      cardsInRow={isMobile ? 1 : maxInRow || 4}
      tags={badge && badge.name.length > 0 ? [badge.name] : undefined}
      tagColor={badge?.type}
      cover={thumbUrl}
      gridItemProps={{
        rowDirection: true,
        spacing: SPACING.REGULAR,
        isFirst: index === 0,
      }}
      isWeb={isWeb}
      containerWidth={containerWidth}
      isMobile={isMobile}
      alt={title}
    >
      {isContent && (
        <Grid
          justifyContent={'space-between'}
          padding={SPACING.REGULAR}
          spacing={SPACING.REGULAR}
          dataSet={cssClasses.slidableHotspotsCardOuter}
          flex={1}
        >
          {!isDisableTitle && (
            <Typography
              variant={TypographyVariant.h4mobile}
              children={title}
              numberOfLines={2}
              style={[styles.fontColor, styles.title]}
            />
          )}
          {!verticalImage && isAnyCardWithAdditionalInfo && (
            <Grid dataSet={cssClasses.slidableHotspotsCardInner} spacing={0}>
              <Typography
                variant={TypographyVariant.subtitle2}
                children={subtitle}
                style={styles.primaryColor}
              />
              <Typography
                variant={TypographyVariant.subtitle1}
                children={dateVal(date, dateDesc)}
                numberOfLines={1}
                style={styles.fontColor}
              />
              {placeSlug ? (
                <Link url={placeSlug} resolveUrl={resolveUrl}>
                  <Typography
                    variant={TypographyVariant.subtitle2}
                    children={place}
                    numberOfLines={1}
                    style={styles.fontColor}
                  />
                </Link>
              ) : (
                <Typography
                  variant={TypographyVariant.subtitle2}
                  children={place}
                  numberOfLines={1}
                  style={styles.fontColor}
                />
              )}
            </Grid>
          )}
          {isAnyCardWithDescription && !isDisableDescription && description && (
            <CardDescription
              background={background}
              numOfLines={3}
              text={description}
            />
          )}
          {isMoreButton && moreButtonText && preparedLink && (
            <LinkButton
              url={preparedLink}
              label={moreButtonText}
              textColor={color ?? fallbackColor}
              borderColor={color ?? fallbackColor}
              isOutlined={false}
              isFullWidth={true}
              resolveUrl={resolveUrl}
            />
          )}
        </Grid>
      )}
    </Card>
  )
}
