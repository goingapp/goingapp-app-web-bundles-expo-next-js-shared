import { StyleProp } from 'react-native'

import {
  ICardData,
  ICardExtra,
  ISectionsComponentCommon,
} from '../../../../models/Compositions'
import { IDateTranslations } from '../../../../models/Locale'
import { IResolveUrl } from '../../../../models/Navigation'

export type ISlidableHotspotData = ICardData
export interface ISlidableHotspotsExtra extends ICardExtra {
  verticalImage: boolean
}

export interface IUseSlidableHotspotsProps extends ISlidableHotspotsExtra {
  background?: string
  verticalImage: boolean
  isControls?: boolean
  resolveUrl: IResolveUrl
  isAnyCardWithAdditionalInfo: boolean
  isAnyCardWithDescription: boolean
  sliderId: string
  isMobile: boolean
  dateTranslations: IDateTranslations
  isWeb: boolean
  containerWidth: number
  dataLength?: number
}

export interface ISlidableHotspotsProps extends ISectionsComponentCommon {
  data: ISlidableHotspotData[]
  extra: ISlidableHotspotsExtra
}

export interface ISlidableCard extends ISlidableHotspotData {
  background?: string
  verticalImage: boolean
  isDisableTitle?: boolean
  isAnyCardWithAdditionalInfo: boolean
  isAnyCardWithDescription: boolean
  isDisableDescription?: boolean
  isMoreButton?: boolean
  moreButtonText?: string
  color?: string
  index: number
  isWeb: boolean
  containerWidth: number
  isMobile: boolean
  isControls?: boolean
  dateTranslations: IDateTranslations
  maxInRow?: number
  resolveUrl: IResolveUrl
  styles: StyleProp
  theme: ReactNativePaper.Theme
  isDarkBg: boolean
  dataLength?: number
}

export interface ISlidableCardStylesProps {
  background?: string
}

export interface IStyles {
  fontColor: StyleProp
  primaryColor: StyleProp
  title: StyleProp
}
