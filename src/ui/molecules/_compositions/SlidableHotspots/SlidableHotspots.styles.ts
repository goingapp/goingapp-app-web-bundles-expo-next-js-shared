import { StyleSheet } from 'react-native'

import { TypographyVariant } from '../../../../constants/Variants'
import { makeStyles } from '../../../../utils/Helpers/makeStyles'
import { ISlidableCardStylesProps, IStyles } from './SlidableHotspots'

export const useStyles = makeStyles<ISlidableCardStylesProps, IStyles>(
  (theme, props) =>
    StyleSheet.create({
      fontColor: {
        color: theme.colors.text,
      },
      primaryColor: {
        color: theme.colors.primary,
      },
      title: {
        minHeight:
          2 * (theme?.typography[TypographyVariant.h4mobile].lineHeight || 0),
      },
    })
)
