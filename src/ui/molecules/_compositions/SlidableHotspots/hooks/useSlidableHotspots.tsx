import { useTheme } from 'react-native-paper'

import { createCompositionsCardUrl } from '../../../../../utils/Helpers/createCompositionsCardUrl'
import { isColorDark } from '../../../../../utils/Helpers/isColorDark'
import { Link } from '../../../../_reusable/Link'
import { SlidableCard } from '../../SlidableCard'
import {
  ISlidableHotspotData,
  IUseSlidableHotspotsProps,
} from '../SlidableHotspots'
import { useStyles } from '../SlidableHotspots.styles'

export const useSlidableHotspots = ({
  background,
  disableTitle: isDisableTitle,
  disableDescription: isDisableDescription,
  sliderId,
  verticalImage,
  isAnyCardWithAdditionalInfo,
  isAnyCardWithDescription,
  moreButton: isMoreButton,
  moreButtonText,
  maxInRow,
  color,
  isControls,
  isMobile,
  resolveUrl,
  dateTranslations,
  isWeb,
  containerWidth,
  dataLength,
}: IUseSlidableHotspotsProps) => {
  const theme = useTheme()
  const styles = useStyles({ background })

  const renderItem = ({
    item,
    index,
  }: {
    item: ISlidableHotspotData
    index: number
  }) => {
    const {
      link,
      slug,
      badge,
      place_slug,
      place,
      subtitle,
      thumbUrl,
      title,
      description,
      date,
      dateDesc,
      tags,
    } = item

    const url = createCompositionsCardUrl(link, slug)

    if (!isMoreButton && url) {
      return (
        <Link url={url} resolveUrl={resolveUrl}>
          <SlidableCard
            badge={badge}
            place={place}
            place_slug={place_slug}
            subtitle={subtitle}
            thumbUrl={thumbUrl}
            title={title}
            index={index}
            description={description}
            date={date}
            dateDesc={dateDesc}
            tags={tags}
            background={background}
            isDisableDescription={isDisableDescription}
            isDisableTitle={isDisableTitle}
            verticalImage={verticalImage}
            isAnyCardWithDescription={isAnyCardWithDescription}
            isAnyCardWithAdditionalInfo={isAnyCardWithAdditionalInfo}
            isMoreButton={isMoreButton}
            moreButtonText={moreButtonText}
            color={color}
            isControls={isControls}
            dateTranslations={dateTranslations}
            isMobile={isMobile}
            maxInRow={maxInRow}
            resolveUrl={resolveUrl}
            isWeb={isWeb}
            containerWidth={containerWidth}
            theme={theme}
            styles={styles}
            isDarkBg={isColorDark(background)}
            dataLength={dataLength}
          />
        </Link>
      )
    }

    return (
      <SlidableCard
        badge={badge}
        place={place}
        place_slug={place_slug}
        subtitle={subtitle}
        thumbUrl={thumbUrl}
        title={title}
        index={index}
        description={description}
        date={date}
        dateDesc={dateDesc}
        tags={tags}
        link={url}
        background={background}
        isDisableDescription={isDisableDescription}
        isDisableTitle={isDisableTitle}
        verticalImage={verticalImage}
        isAnyCardWithDescription={isAnyCardWithDescription}
        isAnyCardWithAdditionalInfo={isAnyCardWithAdditionalInfo}
        isMoreButton={isMoreButton}
        moreButtonText={moreButtonText}
        color={color}
        isControls={isControls}
        dateTranslations={dateTranslations}
        isMobile={isMobile}
        maxInRow={maxInRow}
        resolveUrl={resolveUrl}
        isWeb={isWeb}
        containerWidth={containerWidth}
        theme={theme}
        styles={styles}
        isDarkBg={isColorDark(background)}
        dataLength={dataLength}
      />
    )
  }

  const keyExtractor = (_item: ISlidableHotspotData, index: number) =>
    `${sliderId}-${index}`

  return {
    keyExtractor,
    renderItem,
  }
}
