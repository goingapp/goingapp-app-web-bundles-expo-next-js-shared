import { useRef, useState } from 'react'
import { FlatList } from 'react-native'

import { cssClasses } from '../../../../constants/CSS'
import { SPACING } from '../../../../constants/Grid'
import { chunkBigPayload } from '../../../../utils/Helpers/chunkBigPayload'
import { SliderControls } from '../../../_reusable/SliderControls'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { useSlidableHotspots } from './hooks/useSlidableHotspots'
import {
  ISlidableHotspotData,
  ISlidableHotspotsProps,
} from './SlidableHotspots'

export const SlidableHotspots = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  isMobile,
  resolveUrl,
  dateTranslations,
  isWeb,
  containerWidth,
  isLegacySafari,
}: ISlidableHotspotsProps) => {
  const {
    disableTitle,
    disablePlace,
    disableDate,
    disableDescription,
    verticalImage,
    moreButton,
    moreButtonText,
    color,
    maxInRow,
  } = extra
  const cardsInRow = isMobile ? 1 : maxInRow || 4
  const dataChunked = chunkBigPayload(data, cardsInRow)
  const isAnyCardWithAdditionalInfo = data.some(
    (item) => !!item.place || !!item.date || !!item.dateDesc
  )
  const isAnyCardWithDescription = data.some((item) => !!item.description)
  const isControls = !isMobile && data.length > cardsInRow // TODO: include mobile controls variant
  const ref = useRef<FlatList>(null)
  const [dataIndex, setDataIndex] = useState(0)
  const [_data, setData] = useState<ISlidableHotspotData[]>(dataChunked[0])

  const getMoreData = () => {
    setDataIndex(dataIndex + 1)
    if (dataChunked[dataIndex + 1] && dataChunked[dataIndex + 1].length) {
      setData([..._data, ...dataChunked[dataIndex + 1]])
    }
  }

  const { keyExtractor, renderItem } = useSlidableHotspots({
    background,
    sliderId: id,
    disablePlace,
    disableDate,
    disableDescription,
    disableTitle,
    verticalImage,
    isAnyCardWithAdditionalInfo,
    isAnyCardWithDescription,
    moreButton,
    moreButtonText,
    color,
    isControls,
    resolveUrl,
    maxInRow: cardsInRow,
    dateTranslations,
    isMobile,
    isWeb,
    containerWidth,
    dataLength: _data.length,
  })

  const list = (
    <FlatList
      data={_data}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      horizontal={true}
      viewabilityConfig={{ itemVisiblePercentThreshold: 100 }}
      maxToRenderPerBatch={cardsInRow * 2}
      initialNumToRender={_data.length}
      onEndReached={getMoreData}
      ref={ref}
      centerContent={true}
      scrollEnabled={!isControls}
    />
  )

  const slider = (
    <Grid dataSet={cssClasses.slidableHotspotsList} flex={1}>
      {isControls ? (
        <SliderControls
          dataLength={_data.length}
          sliderRef={ref}
          elementsInRow={cardsInRow}
          elementSpacing={SPACING.REGULAR}
          isMobile={isMobile}
          isWeb={isWeb}
          isLegacySafari={isLegacySafari}
        >
          {list}
        </SliderControls>
      ) : (
        list
      )}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        id={id}
        children={slider}
        gridItemProps={gridItemProps}
        color={color}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.LARGE}
      dataSet={cssClasses.slidableHotspots}
      nativeID={id}
    >
      {slider}
    </Grid>
  )
}
