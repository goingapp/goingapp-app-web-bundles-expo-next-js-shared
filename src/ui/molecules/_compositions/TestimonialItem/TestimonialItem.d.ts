import { StyleProp } from 'react-native'

import { IResolveUrl } from '../../../../models/Navigation'
import { ITestimonialsData } from '../Testimonials/Testimonials'

export interface ITestimonialItemProps {
  item: ITestimonialsData
  containerWidth: number
  resolveUrl: IResolveUrl
  styles: StyleProp
  theme: ReactNativePaper.Theme
  isMobile: boolean
}
