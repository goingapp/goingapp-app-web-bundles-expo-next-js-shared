import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { Link } from '../../../_reusable/Link'
import { Grid } from '../../../atoms/Grid'
import { ResponsiveImage } from '../../../atoms/ResponsiveImage'
import { Typography } from '../../../atoms/Typography'
import { ITestimonialItemProps } from './TestimonialItem'

export const TestimonialItem = ({
  item,
  containerWidth,
  resolveUrl,
  styles,
  theme,
  isMobile,
}: ITestimonialItemProps) => {
  const { author, authorsJob, description, link, src } = item

  const image = (
    <ResponsiveImage
      containerStyle={styles.pill}
      borderRadius={100}
      aspectRatio={[1, 1]}
      imageUrl={src}
      alt={author}
    />
  )

  return (
    <Grid
      spacing={SPACING.REGULAR}
      padding={SPACING.LARGE}
      withBackground={true}
      containerStyle={{ width: containerWidth - theme.spacing(SPACING.LARGE) }}
      rowDirection={!isMobile}
    >
      <Grid padding={SPACING.REGULAR}>
        {link ? (
          <Link url={link} resolveUrl={resolveUrl}>
            {image}
          </Link>
        ) : (
          image
        )}
      </Grid>
      <Grid flex={1}>
        <Grid>
          <Typography
            variant={TypographyVariant.body1}
            children={description}
            style={styles.centeredText}
            numberOfLines={4}
          />
        </Grid>
        <Grid>
          <Typography
            variant={TypographyVariant.body1}
            children={author}
            style={styles.centeredText}
            numberOfLines={1}
          />
          <Typography
            variant={TypographyVariant.subtitle1}
            children={authorsJob}
            style={styles.centeredText}
            numberOfLines={1}
          />
        </Grid>
      </Grid>
    </Grid>
  )
}
