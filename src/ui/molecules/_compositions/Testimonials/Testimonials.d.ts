import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ITestimonialsData {
  author: string
  authorsJob: string
  description: string
  link?: string
  src: string
}

export interface ITestimonialsProps extends ISectionsComponentCommon {
  data: ITestimonialsData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}
