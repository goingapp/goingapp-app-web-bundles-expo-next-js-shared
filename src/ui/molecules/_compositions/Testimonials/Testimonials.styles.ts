import { StyleSheet } from 'react-native'

export const useStyles = (isMobile?: boolean) =>
  StyleSheet.create({
    centeredText: {
      textAlign: !isMobile ? 'left' : 'center',
    },
    pill: { width: 200, height: 200, alignSelf: 'center' },
  })
