import { useRef } from 'react'
import { FlatList } from 'react-native'

import { useTheme } from 'react-native-paper'

import { IResolveUrl } from '../../../../../models/Navigation'
import { TestimonialItem } from '../../TestimonialItem'
import { ITestimonialsData } from '../Testimonials'
import { useStyles } from '../Testimonials.styles'

export const useTestimonials = ({
  id,
  containerWidth,
  resolveUrl,
  isMobile,
}: {
  id: string
  containerWidth: number
  resolveUrl: IResolveUrl
  isMobile: boolean
}) => {
  const theme = useTheme()
  const styles = useStyles(isMobile)
  const ref = useRef<FlatList>(null)

  const renderItem = ({ item }: { item: ITestimonialsData }) => (
    <TestimonialItem
      item={item}
      containerWidth={containerWidth}
      resolveUrl={resolveUrl}
      theme={theme}
      styles={styles}
      isMobile={isMobile}
    />
  )

  const keyExtractor = (_item: ITestimonialsData, index: number) =>
    `${id}-${index}`

  return {
    keyExtractor,
    renderItem,
    ref,
  }
}
