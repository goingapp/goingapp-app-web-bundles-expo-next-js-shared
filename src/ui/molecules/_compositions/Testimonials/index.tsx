import { FlatList } from 'react-native'

import { SPACING } from '../../../../constants/Grid'
import { checkIsLegacySafari } from '../../../../utils/Helpers/checkIsLegacySafari'
import { SliderControls } from '../../../_reusable/SliderControls'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { useTestimonials } from './hooks/useTestimonials'
import { ITestimonialsProps } from './Testimonials'
import { useStyles } from './Testimonials.styles'

export const Testimonials = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  isMobile,
  containerWidth,
  resolveUrl,
  isWeb,
}: ITestimonialsProps) => {
  const styles = useStyles()

  const { keyExtractor, renderItem, ref } = useTestimonials({
    id,
    containerWidth,
    resolveUrl,
    isMobile,
  })

  const { isLegacySafari } = checkIsLegacySafari()

  const list = (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      horizontal={true}
      ref={ref}
      scrollEnabled={false}
    />
  )

  const testimonials = (
    <Grid>
      {data.length > 1 ? (
        <SliderControls
          dataLength={data.length}
          isMobile={true}
          sliderRef={ref}
          elementSpacing={SPACING.REGULAR}
          elementsInRow={1}
          children={list}
          isWeb={isWeb}
          isLegacySafari={isLegacySafari}
        />
      ) : (
        list
      )}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={testimonials}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return <Grid gridItemProps={gridItemProps}>{testimonials}</Grid>
}
