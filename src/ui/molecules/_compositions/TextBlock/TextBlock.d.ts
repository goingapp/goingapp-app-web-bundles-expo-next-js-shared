import {
  IBasicComponentData,
  ISectionComponentExtra,
  ISectionsComponentCommon,
} from '../../../../models/Compositions'

export interface ITextBlockData extends IBasicComponentData {
  formattedDescription: string
  cta: string
  reversed: boolean
  src?: string
}

export interface ITextBlockProps extends ISectionsComponentCommon {
  data: ITextBlockData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}
