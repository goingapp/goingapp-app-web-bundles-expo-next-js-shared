import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'
import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { TextBlockItem } from '../TextBlockItem'
import { ITextBlockProps } from './TextBlock'

export const TextBlock = ({
  data,
  extra,
  gridItemProps,
  background,
  id,
  resolveUrl,
  isMobile,
  markdownComponent,
}: ITextBlockProps) => {
  const theme = useTheme()

  const textBlockItems = (
    <Grid spacing={SPACING.LARGE}>
      {data.map((item, index) => (
        <TextBlockItem
          item={item}
          key={`textBlock-${id}-${index}`}
          resolveUrl={resolveUrl}
          color={extra.color}
          theme={theme}
          isBgDark={isColorDark(background)}
          markdownComponent={markdownComponent}
          isMobile={isMobile}
        />
      ))}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={textBlockItems}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid gridItemProps={gridItemProps} spacing={SPACING.LARGE}>
      {textBlockItems}
    </Grid>
  )
}
