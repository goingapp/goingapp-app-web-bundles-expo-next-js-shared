import { ComponentType } from 'react'

import { IHtmlMarkdownProps } from 'goingapp-app-web-bundles-expo-next-js-shared'

import { IResolveUrl } from '../../../../models/Navigation'
import { ITextBlockData } from '../TextBlock/TextBlock'

export interface ITextBlockItemProps {
  item: ITextBlockData
  color?: string
  resolveUrl: IResolveUrl
  isBgDark: boolean
  isMobile: boolean
  theme: ReactNativePaper.Theme
  markdownComponent: ComponentType<IHtmlMarkdownProps>
}
