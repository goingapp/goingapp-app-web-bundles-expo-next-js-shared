import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { Grid } from '../../../atoms/Grid'
import { ResponsiveImage } from '../../../atoms/ResponsiveImage'
import { Typography } from '../../../atoms/Typography'
import { LinkButton } from '../_reusable/LinkButton'
import { ITextBlockItemProps } from './TextBlockItem'

export const TextBlockItem = ({
  item,
  color,
  resolveUrl,
  theme,
  isBgDark,
  markdownComponent: HtmlMarkdown,
  isMobile,
}: ITextBlockItemProps) => {
  const {
    cta,
    formattedDescription,
    title,
    link,
    src,
    reversed: isReversed,
  } = item
  const fontColor = color || isBgDark ? theme.colors.white : theme.colors.black

  return (
    <Grid spacing={SPACING.REGULAR}>
      {!isReversed && src && <ResponsiveImage imageUrl={src} alt={title} />}
      <Grid spacing={SPACING.REGULAR} padding={SPACING.REGULAR}>
        <Typography
          variant={isMobile ? TypographyVariant.h2mobile : TypographyVariant.h2}
          children={title}
        />
        <HtmlMarkdown
          fontColor={fontColor}
          markdownString={formattedDescription}
        />
        {link && (
          <LinkButton
            url={link}
            label={cta}
            resolveUrl={resolveUrl}
            isOutlined={true}
          />
        )}
      </Grid>
      {isReversed && src && <ResponsiveImage imageUrl={src} />}
    </Grid>
  )
}
