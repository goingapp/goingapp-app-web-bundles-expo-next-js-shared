import { ISectionComponentExtra } from '../../../../models/Compositions'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ITimelineData {
  markerData: string
  markerLink?: string
  markerTitle: string
}

export interface ITimelineProps extends ISectionsComponentCommon {
  data: ITimelineData[]
  extra: ISectionComponentExtra
  background: string
  id: string
}
