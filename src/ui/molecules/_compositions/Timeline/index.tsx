import { SPACING } from '../../../../constants/Grid'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { TimelineItem } from '../TimelineItem'
import { ITimelineProps } from './Timeline'

export const Timeline = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  isMobile,
  resolveUrl,
}: ITimelineProps) => {
  const timeline = (
    <Grid>
      {data.map((item, index) => (
        <TimelineItem
          item={item}
          index={index}
          key={`timeline-${id}-${index}`}
          resolveUrl={resolveUrl}
          dataLength={data.length}
        />
      ))}
    </Grid>
  )

  if (extra.sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={extra.sectionTitle}
        description={extra.sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={timeline}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return (
    <Grid gridItemProps={gridItemProps} spacing={SPACING.LARGE}>
      {timeline}
    </Grid>
  )
}
