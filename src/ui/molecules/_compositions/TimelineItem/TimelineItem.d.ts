import { IResolveUrl } from '../../../../models/Navigation'
import { ITimelineData } from '../Timeline/Timeline'

export interface ITimelineItemProps {
  item: ITimelineData
  index: number
  dataLength: number
  background?: string
  color?: string
  resolveUrl: IResolveUrl
}

export interface ITimelineItemStyleProps {
  theme: ReactNativePaper.Theme
  index: number
  dataLength: number
  background?: string
  color?: string
}
