import { StyleSheet } from 'react-native'

import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { ITimelineItemStyleProps } from './TimelineItem'

export const useStyles = ({
  theme,
  index,
  dataLength,
  background,
  color,
}: ITimelineItemStyleProps) => {
  const autoColor = color
    ? color
    : isColorDark(background)
    ? theme.colors.white
    : theme.colors.black

  return StyleSheet.create({
    bullet: {
      backgroundColor: theme.colors.primary,
      borderStyle: 'solid',
      borderColor: autoColor,
      borderWidth: 2,
      borderRadius: 10,
      height: 8,
      width: 8,
      left: 2.5,
    },
    lineContainer: {
      marginRight: theme.spacing(6),
      overflow: 'visible',
    },
    line: {
      flex: 1,
      width: 3,
    },
    lineUpper: {
      backgroundColor: index === 0 ? 'transparent' : autoColor,
    },
    lineLower: {
      backgroundColor: index === dataLength - 1 ? 'transparent' : autoColor,
    },
    typography: {
      color: autoColor,
    },
  })
}
