import { View } from 'react-native'

import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { Link } from '../../../_reusable/Link'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { ITimelineItemProps } from './TimelineItem'
import { useStyles } from './TimelineItem.styles'

export const TimelineItem = ({
  item,
  index,
  background,
  color,
  dataLength,
  resolveUrl,
}: ITimelineItemProps) => {
  const { markerData, markerLink, markerTitle } = item
  const theme = useTheme()
  const styles = useStyles({
    theme,
    index,
    dataLength,
    background,
    color,
  })

  const itemLayout = (
    <Grid rowDirection={true}>
      <Grid
        flex={1}
        alignItems={'flex-end'}
        containerStyle={styles.lineContainer}
      >
        <View style={[styles.line, styles.lineUpper]} />
        <View style={styles.bullet} />
        <View style={[styles.line, styles.lineLower]} />
      </Grid>
      <Grid padding={SPACING.SMALL} flex={1}>
        <Typography
          variant={TypographyVariant.subtitle2}
          children={markerTitle}
          style={styles.typography}
        />
        <Typography
          variant={TypographyVariant.helperText}
          children={markerData}
          style={styles.typography}
        />
      </Grid>
    </Grid>
  )

  return markerLink ? (
    <Link url={markerLink} resolveUrl={resolveUrl}>
      {itemLayout}
    </Link>
  ) : (
    itemLayout
  )
}
