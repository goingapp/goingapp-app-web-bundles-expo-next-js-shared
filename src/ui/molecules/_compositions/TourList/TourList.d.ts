import { ISectionComponentExtra } from '../../../../../models/Compositions'
import { IDate } from '../../../../../models/Rundate'
import { ISectionsComponentCommon } from '../../../../models/Compositions'

export interface ITourListProps extends ISectionsComponentCommon {
  data: ITourListData[]
  extra: ISectionComponentExtra
}

export interface ITourListData {
  title: string
  link?: string
  startDate?: IDate
  endDate?: IDate
}
