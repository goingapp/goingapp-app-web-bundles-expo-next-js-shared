import { StyleSheet } from 'react-native'

import { SPACING } from '../../../../constants/Grid'

export const useStyles = (theme: ReactNativePaper.Theme, color?: string) => {
  return StyleSheet.create({
    container: {
      paddingVertical: theme.spacing(SPACING.MEDIUM),
    },
    containerMobile: {
      paddingVertical: theme.spacing(SPACING.MEDIUM),
    },
    divider: {
      backgroundColor: color,
    },
    fontColor: {
      color: color,
    },
    stretch: {
      flex: 1,
    },
  })
}
