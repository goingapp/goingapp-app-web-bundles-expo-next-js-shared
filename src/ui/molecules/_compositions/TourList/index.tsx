import { useTheme } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'
import { isColorDark } from '../../../../utils/Helpers/isColorDark'
import { Grid } from '../../../atoms/Grid'
import { ComponentWithSectionLead } from '../_reusable/ComponentWithSectionLead'
import { TourListItem } from '../TourListItem'
import { ITourListProps } from './TourList'
import { useStyles } from './TourList.styles'

export const TourList = ({
  data,
  extra,
  background,
  id,
  gridItemProps,
  isMobile,
  resolveUrl,
}: ITourListProps) => {
  const theme = useTheme()
  const isBgDark = isColorDark(background)
  const { color, sectionTitle, sectionDescription } = extra
  const fontColor = color
    ? color
    : isBgDark
    ? theme.colors.white
    : theme.colors.black
  const styles = useStyles(theme, fontColor)

  const tourList = (
    <Grid
      spacing={isMobile ? SPACING.SMALL : SPACING.MEDIUM}
      containerStyle={isMobile ? styles.containerMobile : styles.container}
    >
      {data.map((item, index) => (
        <TourListItem
          item={item}
          index={index}
          styles={styles}
          dataLength={data.length}
          key={`tour-list-${id}-${index}-test`}
          resolveUrl={resolveUrl}
          isMobile={isMobile}
        />
      ))}
    </Grid>
  )

  if (sectionTitle) {
    return (
      <ComponentWithSectionLead
        title={sectionTitle}
        description={sectionDescription}
        background={background}
        color={extra.color}
        id={id}
        children={tourList}
        gridItemProps={gridItemProps}
        isMobile={isMobile}
      />
    )
  }

  return <Grid spacing={SPACING.LARGE}>{tourList}</Grid>
}
