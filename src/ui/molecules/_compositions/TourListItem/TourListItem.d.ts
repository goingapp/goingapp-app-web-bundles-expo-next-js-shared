import { StyleProp } from 'react-native'

import { IResolveUrl } from '../../../../models/Navigation'
import { IGridItem } from '../../../atoms/Grid/Grid'
import { ITourListData } from '../TourList/TourList'

export interface ITourListItemProps extends IGridItem {
  item: ITourListData
  index: number
  dataLength: number
  styles: StyleProp
  isMobile: boolean
  resolveUrl: IResolveUrl
}

export interface IStyles {
  divider: StyleProp
  fontColor: StyleProp
  stretch: StyleProp
}
