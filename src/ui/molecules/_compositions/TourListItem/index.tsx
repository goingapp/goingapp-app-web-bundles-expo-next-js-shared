import { Divider, IconButton, useTheme } from 'react-native-paper'

import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { DateTime } from '../../../../services/Date/DateTime'
import { Grid } from '../../../atoms/Grid'
import { Typography } from '../../../atoms/Typography'
import { ITourListItemProps } from './TourListItem'

export const TourListItem = ({
  item,
  index,
  dataLength,
  styles,
  gridItemProps,
  isMobile,
  resolveUrl,
}: ITourListItemProps) => {
  const { startDate, endDate, link, title } = item
  const theme = useTheme()

  const friendlyStartDate =
    startDate &&
    DateTime.getFriendlyDate({
      isoDate: DateTime.getIsoDateString(startDate),
    })
  const friendlyEndDate =
    endDate &&
    DateTime.getFriendlyDate({
      isoDate: DateTime.getIsoDateString(endDate),
    })

  const tourDate =
    friendlyStartDate && friendlyEndDate
      ? `${friendlyStartDate} - ${friendlyEndDate}`
      : 'TBA'

  const handleTourLink = (url?: string) => () =>
    url ? resolveUrl(url) : () => {}

  const button = (
    <IconButton
      icon={'arrow-right'}
      onPress={handleTourLink(link)}
      touchSoundDisabled={true}
      color={theme.colors.primary}
      disabled={!link}
    />
  )

  return isMobile ? (
    <Grid spacing={SPACING.SMALL} gridItemProps={gridItemProps}>
      <Grid rowDirection={true} spacing={SPACING.SMALL} alignItems={'center'}>
        <Grid
          padding={SPACING.REGULAR}
          spacing={SPACING.SMALL}
          containerStyle={styles.stretch}
        >
          <Typography
            variant={TypographyVariant.h5}
            children={title}
            style={styles.fontColor}
          />
          <Typography
            variant={TypographyVariant.body1}
            children={tourDate}
            style={[styles.stretch, styles.fontColor]}
          />
        </Grid>
        {button}
      </Grid>
      {index < dataLength - 1 && (
        <Grid>
          <Divider style={styles.divider} />
        </Grid>
      )}
    </Grid>
  ) : (
    <Grid spacing={SPACING.MEDIUM} gridItemProps={gridItemProps}>
      <Grid spacing={SPACING.MEDIUM} padding={SPACING.MEDIUM}>
        <Grid rowDirection={true} spacing={SPACING.HUGE} alignItems={'center'}>
          <Typography
            variant={TypographyVariant.h4}
            children={title}
            style={styles.stretch}
          />
          <Typography
            variant={TypographyVariant.body1}
            children={tourDate}
            style={styles.stretch}
          />
          {button}
        </Grid>
      </Grid>
      {index < dataLength - 1 && (
        <Grid>
          <Divider style={styles.divider} />
        </Grid>
      )}
    </Grid>
  )
}
