import { SPACING } from '../../../../constants/Grid'
import { TypographyVariant } from '../../../../constants/Variants'
import { getFriendlyDate } from '../../../../utils/Helpers/getFriendlyDate'
import { Link } from '../../../_reusable/Link'
import { Grid } from '../../../atoms/Grid'
import { ResponsiveImage } from '../../../atoms/ResponsiveImage'
import { Typography } from '../../../atoms/Typography'
import { IBigAndVerticalListItem } from '../BigAndVerticalList/BigAndVerticalList'

export const VerticalListItem = ({
  item,
  gridItemProps: verticalItemGridItemProps,
  resolveUrl,
  dateTranslations,
}: IBigAndVerticalListItem) => {
  if (!item) {
    return null
  }
  const { thumbUrl, title, link, date, dateDesc } = item

  if (!link) {
    return null
  }

  return (
    <Link
      gridItemProps={verticalItemGridItemProps}
      url={link}
      resolveUrl={resolveUrl}
    >
      <Grid rowDirection={true} withBackground={true} flex={1}>
        <ResponsiveImage flex={10} imageUrl={thumbUrl} />
        <Grid flex={23} padding={SPACING.REGULAR} justifyContent={'center'}>
          <Typography
            variant={TypographyVariant.caption}
            children={title}
            numberOfLines={2}
          />
          {(date || dateDesc) && (
            <Typography
              variant={TypographyVariant.subtitle2}
              children={getFriendlyDate(dateTranslations, date, dateDesc)}
              numberOfLines={1}
            />
          )}
        </Grid>
      </Grid>
    </Link>
  )
}
