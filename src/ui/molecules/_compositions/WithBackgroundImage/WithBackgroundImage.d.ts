import { IStyles } from '../Section/Section'

export interface IWithBackgroundImageProps {
  backgroundImg: string
  background: string
  nativeID: string
  contain: boolean
  useGradient?: boolean
  isDesktop: boolean
  styles: IStyles
  renderer: JSX.Element
}
