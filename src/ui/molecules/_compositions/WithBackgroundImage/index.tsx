import { ImageBackground, View } from 'react-native'

import { GradientSection } from '../GradientSection'
import { IWithBackgroundImageProps } from './WithBackgroundImage'

export const WithBackgroundImage = ({
  backgroundImg,
  background,
  nativeID,
  contain,
  useGradient,
  isDesktop,
  styles,
  renderer,
}: IWithBackgroundImageProps) => {
  return (
    <View nativeID={nativeID}>
      <ImageBackground
        source={{ uri: backgroundImg }}
        resizeMode={contain ? 'contain' : 'cover'}
        style={[
          styles.backgroundImage,
          !useGradient && isDesktop && styles.desktopMaxWidth,
        ]}
      >
        {useGradient ? (
          <GradientSection
            nativeID={nativeID}
            isDesktop={isDesktop}
            background={background}
            styles={styles}
            renderer={renderer}
          />
        ) : (
          renderer
        )}
      </ImageBackground>
    </View>
  )
}
