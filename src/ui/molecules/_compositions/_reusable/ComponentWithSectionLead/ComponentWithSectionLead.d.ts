import { ViewStyle } from 'react-native'

import { IGridItemElement } from '../../../../../atoms/Grid/Grid'
import { IGridItem } from '../../../../atoms/Grid/Grid'

export interface IComponentWithSectionLeadStyleProps extends IGridItem {
  background?: string
  style?: ViewStyle
}

export interface IComponentWithSectionLeadProps
  extends IComponentWithSectionLeadStyleProps {
  children: IGridItemElement
  description?: string
  color?: string
  id: string
  title: string
  isMobile: boolean
}
