import { cssClasses } from '../../../../../constants/CSS'
import { SPACING } from '../../../../../constants/Grid'
import { Grid } from '../../../../atoms/Grid'
import { useStyles } from '../../SectionLead/SectionLead.styles'
import { SectionLeadItem } from '../../SectionLeadItem'
import { IComponentWithSectionLeadProps } from './ComponentWithSectionLead'

export const ComponentWithSectionLead = ({
  children,
  title,
  description,
  background,
  color,
  id,
  gridItemProps,
  style,
  isMobile,
}: IComponentWithSectionLeadProps) => {
  const styles = useStyles({ background, color })
  const data = {
    title,
    description,
  }

  return (
    <Grid
      gridItemProps={gridItemProps}
      spacing={SPACING.HUGE}
      dataSet={cssClasses.componentWithSectionLead}
      nativeID={id}
      containerStyle={style}
    >
      <SectionLeadItem
        item={data}
        isMobile={isMobile}
        styles={styles}
        isSubheader={true}
      />
      {children}
    </Grid>
  )
}
