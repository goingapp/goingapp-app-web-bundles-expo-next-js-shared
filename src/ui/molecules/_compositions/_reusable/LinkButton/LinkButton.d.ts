import { StyleProp, StyleSheet, TextStyle, ViewStyle } from 'react-native'

import { ButtonVariant } from '../../../../../../constants/Variants/Button'
import { IResolveUrl } from '../../../../../models/Navigation'
import { IGridItem } from '../../../../atoms/Grid/Grid'

export interface IStyles {
  button: StyleProp<ViewStyle>
  label: StyleProp<TextStyle>
}

export interface ILinkButtonStyleProps {
  borderColor?: string
  bgColor?: string
  textColor?: string
  isOutlined?: boolean
}

export interface ILinkButtonProps extends IGridItem {
  url: string
  label: string
  style?: StyleSheet.NamedStyles
  borderColor?: string
  isOutlined?: boolean
  isFullWidth?: boolean
  isSecondary?: boolean
  bgColor?: string
  textColor?: string
  variant?: ButtonVariant
  resolveUrl: IResolveUrl
}
