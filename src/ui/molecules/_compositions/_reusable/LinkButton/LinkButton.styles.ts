import { makeStyles } from '../../../../../utils/Helpers/makeStyles'
import { ILinkButtonStyleProps, IStyles } from './LinkButton'

export const useStyles = makeStyles<ILinkButtonStyleProps, IStyles>(
  (theme, props) => ({
    button: {
      borderWidth: !props?.isOutlined ? 0 : 1,
      borderColor: props?.borderColor ?? theme.colors.text,
      ...(props?.bgColor ? { backgroundColor: props?.bgColor } : undefined),
    },
    label: {
      color:
        props?.textColor || props?.isOutlined
          ? theme.colors.text
          : theme.colors.primaryButtonText,
    },
  })
)
