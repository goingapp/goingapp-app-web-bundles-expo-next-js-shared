import { ButtonVariant } from '../../../../../constants/Variants'
import { MainButton } from '../../../../_reusable/MainButton'
import { ILinkButtonProps } from './LinkButton'
import { useStyles } from './LinkButton.styles'

export const LinkButton = ({
  url,
  label,
  style,
  borderColor,
  textColor,
  bgColor,
  gridItemProps,
  isOutlined,
  isSecondary,
  isFullWidth,
  variant,
  resolveUrl,
}: ILinkButtonProps) => {
  const styles = useStyles({ textColor, bgColor, borderColor, isOutlined })
  const handleUrl = () => resolveUrl(url)

  return (
    <MainButton
      action={handleUrl}
      label={label}
      secondary={isSecondary}
      variant={variant || ButtonVariant.large}
      textStyle={styles.label}
      outlined={isOutlined}
      gridItemProps={gridItemProps}
      style={[styles.button, style]}
      fullWidth={isFullWidth}
    />
  )
}
