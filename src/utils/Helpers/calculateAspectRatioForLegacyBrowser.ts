/**
 * Function calculates aspect ratio for unsupporting browsers, i.e. Safari < 15.
 *
 * The return value must be set as a percentage value of padding-top, in the same
 * rule as aspect-ratio is being set.
 *
 * Direct child component must additionally have position: absolute being set
 * and all four position values (top, left, right, bottom) changed to 0.
 *
 * @param {[number, number]} aspectRatio - expected aspect ratio of an element served
 * as an array of two numbers.
 *
 * */

export const calculateAspectRatioForLegacyBrowser = (
  aspectRatio: [number, number]
) => (aspectRatio[1] / aspectRatio[0]) * 100
