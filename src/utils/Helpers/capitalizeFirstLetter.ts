/**
 * Capitalize the first letter of a string
 *
 * @param {string} str - input string
 *
 * @return {string} string with first letter capitalized
 */
export const capitalizeFirstLetter = (str: string) =>
  str.charAt(0).toUpperCase() + str.slice(1)
