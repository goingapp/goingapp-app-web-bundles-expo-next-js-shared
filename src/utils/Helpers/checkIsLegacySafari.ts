import { useEffect, useState } from 'react'

export const checkIsLegacySafari = () => {
  const [isLegacySafari, setIsLegacySafari] = useState(false)

  useEffect(() => {
    try {
      const _isLegacyBrowser = /^((?!chrome|android).)*safari/i.test(
        navigator.userAgent
      )
      setIsLegacySafari(_isLegacyBrowser)
    } catch (error) {
      console.error(error)
    }
  }, [])

  return {
    isLegacySafari,
  }
}
