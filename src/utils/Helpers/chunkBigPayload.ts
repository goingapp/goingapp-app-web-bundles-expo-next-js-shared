export const chunkBigPayload = <TData>(
  data: TData[],
  batchSize: number = 10
) => {
  let chunksArr: TData[][] = []
  let currentChunk = -1

  data?.map((item, index) => {
    if (index === 0 || index % batchSize === 0) {
      chunksArr.push([])
      currentChunk++
    }

    if (!chunksArr[currentChunk]) {
      return
    }

    chunksArr[currentChunk].push(item)
  })

  return chunksArr
}
