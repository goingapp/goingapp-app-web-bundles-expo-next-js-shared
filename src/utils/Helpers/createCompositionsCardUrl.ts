export const createCompositionsCardUrl = (
  link?: string,
  slug?: string,
  isNext = true
) => {
  const isWordpressLink = link?.includes('artykul/')

  if (slug) {
    return `/wydarzenie/${slug}`
  }

  if (link?.includes('more-legacy.goingapp.pl')) {
    const linkSplit = link?.split('/')
    return `/artykul/${
      link?.endsWith('/')
        ? linkSplit[linkSplit.length - 2]
        : linkSplit[linkSplit.length - 1]
    }`
  }

  return isWordpressLink
    ? isNext
      ? link?.replace('/going/', '/').replace('/partner-profile/', '/')
      : link?.replace('/partner-profile/', '/')
    : link
}
