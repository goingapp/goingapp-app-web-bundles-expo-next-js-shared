/**
 * Function formatting plural nouns in a selected locale (not supported in safari <13 and Android)
 *
 * @param { number } count - number in which to draw a noun
 * @param { string } locale - selected language
 * @param { [key: string]: string } nouns - plural versions of the noun
 *   eg for polish: {many: 'wyników', one: 'wynik', few: 'wyniki'}
 *   eg for english/german: {one: 'file', other: 'files'}
 *
 * @return {string} formatted plural noun
 */

export const formatPluralNouns = (
  count: number,
  locale: string,
  nouns: { [key: string]: string }
): string => {
  const format = new Intl.PluralRules(locale)
  const quantity = format.select(count)

  return nouns[quantity]
}
