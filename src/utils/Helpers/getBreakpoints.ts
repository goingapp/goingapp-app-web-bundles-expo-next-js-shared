import { BreakpointValue } from '../../constants/Breakpoints'

/**
 * Function returns device specific breakpoints depending on the screen width.
 *
 * @param width
 */
export const getBreakpoints = (width: number) => {
  const isDesktop = width > BreakpointValue.LG
  const isMobile = width < BreakpointValue.SM
  const isTablet = !isMobile && !isDesktop

  return {
    isDesktop,
    isMobile,
    isTablet,
    noDesktop: !isDesktop,
    noMobile: !isMobile,
    noTablet: !isTablet,
  }
}
