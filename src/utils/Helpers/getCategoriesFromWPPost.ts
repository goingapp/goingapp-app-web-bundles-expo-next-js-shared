import { WP_REST_API_Post } from 'wp-types'

export const getCategoriesFromWPPost = (post: WP_REST_API_Post) =>
  post.categories || []
