import { IDate } from '../../models/DateTime'
import { IDateTranslations } from '../../models/Locale'
import { DateTime } from '../../services/Date/DateTime'

export const getFriendlyDate = (
  dateTranslations: IDateTranslations,
  date?: IDate,
  dateDesc?: string
) =>
  dateDesc
    ? dateDesc
    : date
    ? DateTime.getFriendlyDate({
        isoDate: DateTime.getIsoDateString(date),
        dateTranslate: dateTranslations,
      })
    : undefined
