import { Fragment } from 'react'

/**
 * function that inserts <br> markups on line breaks
 *
 * @param {string} text
 *
 * @return {ReactNode[]} array of text nodes
 */
export const insertLineBreaks = (text: string) => {
  const parts = text.split('\r\n')

  return parts.map((part, index) => (
    <Fragment key={index}>
      {part}
      {index + 1 < parts.length && <br />}
    </Fragment>
  ))
}
