import { useMemo } from 'react'
import { StyleSheet } from 'react-native'

import { useTheme } from 'react-native-paper'

/**
 * Hook factory function. Returns useStyles hook, used with props parameter to compile dynamic component styles. Usage:
 *
 * const useStyles = makeStyles<StyleProps, Styles(optional)>(({props}, {theme}) => {styles})
 *
 * const styles = useStyles({props}) - when using without props, pass an empty object.
 *
 * The useStyles hook takes props object as a parameter and returns React Native named styles.
 *
 * @return {(props: TStyleProps) => TStyles}
 */
export const makeStyles =
  <TStyleProps, TStyles>(
    styles: (theme: ReactNativePaper.Theme, props?: TStyleProps) => TStyles
  ): ((props?: TStyleProps) => TStyles) =>
  (props) => {
    const theme = useTheme()

    return useMemo(
      () => StyleSheet.create(styles(theme, props)),
      Object.values(props ? props : {})
    )
  }
