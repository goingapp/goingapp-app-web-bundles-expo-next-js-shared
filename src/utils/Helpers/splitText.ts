/**
 * function that splits continuous text in string on enters
 *
 * @param {string} text
 *
 * @return {string[]} array of text string blocks
 */
export const splitText = (text: string): string[] => text.split('\r\n\r\n')
