/* tslint:disable:no-console */
import { LoggerMessages } from './Logger'

export class Logger {
  public static debug(
    reason: string,
    messages: LoggerMessages,
    styles: string = 'color: #f4b841'
  ) {
    if (__DEV__) {
      console.log(`%c ${Logger.DEBUG}[${reason}]`, styles, messages)
    } else {
      try {
        // @TODO: add Crashlytis
      } catch (e) {}
    }
  }

  public static error(
    reason: string,
    messages: LoggerMessages,
    styles: string = 'color: #f45042'
  ) {
    if (__DEV__) {
      console.log(`%c ${Logger.ERROR}[${reason}]`, styles, messages)
    } else {
      // @TODO: add Crashlytis
    }
  }

  public static fatal(
    reason: string,
    messages: LoggerMessages,
    styles: string = 'color: yellow'
  ) {
    if (__DEV__) {
      console.log(`%c ${Logger.FATAL}[${reason}]`, styles, messages)
    } else {
      // @TODO: add Crashlytis
    }
  }

  public static info(
    reason: string,
    messages: LoggerMessages,
    styles: string = 'color: #4286f4'
  ) {
    if (__DEV__) {
      console.log(`%c ${Logger.INFO}[${reason}]`, styles, messages)
    } else {
      try {
        // @TODO: add Crashlytis
      } catch (e) {}
    }
  }

  protected static DEBUG = '[Debug]'
  protected static INFO = '[Info]'
  protected static ERROR = '[Error]'
  protected static FATAL = '[FATAL]'
}
